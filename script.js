"use strict";

//
// Important TODO:
//     Change how we deal with temp health? It's kind of awkward to deal with healing, damage, and getting more temp health applied
//         Can we just initially treat it as negative damage? Not technically because it can't stack, but in practice it might be ok
//     Replace eval() with https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/eval#Never_use_eval!
//     Display dice statistics graph for lots of things
//         damage for equipped weapons
//         saving throw/ability check vs DCs
//
// Minor TODO
//     Change the icons from svg to png? See if that shrinks the size of the saved webpage?
//     Current conditions
//         Exhaustion level
//         Any misc effects (potion effects, etc...)
//     Auto include CSS script to reduce duplication?
//         Could auto include a lot of things
//         window.location.pathname
//     Add section in Combat Options for "Free Action"?
//     Make inventory description a list so we get nicer line breaks?
//     SavingThrow formula
//         support things that affect saving throws
//         i.e. normally your strength saving throw == StrMod + Proficiency, but the paladin has an aura that adds their ChaMod (e.g. +3)
//         Stone of Good Luck
//     Death Saves?
//     Ability to sort tables
//         Looks like this needs JS
//     Add spell/item description as a tooltip?
//         Would have to support long, long text
//     Better tooltip background color?
//     Better formula for physical attributes (e.g. running long jump distance)
//         Add global variables?
//

//////////////////////
// GLOBAL CONSTANTS //
//////////////////////
const Stat =
{
    Strength     : "Strength",
    Dexterity    : "Dexterity",
    Constitution : "Constitution",
    Intelligence : "Intelligence",
    Wisdom       : "Wisdom",
    Charisma     : "Charisma",
}

const StatToShortStat =
{
    [Stat.Strength    ] : "Str",
    [Stat.Dexterity   ] : "Dex",
    [Stat.Constitution] : "Con",
    [Stat.Intelligence] : "Int",
    [Stat.Wisdom      ] : "Wis",
    [Stat.Charisma    ] : "Cha",
}

const Skill =
{
    Acrobatics     : "Acrobatics",
    AnimalHandling : "AnimalHandling",
    Arcana         : "Arcana",
    Athletics      : "Athletics",
    Deception      : "Deception",
    History        : "History",
    Insight        : "Insight",
    Intimidation   : "Intimidation",
    Investigation  : "Investigation",
    Medicine       : "Medicine",
    Nature         : "Nature",
    Perception     : "Perception",
    Performance    : "Performance",
    Persuasion     : "Persuasion",
    Religion       : "Religion",
    SleightOfHand  : "SleightOfHand",
    Stealth        : "Stealth",
    Survival       : "Survival",
}

const SkillToStat =
{
    [Skill.Acrobatics    ] : Stat.Dexterity,
    [Skill.AnimalHandling] : Stat.Wisdom,
    [Skill.Arcana        ] : Stat.Intelligence,
    [Skill.Athletics     ] : Stat.Strength,
    [Skill.Deception     ] : Stat.Charisma,
    [Skill.History       ] : Stat.Intelligence,
    [Skill.Insight       ] : Stat.Wisdom,
    [Skill.Intimidation  ] : Stat.Charisma,
    [Skill.Investigation ] : Stat.Intelligence,
    [Skill.Medicine      ] : Stat.Wisdom,
    [Skill.Nature        ] : Stat.Intelligence,
    [Skill.Perception    ] : Stat.Wisdom,
    [Skill.Performance   ] : Stat.Charisma,
    [Skill.Persuasion    ] : Stat.Charisma,
    [Skill.Religion      ] : Stat.Intelligence,
    [Skill.SleightOfHand ] : Stat.Dexterity,
    [Skill.Stealth       ] : Stat.Dexterity,
    [Skill.Survival      ] : Stat.Wisdom,
}

const Size =
{
    Tiny     : "Tiny",
    Small    : "Small",
    Medium   : "Medium",
    Large    : "Large",
    Huge     : "Huge",
    Gigantic : "Gigantic",
}

const CarryingCapacityMultiplier =
{
    [Size.Tiny    ] : 0.5,
    [Size.Small   ] : 1,
    [Size.Medium  ] : 1,
    [Size.Large   ] : 2,
    [Size.Huge    ] : 4,
    [Size.Gigantic] : 8,
}

const SqueezeSize =
{
    [Size.Tiny    ] : Size.Tiny, // Nothing smaller than Tiny.
    [Size.Small   ] : Size.Tiny,
    [Size.Medium  ] : Size.Small,
    [Size.Large   ] : Size.Medium,
    [Size.Huge    ] : Size.Large,
    [Size.Gigantic] : Size.Huge,
}

const Class =
{
    Artificer   : "Artificer",
    Barbarian   : "Barbarian",
    Bard        : "Bard",
    Cleric      : "Cleric",
    Druid       : "Druid",
    Expert      : "Expert",      // Tasha's sidekick class.
    Fighter     : "Fighter",
    Monk        : "Monk",
    Paladin     : "Paladin",
    Ranger      : "Ranger",
    Rogue       : "Rogue",
    Sorcerer    : "Sorcerer",
    Spellcaster : "Spellcaster", // Tasha's sidekick class.
    Warlock     : "Warlock",
    Warrior     : "Warrior",     // Tasha's sidekick class.
    Wizard      : "Wizard",
}

/////////////////
// AUTO RELOAD //
/////////////////

// It is very difficult to get the webpage to auto reload when the underlying
// HTML/JS/CSS files change without serving the webpage from an external HTTP
// server. So since we cannot do it automatically we can at least make it as
// effortless as possible.
//
// Put it very early in the script so that it will still be setup even if
// later code has syntax errors.
{
    // When the window loses focus...
    window.onblur = function()
    {
        // And then regains focus...
        window.onfocus = function()
        {
            // Force reload the page.
            //
            // We don't bypass the browser cache because that also resets the scroll position in Firefox.
            // The webpage always seems to reload with updated data as expected so I guess it's fine. I'm
            // not sure what data the browser would be caching in this case since it's a local file.
            location.reload(false);
        }
    };
}

/////////////////////
// ERRORS & ASSERT //
/////////////////////
function CrashThePage(errorMessage, invalidObject)
{
    // Print out a "friendly" error message.
    console.error(errorMessage);
    alert(errorMessage);

    // Also print out the object itself to help debugging.
    // Having it be on its own log line makes it easier to
    // inspect inside Firefox.
    console.error(invalidObject);
    alert(invalidObject);

    // Actually crash the page.
    throw new Error("Crashing the page!");
}

function AssertEq(actual, expected)
{
    if( actual !== expected )
    {
        CrashThePage(`"${actual}" != "${expected}"`);
    }
}

//////////////////////////////////////////
// AUTO-CALCULATED CHARACTER PROPERTIES //
//////////////////////////////////////////

//
// Helper function to make sure that all the auto-added properties are configured the same.
//
function DefineProperty(obj, propertyName, propertyGetter)
{
    // We want to make sure that we don't overwrite a property that
    // already exists since it could cause some hard to notice bugs.
    if( typeof(obj[propertyName]) === "undefined" )
    {
        Object.defineProperty( obj, propertyName,
        {
            get: propertyGetter,

            // Needed so that if we print the object as JSON it appears.
            // Also just keeps the autogenerated properties treated the
            // same as the "normal" properties.
            enumerable: true,
        });
    }
    else
    {
        CrashThePage(`Property "${propertyName}" is already defined! We don't want to overwrite it!`, obj);
    }
}

//
// Helper function to take in a mathematical expression
// as string and evaluate it to get a value.
//
function EvaluateString(s)
{
    // Evaluate the formula.
    // Pretty bad/gross, but this is only for local use so whatever.
    return eval( s );
}

//
// Helper function to automatically find all "formula" properties and create
// new "value" properties that are the result of calculating the formula.
//
// NOTE: This only does a shallow search rather than doing a deep recursion
//       through the entire object. This is because there are dependencies
//       between the formulas (i.e. formulas that reference the values derived
//       from other formulas) so we have to resolve the formulas in a manual
//       order.
//
function DefineValuePropertiesFromFormulas(obj)
{
    // Create a copy since we're going to be
    // adding new properties as we iterate.
    var currentKeys = Object.keys(obj);

    const keySuffix = "_Formula";

    for( const key of currentKeys )
    {
        if( !key.endsWith(keySuffix) )
        {
            // Property didn't have the right name, don't care about it.
            continue;
        }

        // Remove the suffix.
        const newPropertyName = key.slice(0, -keySuffix.length);

        // Create a new property that is the evaluated formula.
        DefineProperty( obj, newPropertyName, function()
        {
            // We pass in the object itself as the context so that
            // strings that reference "this" evaluate correctly.
            return EvaluateString.call( obj, obj[key] );
        });
    }
};

//
// A helper function to setup the global variables used in formulas.
//
// We keep track of every variable we add so that we can show how the
// formulas are evaluated in GetTooltipTextWithReplacedGlobalVariables().
//
function AddAndRecordGlobalVariable(globalVariableName, globalVariableValue)
{
    // If this is the first variable we've added then setup
    // the global list we use to keep track of them.
    if( typeof window.ExtraGlobalVariableList == 'undefined' )
    {
        window.ExtraGlobalVariableList = [];
    }

    // Each global variable should only be recorded one time
    // so make sure we don't silently record something twice.
    if( !window.ExtraGlobalVariableList.includes(globalVariableName) )
    {
        window.ExtraGlobalVariableList.push(globalVariableName);

        // Actually create the global variable.
        window[globalVariableName] = globalVariableValue;
    }
    else
    {
        CrashThePage(`Global variable ${globalVariableName} has already been recorded!`, globalVariableValue);
    }
}

//
// Add a bunch more properties to the input character to
// cover basic game rules and formulas.
//
// Also wrap some character properties in global functions
// so that the formulas are much shorter and nicer to read.
//
function DefineAutoGeneratedAndGlobalProperties(character)
{
    //////////////////////////////
    // LEVEL & CHARACTER LEVELS //
    //////////////////////////////
    DefineProperty( character, "Level", function()
    {
        return this.Levels.length;
    });

    for( const className in Class )
    {
        const classLevelPropertyName = className + "Level";

        DefineProperty( character, classLevelPropertyName, function()
        {
            return character.Levels.filter(x => x == className).length;
        });
    }

    AddAndRecordGlobalVariable("Level", character.Level);

    for( const className in Class )
    {
        const classLevelPropertyName = className + "Level";

        AddAndRecordGlobalVariable(classLevelPropertyName, character[classLevelPropertyName]);
    }

    ///////////////////////
    // PROFICIENCY BONUS //
    ///////////////////////
    DefineProperty( character, "ProficiencyBonus_Formula", function()
    {
        return "Math.ceil(Level / 4) + 1";
    });

    // We need to evaluate the proficiency bonus formula right
    // now since because subsequent formulas depend on it.
    DefineValuePropertiesFromFormulas(character);

    AddAndRecordGlobalVariable("ProficiencyBonus", character.ProficiencyBonus);
    AddAndRecordGlobalVariable("ProfBonus"       , character.ProficiencyBonus);

    ///////////
    // STATS //
    ///////////
    for( const stat in Stat )
    {
        const characterStat = character.Stats[stat];

        // Modifier.
        DefineProperty( characterStat, "Modifier_Formula", function()
        {
            return `Math.floor((${stat} - 10) / 2)`;
        });

        // Saving Throw Proficiency
        DefineProperty( characterStat, "SavingThrowIsProficient", function()
        {
            return character.ProficientSavingThrows.includes(stat);
        });

        // Saving Throw Modifier.
        DefineProperty( characterStat, "SavingThrow_Formula", function()
        {
            const statModifier = stat + "Modifier";

            if( this.SavingThrowIsProficient )
            {
                return `${statModifier} + ProficiencyBonus`;
            }
            else
            {
                return `${statModifier}`;
            }
        });

        DefineValuePropertiesFromFormulas( characterStat );

        const shortStart = StatToShortStat[stat];

        AddAndRecordGlobalVariable(stat              , characterStat.Value   ); // "Strength"
        AddAndRecordGlobalVariable(shortStart        , characterStat.Value   ); // "Str"
        AddAndRecordGlobalVariable(stat + "Modifier" , characterStat.Modifier); // "StrengthModifier"
        AddAndRecordGlobalVariable(stat + "Mod"      , characterStat.Modifier); // "StrengthMod"
        AddAndRecordGlobalVariable(shortStart + "Mod", characterStat.Modifier); // "StrMod"
    }

    // Need to be after the stats because these formulas probably reference a stat modifier.
    AddAndRecordGlobalVariable("AttackBonus", character.AttackBonus);
    AddAndRecordGlobalVariable("SaveDC"     , character.SaveDC     );

    ////////////
    // SKILLS //
    ////////////

    // Need to create empty map since input JSON doesn't have a skills section.
    const characterSkills = character.Skills = {};

    for( const skill in Skill )
    {
        // Need to create empty map since input JSON doesn't have a skills section.
        const characterSpecificSkill = characterSkills[skill] = {};

        // Proficiency Level.
        DefineProperty( characterSpecificSkill, "ProficiencyMultiplier", function()
        {
            const lookup = character.ProficientSkillMultipliers[skill];

            return (lookup == null) ? character.DefaultSkillProficiencyMultiplier
                                    : lookup;
        });

        DefineProperty( characterSpecificSkill, "IsProficient", function()
        {
            return this.ProficiencyMultiplier > character.DefaultSkillProficiencyMultiplier;
        });

        // Modifier.
        DefineProperty( characterSpecificSkill, "Modifier_Formula", function()
        {
            const statForSkill = SkillToStat[skill];
            const statModifier = statForSkill + "Modifier";

            // In an attempt to keep the formula text/tooltip as simple and
            // readable as possible we manually handle some common cases.
            let proficiencyBonusCalculation = "!!!UNREPLACED!!!";

            if( this.ProficiencyMultiplier === 0 )
            {
                // The proficiency bonus doesn't do anything, so don't mention it.
                proficiencyBonusCalculation = "";
            }
            else if( this.ProficiencyMultiplier === 1 )
            {
                // There's no need to multiply anything.
                proficiencyBonusCalculation = " + ProficiencyBonus";
            }
            else if( Number.isInteger(character.ProficiencyBonus * this.ProficiencyMultiplier) )
            {
                // There's no need to round anything.
                proficiencyBonusCalculation = ` + (ProficiencyBonus * ${this.ProficiencyMultiplier})`;
            }
            else
            {
                // The most general case, we have to handle every
                // case so the formula is the most verbose.
                proficiencyBonusCalculation = ` + Math.floor(ProficiencyBonus * ${this.ProficiencyMultiplier})`;
            }

            return `${statModifier}${proficiencyBonusCalculation}`;
        });

        DefineValuePropertiesFromFormulas(characterSpecificSkill);

        AddAndRecordGlobalVariable(skill, characterSpecificSkill.Modifier);
    }

    /////////////////////////
    // HEALTH AND HIT DICE //
    /////////////////////////
    const classToHitDieSizeMapping =
    {
        [Class.Artificer]   :  8,
        [Class.Barbarian]   : 12,
        [Class.Bard]        :  8,
        [Class.Cleric]      :  8,
        [Class.Druid]       :  8,
        [Class.Expert]      : character.TashaSidekickHitDieSize,
        [Class.Fighter]     : 10,
        [Class.Monk]        :  8,
        [Class.Paladin]     : 10,
        [Class.Ranger]      : 10,
        [Class.Rogue]       :  8,
        [Class.Sorcerer]    :  6,
        [Class.Spellcaster] : character.TashaSidekickHitDieSize,
        [Class.Warlock]     :  8,
        [Class.Warrior]     : character.TashaSidekickHitDieSize,
        [Class.Wizard]      :  6,
    };

    // Record the size and number of all the hit dice the character has from all sources.
    // Use an an array rather than a map or object because it needs to be sorted.
    character.Health.SortedHitDiceList = [];

    let AddHitDice = function(hitDieSize, numHitDiceToAdd)
    {
        const entry = character.Health.SortedHitDiceList.find( function(element)
        {
            return element.Size == hitDieSize;
        });

        // Existing die size, update.
        if( entry != undefined )
        {
            entry.CurrentAmount += numHitDiceToAdd;
            entry.MaxAmount     += numHitDiceToAdd;
        }
        // New die size, add entry.
        else
        {
            character.Health.SortedHitDiceList.push(
            {
                Size          : hitDieSize,
                CurrentAmount : numHitDiceToAdd,
                MaxAmount     : numHitDiceToAdd,
            });
        }
    };

    let totalHealthGainDueToHitDiceFormula = "";

    let hasAnySidekickLevels = false;

    // Accumulate the HP and hit dice from all your class levels.
    for( let i = 0; i < character.Levels.length; i++ )
    {
        const classTaken = character.Levels[i];
        const isFirstLevel = (i == 0);

        const classHitDieSize = classToHitDieSizeMapping[classTaken];

        AddHitDice(classHitDieSize, 1);

        // Rolling for health is not supported, always take the expected value.
        //
        // If you want to roll for health you'll have to do it manually and
        // then set it using OverrideMaxHP_Formula.
        const hitDieExpected = Math.ceil((classHitDieSize + 1) / 2);

        // Tasha's sidekick system has special rules related to health.
        const isSideKick = (classTaken == Class.Expert)
                        || (classTaken == Class.Warrior)
                        || (classTaken == Class.Spellcaster);

        if( isSideKick )
        {
            hasAnySidekickLevels = true;
        }

        // Tasha's sidekicks explicitly don't get the extra health boost at first level.
        // They always just get the average gain (since we don't support rolling for health).
        const doesFirstLevelGiveMoreHealth = !isSideKick;

        // First level (usually) gets more health.
        const healthGainDueToHitDie = (isFirstLevel && doesFirstLevelGiveMoreHealth) ? classHitDieSize : hitDieExpected;

        if( !isFirstLevel )
        {
            totalHealthGainDueToHitDiceFormula += " + ";
        }

        totalHealthGainDueToHitDiceFormula += `${healthGainDueToHitDie}`;
    }

    // Tasha's sidekicks get an extra boost to their starting values for health and
    // hit dice because they use a creature from the Monster Manual as a base.
    if( hasAnySidekickLevels )
    {
        AddHitDice(character.TashaSidekickHitDieSize, character.TashaSidekickStartingNumHitDice);

        totalHealthGainDueToHitDiceFormula = `${character.TashaSidekickStartingHealth} + ${totalHealthGainDueToHitDiceFormula}`;
    }

    // Want largest hit die first.
    // Need custom compare function because Javascript compares numbers as strings by default.
    character.Health.SortedHitDiceList.sort( function(a, b)
    {
        return b.Size - a.Size;
    });

    const numTotalHitDice = character.Health.SortedHitDiceList.map(x => x.MaxAmount).reduce((partial_sum, a) => partial_sum + a, 0);

    // Global variables.
    AddAndRecordGlobalVariable("HitDiceMax"  , numTotalHitDice);
    AddAndRecordGlobalVariable("HitDiceSpent", character.Health.NumHitDiceSpent);

    // Adjust the current hit dice amounts based on how many we've spent.
    // Spend the largest hit dice first because it's
    //     1) Usually the smartest option.
    //     2) Reduces the amount of extra player bookkeeping needed in the character sheet.
    let numHitDiceSpent = character.Health.NumHitDiceSpent;

    for( const hitDie of character.Health.SortedHitDiceList )
    {
        if( numHitDiceSpent <= 0 )
        {
            break;
        }

        const numHitDiceToSpend = Math.min(numHitDiceSpent, hitDie.CurrentAmount);

        numHitDiceSpent      -= numHitDiceToSpend;
        hitDie.CurrentAmount -= numHitDiceToSpend;
    }

    // Max Health.
    DefineProperty( character.Health, "MaxHP_Formula", function()
    {
        // If the character has a specific amount of HP then just use that.
        if( character.OverrideMaxHP_Formula !== undefined )
        {
            return `(${character.OverrideMaxHP_Formula})`;
        }
        // Otherwise derive it based on what class (i.e. hit die size) you took at each level.
        else
        {
            const extraHealthGainedDueToLevelFormula = `Level * (${character.ExtraHealthGainOnLevel_Formula})`;

            return `(${totalHealthGainDueToHitDiceFormula}) + (${extraHealthGainedDueToLevelFormula})`;
        }
    });

    // Current Health.
    DefineProperty( character.Health, "CurrentHP_Formula", function()
    {
        return `${this.MaxHP} + ${this.TempHP} - (${this.CurrentDamage_Formula})`;
    });

    // Hit Dice.
    DefineProperty( character.Health, "NumHitDiceRestoredOnLongRest_Formula", function()
    {
        return `Math.max(1, Math.floor(HitDiceMax / 2))`;
    });

    DefineProperty( character.Health, "CurrentNumHitDice_Formula", function()
    {
        return `HitDiceMax - HitDiceSpent`;
    });

    DefineValuePropertiesFromFormulas(character.Health);

    /////////////////////////
    // PHYSICAL ATTRIBUTES //
    /////////////////////////
    for( const size of Object.values(Size) )
    {
        AddAndRecordGlobalVariable( `CarryingCapacityMultiplier.${size}`, CarryingCapacityMultiplier[size] );
    }

    // Global variables.
    AddAndRecordGlobalVariable("Height", character.PhysicalAttributes.Height);

    DefineProperty( character.PhysicalAttributes, "CarryingCapacity_Formula", function()
    {
        return `(Strength * 15) * CarryingCapacityMultiplier.${this.CarryingCapacitySize}`;
    });

    DefineProperty( character.PhysicalAttributes, "PushPullLiftWeight_Formula", function()
    {
        return `this.CarryingCapacity * 2`;
    });

    DefineProperty( character.PhysicalAttributes, "LongJumpRunning_Formula", function()
    {
        return `Strength`;
    });

    DefineProperty( character.PhysicalAttributes, "LongJumpStanding_Formula", function()
    {
        return `Math.floor(Strength / 2)`;
    });

    DefineProperty( character.PhysicalAttributes, "HighJumpRunningNoCharacterHeight_Formula", function()
    {
        return `3 + StrengthModifier`;
    });

    DefineProperty( character.PhysicalAttributes, "HighJumpStandingNoCharacterHeight_Formula", function()
    {
        return `Math.floor((3 + StrengthModifier) / 2)`;
    });

    DefineProperty( character.PhysicalAttributes, "HighJumpRunningWithCharacterHeight_Formula", function()
    {
        return `Math.floor(3 + StrengthModifier + (Height * 1.5))`;
    });

    DefineProperty( character.PhysicalAttributes, "HighJumpStandingWithCharacterHeight_Formula", function()
    {
        return `Math.floor(((3 + StrengthModifier) / 2) + (Height * 1.5))`;
    });

    DefineValuePropertiesFromFormulas(character.PhysicalAttributes);

    AddAndRecordGlobalVariable("MoveSpeed"  , character.PhysicalAttributes.MoveSpeed  );
    AddAndRecordGlobalVariable("FlyingSpeed", character.PhysicalAttributes.FlyingSpeed);
    AddAndRecordGlobalVariable("SwimSpeed"  , character.PhysicalAttributes.SwimSpeed  );
    AddAndRecordGlobalVariable("ClimbSpeed" , character.PhysicalAttributes.ClimbSpeed );

    /////////////
    // ATTACKS //
    /////////////
    for( const attack of character.Attacks )
    {
        DefineValuePropertiesFromFormulas(attack);
    }

    for( const savingThrowAttack of character.SavingThrowAttacks )
    {
        DefineValuePropertiesFromFormulas(savingThrowAttack);
    }

    ///////////////
    // RESOURCES //
    ///////////////
    for( const resource of character.Resources )
    {
        DefineValuePropertiesFromFormulas(resource);

        const recoverString = `Recover ${resource.Name}`;

        if( resource.Recovery == "Short Rest")
        {
            character.RestOptions["On Short Rest"].push(recoverString);
            character.RestOptions["On Long Rest" ].push(recoverString);
        }
        else if( resource.Recovery == "Long Rest")
        {
            character.RestOptions["On Long Rest"].push(recoverString);
        }
        else
        {
            CrashThePage(`Unknown resource recovery option "${resource.Recovery}". Must be "Short Rest" or "Long Rest"!`, resource);
        }
    }

    //////////////////
    // REST OPTIONS //
    //////////////////
    const currentHitDice               = GetInlineFormulaTooltipHTML( character.Health, "CurrentNumHitDice"            );
    const numHitDiceRestoredOnLongRest = GetInlineFormulaTooltipHTML( character.Health, "NumHitDiceRestoredOnLongRest" );

    character.RestOptions["On Short Rest"].push(`Spend up to ${currentHitDice} hit dice`);

    if( character.SpellSlots.length > 0 )
    {
        character.RestOptions["On Long Rest"].push("Restore all spell slots");
    }

    character.RestOptions["On Long Rest"].push("Recover all HP");
    character.RestOptions["On Long Rest"].push("Lose any temp HP");
    character.RestOptions["On Long Rest"].push(`Recover up to ${numHitDiceRestoredOnLongRest} spent hit dice`);
    character.RestOptions["On Long Rest"].push("Reduce exhaustion level by 1");

    ////////////////////
    // COMBAT OPTIONS //
    ////////////////////
    const athletics  = GetInlineTooltipHTML( PrefixNumberWithSign(Athletics ), "Athletics"  );
    const acrobatics = GetInlineTooltipHTML( PrefixNumberWithSign(Acrobatics), "Acrobatics" );
    const stealth    = GetInlineTooltipHTML( PrefixNumberWithSign(Stealth   ), "Stealth"    );

    const overrunTumble = `${GetInlineTooltipHTML("DMG 272", "Variant")}: Overrun (${athletics})/Tumble (${acrobatics})`;

    character.CombatOptions["Action"].push("Attack");
    character.CombatOptions["Action"].push(`${GetInlineTooltipHTML("PHB 195", "Shove/Grapple")} a creature (${athletics})`);
    character.CombatOptions["Action"].push(`Escape a grapple (${athletics} or ${acrobatics})`);
    character.CombatOptions["Action"].push("Use an Object, Search, Ready, Help, Dodge, Disengage, Dash");
    character.CombatOptions["Action"].push(`Hide (${stealth})`);
    character.CombatOptions["Action"].push("Stabilize a creature");
    character.CombatOptions["Action"].push("Don/Doff a shield");
    character.CombatOptions["Action"].push(`${overrunTumble}`);

    character.CombatOptions["Bonus Action"].push(`${GetInlineTooltipHTML("PHB 195", "Two-Weapon Fighting")} (if holding a light melee weapon in other hand)`);
    character.CombatOptions["Bonus Action"].push(`${overrunTumble}`);

    character.CombatOptions["Reaction"].push("Release a readied Action when the trigger occurs");
    character.CombatOptions["Reaction"].push("Opportunity attack if enemy leaves melee range");
    character.CombatOptions["Reaction"].push("Dismount as your mount is knocked prone to land on your feet");

    const standFromProne = GetInlineFormulaTooltipHTML(character.PhysicalAttributes, "StandFromProne");

    character.CombatOptions["Movement"].push("Drop prone");
    character.CombatOptions["Movement"].push(`Get up from being prone (costs ${standFromProne} movement)`);
    character.CombatOptions["Movement"].push(`${GetInlineTooltipHTML("PHB 192", "Squeeze")} (move through space large enough for a ${SqueezeSize[character.PhysicalAttributes.Size]} creature)`);
    character.CombatOptions["Movement"].push(`${GetInlineTooltipHTML("PHB 182", "Climb")} (cost 1 extra foot per foot of movement)`);
    character.CombatOptions["Movement"].push(`${GetInlineTooltipHTML("PHB 182", "Swim")} (cost 1 extra foot per foot of movement)`);
    character.CombatOptions["Movement"].push(`${GetInlineTooltipHTML("PHB 182", "Long/High Jump")} (move 10 feet on foot first for double distance)`);

    character.CombatOptions["Any Time"].push("Release a grappled creature");
    character.CombatOptions["Any Time"].push("End concentration on a spell");

    ////////////////////
    // EXTERNAL LINKS //
    ////////////////////
    character.ExternalLinks["Spells List"  ] = "https://donjon.bin.sh/5e/spells/";
    character.ExternalLinks["Spell Cards"  ] = "http://hardcodex.ru/";
    character.ExternalLinks["D&D 5e Graphs"] = "https://stuart-rudderham.bitbucket.io/dnd5e-statistics/";
}

/////////////////////
// HTML GENERATION //
/////////////////////
function PrefixNumberWithSign(input)
{
    if( typeof input === 'number' )
    {
        // Negative numbers will automatically get a "-" character
        // prefixed so we only need to manually add a "+".
        return (input < 0 ? "" : "+" ) + input;
    }
    else
    {
        // Not a number, leave it unchanged.
        return input;
    }
}

function PrefixNumberWithSpaceAndSign(input)
{
    if( typeof input === 'number' )
    {
        // Handle negative explicitly because we want
        // to insert a space after the "-" character.
        return (input < 0 ? "- " : "+ " ) + Math.abs(input);
    }
    else
    {
        // Not a number, leave it unchanged.
        return input;
    }
}

//
// Helper function to help explain the math done in the tooltip text.
// For example, rather than just showing:
//
//     "StrengthModifier + ProficiencyBonus"
//
// we can show the equation after it's been partially solved:
//
//     "StrengthModifier + ProficiencyBonus"
//     "3 + 2"
//
function GetTooltipTextWithReplacedGlobalVariables(tooltipText)
{
    let tooltipTextWithReplacedGlobalVariables = tooltipText;

    // Find all occurrences of any of global variable we've created
    // and replace them with the value of that global variable.
    if( window.ExtraGlobalVariableList )
    {
        for( const globalVariableName of window.ExtraGlobalVariableList )
        {
            // We need to use RegEx so that the String.replace function replaces
            // all instances of the string rather than just the first.
            //
            // We use '\b' in the RegEx before and after what we want to replace
            // to make sure that we're replacing exactly that and don't run into
            // prefix/suffix matching issues. For example, we don't want "Cha" to
            // match with "Charisma" or "Level" to match "WizardLevel" because
            // then we'd end up with garbage like "10isma".
            //
            // Using '\b' rather than just a simple whitespace check like ' '
            // is less restrictive on the user, it should be perfectly valid for
            // them to have a tooltip formula that is "Cha+Int*3".
            const re = new RegExp(`\\b${globalVariableName}\\b`, 'g');

            tooltipTextWithReplacedGlobalVariables = tooltipTextWithReplacedGlobalVariables.replace(re, window[globalVariableName]);
        }
    }

    // This tooltip text had no global variables in it so nothing was changed.
    const isSame = (tooltipTextWithReplacedGlobalVariables == tooltipText);

    // This tooltip text has been simplified down to a single number (i.e. it
    // was only a single global variable, no math involved).
    //
    // Checking if a string is a number in JS is not very intuitive.
    //     https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/isNaN
    //     https://stackoverflow.com/a/175787
    const isOnlyNumber = !isNaN(tooltipTextWithReplacedGlobalVariables);

    if( isSame || isOnlyNumber )
    {
        // The replaced tooltip text didn't actually add any value so we don't show it.
        return tooltipText;
    }
    else
    {
        // The replaced tooltip text is a simplified version of the input text
        // so we include it as a second line in the tooltip so the user can see
        // the formula being evaluated.
        return `${tooltipText}\n${tooltipTextWithReplacedGlobalVariables}`;
    }
}

function GetInlineTooltipHTML(tooltipText, elementText, elementTextFormatFunction)
{
    // If the tooltip text doesn't actually add any value (e.g. it's
    // the same as the displayed text) then don't bother showing it.
    //
    // JS equality checking is insane, hence the need for all the
    // explicit String() constructor calls to make sure that we're
    // always dealing with strings and no implicit conversions are
    // taking place.
    //
    // For example:
    //
    // 1)  const a = " 1 ";          typeof is string
    //     const b = 1;              typeof is number
    //     a == b;                   true
    //
    // 2)  const a = " 1 ";          typeof is string
    //     const b = "1";            typeof is string
    //     a == b;                   false
    //
    // 3)  const a = String(" 1 ");  typeof is string
    //     const b = String(1);      typeof is string
    //     a == b;                   false
    //
    // Since we can't control if the caller is passing us
    // a number or a string we just always convert it to
    // a string before doing any comparisons, since we
    // want case #3 and don't want to allow case #1.
    const shouldSkipTooltip = (String(tooltipText) == String(elementText))
                           || (String(tooltipText).length == 0);

    const tooltip = shouldSkipTooltip ? "" : ` tabindex='0' data-tooltip-text="${GetTooltipTextWithReplacedGlobalVariables(tooltipText)}"`;

    if( elementTextFormatFunction !== undefined )
    {
        elementText = elementTextFormatFunction(elementText);
    }

    return `<span${tooltip}>${elementText}</span>`;
}

function GetInlineFormulaTooltipHTML(object, propertyName, elementTextFormatFunction)
{
    // Pass in the property name so that the formula property can be
    // automatically derived to reduce the chance of copy/paste errors.
    return GetInlineTooltipHTML( object[propertyName + "_Formula"], object[propertyName], elementTextFormatFunction );
}

//
// A helper function to find math expressions inside a long
// piece of text and replace them with a tooltip.
//
function ReplaceExpressionsWithEvaluatedTooltips(inputStr)
{
    let outputStr = "";

    let startingSearchIndex = 0;
    while( startingSearchIndex < inputStr.length)
    {
        const openBrace = inputStr.indexOf('{', startingSearchIndex);
        if( openBrace == -1 )
        {
            // No opening brace, so nothing left to do.
            outputStr += inputStr.substring(startingSearchIndex);
            break;
        }

        const closingBrace = inputStr.indexOf('}', openBrace);
        if( closingBrace == -1 )
        {
            // No closing brace, so nothing left to do.
            outputStr += inputStr.substring(startingSearchIndex);
            break;
        }

        // Copy over the string up to the start of the expression.
        outputStr += inputStr.substring(startingSearchIndex, openBrace);

        // Extract the expression, ignoring the '{' and '}' characters.
        const expression = inputStr.substring(openBrace + 1, closingBrace);

        // Evaluate the expression.
        const expressionValue = EvaluateString(expression);

        // Replace the expression string with a tooltip.
        outputStr += GetInlineTooltipHTML( expression, expressionValue );

        // Continue checking after the closing brace.
        startingSearchIndex = closingBrace + 1;
    }

    return outputStr;
}

// This function has enough edge cases that it's worth unit-testing.
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(``                                        ), ``                                                                                                          );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(` `                                       ), ` `                                                                                                         );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`a`                                       ), `a`                                                                                                         );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`{`                                       ), `{`                                                                                                         );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`}`                                       ), `}`                                                                                                         );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`{}`                                      ), `${GetInlineTooltipHTML("", "undefined")}`                                                                  );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`{}{}`                                    ), `${GetInlineTooltipHTML("", "undefined")}${GetInlineTooltipHTML("", "undefined")}`                          );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(` {} {} `                                 ), ` ${GetInlineTooltipHTML("", "undefined")} ${GetInlineTooltipHTML("", "undefined")} `                       );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`{1}`                                     ), `${GetInlineTooltipHTML("1", "1")}`                                                                         );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(` {1}`                                    ), ` ${GetInlineTooltipHTML("1", "1")}`                                                                        );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`{1} `                                    ), `${GetInlineTooltipHTML("1", "1")} `                                                                        );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(` {1} `                                   ), ` ${GetInlineTooltipHTML("1", "1")} `                                                                       );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(` { 1 } `                                 ), ` ${GetInlineTooltipHTML(" 1 ", "1")} `                                                                     );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`{1}{2}`                                  ), `${GetInlineTooltipHTML("1", "1")}${GetInlineTooltipHTML("2", "2")}`                                        );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`{1}{2 + 3}`                              ), `${GetInlineTooltipHTML("1", "1")}${GetInlineTooltipHTML("2 + 3", "5")}`                                    );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`{1} + {2} + {3}`                         ), `${GetInlineTooltipHTML("1", "1")} + ${GetInlineTooltipHTML("2", "2")} + ${GetInlineTooltipHTML("3", "3")}` );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`{1}+{2}+{3}`                             ), `${GetInlineTooltipHTML("1", "1")}+${GetInlineTooltipHTML("2", "2")}+${GetInlineTooltipHTML("3", "3")}`     );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`{1 + 2 + 3}`                             ), `${GetInlineTooltipHTML("1 + 2 + 3", "6")}`                                                                 );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`abc{1+(2*3)-4}def`                       ), `abc${GetInlineTooltipHTML("1+(2*3)-4", "3")}def`                                                           );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`You can cast this Math.min(1, 7) times`  ), `You can cast this Math.min(1, 7) times`                                                                    );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`You can cast this {Math.min(1, 7)} times`), `You can cast this ${GetInlineTooltipHTML("Math.min(1, 7)", "1")} times`                                    );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`You can cast this {Math.min(1, 7) times` ), `You can cast this {Math.min(1, 7) times`                                                                   );
AssertEq( ReplaceExpressionsWithEvaluatedTooltips(`You can cast this Math.min(1, 7)} times` ), `You can cast this Math.min(1, 7)} times`                                                                   );

function ToggleAllDetailsElements()
{
    // Simulate function static variables like in C++.
    if( typeof ToggleAllDetailsElements.IsOpen == 'undefined' )
    {
        ToggleAllDetailsElements.IsOpen = true;
    }

    // Toggle the state.
    ToggleAllDetailsElements.IsOpen = !ToggleAllDetailsElements.IsOpen;

    console.log("Setting all <details> to " + (ToggleAllDetailsElements.IsOpen ? "Open" : "Closed") );

    // Set the state of all the elements.
    for( const i of document.getElementsByTagName("details") )
    {
        i.open = ToggleAllDetailsElements.IsOpen;
    }

    // If we're closing all the elements then it's generally because
    // we're trying to view something specific and don't want to scroll
    // down the entire page. So have us scroll to the bottom automatically
    // and save some effort.
    if( !ToggleAllDetailsElements.IsOpen )
    {
        window.scrollTo
        ({
            left: 0,
            top: document.body.scrollHeight,
            behavior: 'smooth'
        });
    }
}

function GenerateCharacterNameHTML(character)
{
    return `
        <!-- Making the picture a secret button is pretty bad UX because it's in no way discoverable -->
        <!-- but I really don't want to find a place to put a dedicated button for this feature.     -->
        <img class='character-head-shot-picture' src='${character.HeadshotPicture}' onclick='ToggleAllDetailsElements()'>

        <span class='character-name'>${character.Name}</span>
    `;
}

function GenerateCharacterLevelHTML(character)
{
    // The input character has individual entries for each level. We want to combine
    // all the levels with the same class so that we can show a "condensed" summary.
    const mergedLevels = [];

    for( const classTaken of character.Levels )
    {
        const entry = mergedLevels.find( function(element)
        {
            return element.Class === classTaken;
        });

        // Existing entry, update.
        if( entry != undefined )
        {
            entry.CurrentAmount += 1;
        }
        // New entry, create.
        else
        {
            mergedLevels.push(
            {
                Class         : classTaken,
                CurrentAmount : 1,
            });
        }
    }

    const mergedLevelsStr = mergedLevels.map(function(entry)
    {
        return `${entry.Class} ${entry.CurrentAmount}`;
    })
    .join(", ");

    return `
        <div class='character-level'>Level ${character.Level} (${mergedLevelsStr})</div>
    `;
}

function GetBarStyle(currentValue, maxValue)
{
    const currentPercent = currentValue / maxValue * 100;

    let filledBarColor = `black`;
         if( currentPercent >= 75 ) { filledBarColor = `rgb(116, 198, 70)`; } // Green.
    else if( currentPercent >= 50 ) { filledBarColor = `rgb(244, 202, 22)`; } // Yellow.
    else if( currentPercent >= 25 ) { filledBarColor = `rgb(251, 149, 53)`; } // Orange.
    else                            { filledBarColor = `rgb(224,  55, 40)`; } // Red.

    const emptyBarColor = `transparent`;

    // Used to get an x% filled bar.
    return `background: linear-gradient(90deg, ${filledBarColor} ${currentPercent}%, ${emptyBarColor} 0%);`
}

function GenerateHealthBarHTML(character)
{
    return `
    <div class='health-bar' style='${GetBarStyle(character.Health.CurrentHP, character.Health.MaxHP)}'>
        Health:
        ${GetInlineFormulaTooltipHTML(character.Health, "CurrentHP")}
        /
        ${GetInlineFormulaTooltipHTML(character.Health, "MaxHP")}
    </div>`;
}

function GenerateStatsListHTML(character)
{
    const stats = Object.values(Stat).map(statName =>
    {
        const stat = character.Stats[statName];

        return `
        <div class='stat'>
            <div class='stat-name-and-modifier'>
                <div class='stat-name'    >${statName}</div>
                <div class='stat-modifier'>${GetInlineFormulaTooltipHTML(stat, "Modifier", PrefixNumberWithSign)}</div>
            </div>
            <div class='stat-value'>${GetInlineFormulaTooltipHTML(stat, "Value")}</div>
        </div>`;
    })
    .join("");

    return `
    <div class='stats-container'>
        ${stats}
    </div>
    `;
}

function GenerateSkillsListHTML(character)
{
    const skillsList = Object.values(Skill).map(skillName =>
    {
        const skill = character.Skills[skillName];

        const skillIsProficientStyle = skill.IsProficient ? '' : 'style="visibility:hidden"';

        const statForSkill = SkillToStat[skillName];

        const shortenedStatName = StatToShortStat[statForSkill];

        let displayedSkillName = skillName;

        // These are the only 2 skills that have spaces in the name so just handle these
        // cases manually to add the spaces when displaying the skill name to the user.
             if( displayedSkillName == Skill.AnimalHandling ) { displayedSkillName = 'Animal Handling'; }
        else if( displayedSkillName == Skill.SleightOfHand  ) { displayedSkillName = 'Sleight of Hand'; }

        // Add a tooltip only if there is a reminder for this skill.
        let skillExceptionReminder = character.SkillExceptionReminders[skillName];
        if( skillExceptionReminder === undefined )
        {
            skillExceptionReminder = displayedSkillName;
        }

        return `
        <tr class='skill'>
            <td>
                <div class='circle' title='Proficient' ${skillIsProficientStyle}>
            </td>
            <td class='skill-stat'    >(${shortenedStatName})</td>
            <td class='skill-name'    >${GetInlineTooltipHTML(skillExceptionReminder, displayedSkillName)}</td>
            <td class='skill-modifier'>${GetInlineFormulaTooltipHTML(skill, "Modifier", PrefixNumberWithSign)}</td>
        </tr>`;
    })
    .join("");

    return `
    <table class='skills-list'>
        <caption>Skills</caption>
        <tbody>
            ${skillsList}
        </tbody>
    </table>
    `;
}

function GenerateSavingThrowsListHTML(character)
{
    const GenerateSingleSavingThrowHTML = function(statName)
    {
        const stat = character.Stats[statName];

        const savingThrowIsProficientStyle = stat.SavingThrowIsProficient ? '' : 'style="visibility:hidden"';

        // Add a tooltip only if there is a reminder for this saving throw.
        let savingThrowExceptionReminder = character.SavingThrowExceptionReminders[statName];
        if( savingThrowExceptionReminder === undefined )
        {
            savingThrowExceptionReminder = statName;
        }

        return `
        <tr class='saving-throw'>
            <td>
                <div class='circle' title='Proficient' ${savingThrowIsProficientStyle}>
            </td>
            <td class='saving-throw-name'>${GetInlineTooltipHTML(savingThrowExceptionReminder, statName)}</td>
            <td class='saving-throw'     >${GetInlineFormulaTooltipHTML(stat, "SavingThrow", PrefixNumberWithSign)}</td>
        </tr>`;
    };

    const savingThrowsList = Object.values(Stat).map(GenerateSingleSavingThrowHTML).join("");

    return `
    <table class='saving-throws-list'>
        <caption>Saving Throws</caption>
        <tbody>
            ${savingThrowsList}
        </tbody>
    </table>
    `;
}

function GenerateSpellsListHTML(character)
{
    const GenerateSingleSpellLevelHTML = function(spells, level)
    {
        const GenerateSingleSpellHTML = function(spell)
        {
            const spellIsPreparedStyle      = spell.IsPrepared      ? '' : 'style="visibility:hidden"';
            const spellIsRitualStyle        = spell.IsRitual        ? '' : 'style="visibility:hidden"';
            const spellIsConcentrationStyle = spell.IsConcentration ? '' : 'style="visibility:hidden"';

            const iconsHTML = `
                <div class='spell-icons-container horizontal-spacing-container'>

                    <!--<div>Icons made by <a href="https://www.flaticon.com/authors/catalin-fertu" title="Catalin Fertu">Catalin Fertu</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>-->
                    <img class='spell-icon' ${spellIsPreparedStyle} title='Prepared' src="../../spell-is-prepared.svg"/>

                    <!--https://www.svgrepo.com/svg/102640/halloween-pentagram-->
                    <img class='spell-icon' ${spellIsRitualStyle} title='Ritual' src="../../spell-is-ritual.svg"/>

                    <!--<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>-->
                    <img class='spell-icon' ${spellIsConcentrationStyle} title='Concentration' src="../../spell-is-concentration.svg"/>

                </div>
            `;

            // The external website has a very specific naming convention.
            let adjustedSpellNameForWebsite = spell.Name;
            adjustedSpellNameForWebsite = adjustedSpellNameForWebsite.split(' ').join('-');
            adjustedSpellNameForWebsite = adjustedSpellNameForWebsite.split("'").join('');

            const spellWebsite = `http://dnd5e.wikidot.com/spell:${adjustedSpellNameForWebsite}`;

            return `
            <tr class='spell'>
                <td                           >${iconsHTML}</td>
                <td class='spell-name'        ><a href='${spellWebsite}' target="_blank">${spell.Name}</td>
                <td class='spell-casting-time'>${spell.CastingTime}</td>
                <td class='spell-range'       >${spell.Range}</td>
                <td class='spell-damage'      >${spell.Damage}</td>
                <td class='spell-type'        >${spell.Type}</td>
                <td class='spell-components'  >${spell.Components}</td>
                <td class='spell-duration'    >${spell.Duration}</td>
            </tr>`;
        };

        if( spells.length == 0 )
        {
            // No spells of that level.
            return "";
        }

        const spellsList = spells.map(GenerateSingleSpellHTML).join("");

        const tableTitle = (level == 0) ? "Cantrips" : `Level ${level} Spells`;

        return `
        <table class='spells-list'>
            <caption>${tableTitle}</caption>
            <thead>
                <tr>
                    <th class='min-content'></th>
                    <th                    >Name</th>
                    <th                    >Casting Time</th>
                    <th                    >Range</th>
                    <th                    >Damage</th>
                    <th                    >Type</th>
                    <th                    >Components</th>
                    <th                    >Duration</th>
                </tr>
            </thead>
            <tbody>
                ${spellsList}
            </tbody>
        </table>
         `;
    };

    const spellsList = character.Spells.map(GenerateSingleSpellLevelHTML).join("");
    if( spellsList.length == 0 )
    {
        // No spells at all.
        return "";
    }

    return `
    <details open>
        <summary><span>Spell List</span></summary>
        <div class='spells-container vertical-spacing-container'>
            ${spellsList}
        </div>
    </details>
    `;
}

function GenerateCharacterBioHTML(character)
{
    const bioSectionsHTML = Object.keys(character.Bio).map(bioSectionName =>
    {
        const bioSection = character.Bio[bioSectionName];

        // This is a pretty hacky special case to let us be able
        // to embed the character picture into the flexbox layout
        // while still allowing the user to control the order in
        // which it appears (by changing the variable order in the
        // character sheet file).
        //
        // The flexbox "order" property could also be used to do
        // this but we don't want to expose user-editable CSS files
        // since there's already enough for the user to deal with
        // when setting up a new character sheet.
        if( bioSectionName == "Picture" )
        {
            return `<img class='bio-section-picture' src='${bioSection}'>`;
        }
        else
        {
            return `
            <figure class='bio-section'>
                <figcaption>${bioSectionName}</figcaption>
                <ul>
                    ${bioSection.map(entry => `<li>${entry}</li>`).join("")}
                </ul>
            </figure>
            `;
        }
    })
    .join("");

    if( bioSectionsHTML.length == 0 )
    {
        // No data at all.
        return "";
    }

    return `
    <details open>
        <summary><span>Character Bio<span></summary>
        <div class='bio-section-list'>
            ${bioSectionsHTML}
        </div>
    </details>
    `;
}

function GenerateHitDiceListHTML(character)
{
    const GenerateSingleHitDieSizeHTML = function(hitDieData)
    {
        const barStyle = GetBarStyle(hitDieData.CurrentAmount, hitDieData.MaxAmount);

        return `
        <tr class='hit-die'>
            <td class='hit-die-size'                      >d${hitDieData.Size}</td>
            <td class='hit-die-roll-mod'                  >${GetInlineFormulaTooltipHTML(character, "HitDiceRollModifier", PrefixNumberWithSpaceAndSign)}</td>
            <td class='hit-die-amount' style='${barStyle}'>${hitDieData.CurrentAmount} / ${hitDieData.MaxAmount}</td>
        </tr>`;
    }

    const hitDiceList = character.Health.SortedHitDiceList.map(GenerateSingleHitDieSizeHTML).join("");

    return `
    <table class='hit-dice-list'>
        <caption>Hit Dice</caption>
        <thead>
            <tr>
                <th colspan="2">Size</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            ${hitDiceList}
        </tbody>
    </table>
    `;
}

function GenerateSpellSlotsListHTML(character)
{
    const GenerateSingleSpellSlotLevelHTML = function(spellSlotData, index)
    {
        if( spellSlotData.Max == 0 )
        {
            // Don't have any of this spell slot level.
            return "";
        }

        // It's most convenient for the user to track the number of slots
        // spent (it makes resetting after a rest simple) but we still want
        // to display the number of slots left, since that's the easiest way
        // for the user to interpret the data.
        const numSpellSlotsRemaining = spellSlotData.Max - spellSlotData.NumSpent;

        const barStyle = GetBarStyle(numSpellSlotsRemaining, spellSlotData.Max);

        return `
        <tr class='spell-slot'>
            <td class='spell-slot-level'                     >${index + 1}</td>
            <td class='spell-slot-amount' style='${barStyle}'>${numSpellSlotsRemaining} / ${spellSlotData.Max}</td>
        </tr>`;
    }

    const spellSlotList = character.SpellSlots.map(GenerateSingleSpellSlotLevelHTML).join("");

    if( spellSlotList == "" )
    {
        // Don't have any spell slots so don't show anything.
        return "";
    }

    return `
    <table class='spell-slot-list'>
        <caption>Spell Slots</caption>
        <thead>
            <tr>
                <th>Level</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            ${spellSlotList}
        </tbody>
    </table>
    `;
}

function GenerateHasInspirationHTML(character)
{
    // Could use "☒" instead of the empty box character.
    const hasInspirationSymbol = character.InspirationReason ? "☑" : "☐";

    return `
    <div class='has-inspiration-container'>
        <figcaption>Inspiration?</figcaption>
        <div class='has-inspiration-symbol'>
            ${GetInlineTooltipHTML(character.InspirationReason || "No Inspiration", hasInspirationSymbol)}
        </div>
    </div>
    `;
}

function GenerateMiscAttributesHTML(character)
{
    const GetMiscAttributeHTML = function(header, value)
    {
        return `
        <tr>
            <td class='misc-attribute-header'>${header}</td>
            <td class='misc-attribute-value' >${value}</td>
        </tr>
        `;
    };

    // Not all characters have data for all the things (e.g. a Barbarian
    // doesn't have a Spell Save DC) so display nothing in that case.
    const EmptyStringIfUndefined = function(valueToTest, outputIfDefined)
    {
        if( valueToTest === undefined )
        {
            return "";
        }
        else
        {
            return outputIfDefined;
        }
    };

    return `
    <table class='misc-attribute-list'>
        <caption>Misc</caption>
        <tbody>
            ${                                                    GetMiscAttributeHTML("AC"             , GetInlineFormulaTooltipHTML(character, "AC"                                    )) }
            ${                                                    GetMiscAttributeHTML("Initiative"     , GetInlineFormulaTooltipHTML(character, "Initiative"      , PrefixNumberWithSign)) }
            ${                                                    GetMiscAttributeHTML("Proficiency"    , GetInlineFormulaTooltipHTML(character, "ProficiencyBonus", PrefixNumberWithSign)) }
            ${EmptyStringIfUndefined(character.AttackBonus      , GetMiscAttributeHTML("Attack Bonus"   , GetInlineFormulaTooltipHTML(character, "AttackBonus"     , PrefixNumberWithSign)))}
            ${EmptyStringIfUndefined(character.SaveDC           , GetMiscAttributeHTML("Save DC"        , GetInlineFormulaTooltipHTML(character, "SaveDC"                                )))}
            ${EmptyStringIfUndefined(character.NumSpellsPrepared, GetMiscAttributeHTML("Spells Prepared", GetInlineFormulaTooltipHTML(character, "NumSpellsPrepared"                     )))}
        </tbody>
    </table>
    `;
}

function GeneratePhysicalAttributesHTML(character)
{
    const pa = character.PhysicalAttributes;

    const GetPhysicalAttributeHTML = function(name, value, unit)
    {
        if( value == 0 )
        {
            // Not all characters have data for all the things (e.g. a Halfling
            // doesn't have darkvision) so display nothing in that case.
            return "";
        }

        return `
        <tr class='physical-attribute'>
            <td class='physical-attribute-name' >${name}</td>
            <td class='physical-attribute-value'>${value}</td>
            <td class='physical-attribute-unit' >${unit}</td>
        </tr>
        `;
    };

    return `
    <table class='physical-attributes-list'>
        <caption>Physical Attributes</caption>

        ${GetPhysicalAttributeHTML("Speed"                   , pa.MoveSpeed                                                          , "ft"   )}
        ${GetPhysicalAttributeHTML("Flying Speed"            , pa.FlyingSpeed                                                        , "ft"   )}
        ${GetPhysicalAttributeHTML("Climb Speed"             , GetInlineFormulaTooltipHTML(pa, "ClimbSpeed"                         ), "ft"   )}
        ${GetPhysicalAttributeHTML("Swim Speed"              , GetInlineFormulaTooltipHTML(pa, "SwimSpeed"                          ), "ft"   )}
        ${GetPhysicalAttributeHTML("Dark Vision"             , pa.DarkVisionDistance                                                 , "ft"   )}
        ${GetPhysicalAttributeHTML("Height"                  , pa.Height                                                             , "ft"   )}
        ${GetPhysicalAttributeHTML("Weight"                  , pa.Weight                                                             , "lbs"  )}
        ${GetPhysicalAttributeHTML("Age"                     , pa.Age                                                                , "years")}
        ${GetPhysicalAttributeHTML("Carrying Capacity"       , GetInlineFormulaTooltipHTML(pa, "CarryingCapacity"                   ), "lbs"  )}
        ${GetPhysicalAttributeHTML("Push/Pull/Lift"          , GetInlineFormulaTooltipHTML(pa, "PushPullLiftWeight"                 ), "lbs"  )}
        ${GetPhysicalAttributeHTML("Long Jump (Run)"         , GetInlineFormulaTooltipHTML(pa, "LongJumpRunning"                    ), "ft"   )}
        ${GetPhysicalAttributeHTML("Long Jump (Stand)"       , GetInlineFormulaTooltipHTML(pa, "LongJumpStanding"                   ), "ft"   )}
        ${GetPhysicalAttributeHTML("High Jump Land (Run)"    , GetInlineFormulaTooltipHTML(pa, "HighJumpRunningNoCharacterHeight"   ), "ft"   )}
        ${GetPhysicalAttributeHTML("High Jump Land (Stand)"  , GetInlineFormulaTooltipHTML(pa, "HighJumpStandingNoCharacterHeight"  ), "ft"   )}
        ${GetPhysicalAttributeHTML("High Jump Pullup (Run)"  , GetInlineFormulaTooltipHTML(pa, "HighJumpRunningWithCharacterHeight" ), "ft"   )}
        ${GetPhysicalAttributeHTML("High Jump Pullup (Stand)", GetInlineFormulaTooltipHTML(pa, "HighJumpStandingWithCharacterHeight"), "ft"   )}
    </table>
    `;
}

function GenerateAttacksListHTML(character)
{
    const attacksList = character.Attacks.map(attack =>
    {
        const shouldShowDamageBonus = (attack.DamageBonus != attack.DamageBonus_Formula) // There's some formula going on that we should show to the user.
                                   || (attack.DamageBonus != 0);                         // Showing a "+ 0" is pointless and confusing.

        const damageBonusHTML = shouldShowDamageBonus ? ` ${GetInlineFormulaTooltipHTML(attack, "DamageBonus", PrefixNumberWithSpaceAndSign)}` // Note the leading space.
                                                      : '';
        return `
        <tr>
            <td>${attack.Name}</td>
            <td>${GetInlineFormulaTooltipHTML(attack, "HitBonus", PrefixNumberWithSign)}</td>
            <td>${attack.DamageDice}${damageBonusHTML}</td>
            <td>${attack.Range}</td>
            <td>${attack.DamageType}</td>
        </tr>`;
    })
    .join("");

    if( attacksList == "" )
    {
        // We don't have any attacks so don't show anything.
        return "";
    }

    return `
    <table class='attack-list'>
        <caption>Attacks</caption>
        <thead>
            <tr>
                <th>Name</th>
                <th>Hit Bonus</th>
                <th>Damage</th>
                <th>Range</th>
                <th>Damage Type</th>
            </tr>
        </thead>
        <tbody>
            ${attacksList}
        </tbody>
    </table>
    `;
}

function GenerateSavingThrowAttacksListHTML(character)
{
    const savingThrowAttacksList = character.SavingThrowAttacks.map(savingThrowAttack =>
    {
        const shouldShowDamageBonus = (savingThrowAttack.DamageBonus != savingThrowAttack.DamageBonus_Formula) // There's some formula going on that we should show to the user.
                                   || (savingThrowAttack.DamageBonus != 0);                                    // Showing a "+ 0" is pointless and confusing.

        const damageBonusHTML = shouldShowDamageBonus ? ` ${GetInlineFormulaTooltipHTML(savingThrowAttack, "DamageBonus", PrefixNumberWithSpaceAndSign)}` // Note the leading space.
                                                      : '';
        return `
        <tr>
            <td>${savingThrowAttack.Name}</td>
            <td>${savingThrowAttack.Stat} ${GetInlineFormulaTooltipHTML(savingThrowAttack, "DC")}</td>
            <td>${savingThrowAttack.DamageDice}${damageBonusHTML}</td>
            <td>${savingThrowAttack.Range}</td>
            <td>${savingThrowAttack.DamageType}</td>
            <td>${savingThrowAttack.OnSuccess}</td>
        </tr>`;
    })
    .join("");

    if( savingThrowAttacksList == "" )
    {
        // We don't have any saving throw attacks so don't show anything.
        return "";
    }

    return `
    <table class='saving-throw-attack-list'>
        <caption>Saving Throw Attacks</caption>
        <thead>
            <tr>
                <th>Name</th>
                <th>DC</th>
                <th>Damage</th>
                <th>Range</th>
                <th>Damage Type</th>
                <th>On Success</th>
            </tr>
        </thead>
        <tbody>
            ${savingThrowAttacksList}
        </tbody>
    </table>
    `;
}

function GenerateResourceListHTML(character)
{
    const GenerateSingleResourceHTML = function(resource)
    {
        // It's most convenient for the user to track the number of slots
        // spent (it makes resetting after a rest simple) but we still want
        // to display the number of slots left, since that's the easiest way
        // for the user to interpret the data.
        const numResourcesRemaining = resource.Max - resource.NumSpent;

        const barStyle = GetBarStyle(numResourcesRemaining, resource.Max);

        return `
        <tr class='resource'>
            <td class='resource-name'                     >${resource.Name}</td>
            <td class='resource-count' style='${barStyle}'>${numResourcesRemaining} / ${GetInlineFormulaTooltipHTML(resource, "Max")}</td>
            <td class='resource-recovery'                 >${resource.Recovery}</td>
        </tr>`;
    };

    const resourceList = character.Resources.map(GenerateSingleResourceHTML).join("");

    if( resourceList == "" )
    {
        // Don't have any resources so don't show anything.
        return "";
    }

    return `
    <table class='resources-list'>
        <caption>Resources</caption>
        <thead>
            <tr>
                <th>Name</th>
                <th>Amount</th>
                <th>Recharge On</th>
            </tr>
        </thead>
        <tbody>
            ${resourceList}
        </tbody>
    </table>
    `;
}

function GenerateRestOptionsListHTML(character)
{
    const restOptionSectionsHTML = Object.keys(character.RestOptions).map(restOptionSectionName =>
    {
        const restOptionSection = character.RestOptions[restOptionSectionName];

        const restOptionsHTML = restOptionSection.map(restOption =>
        {
            return `<li>${restOption}</li>`;
        })
        .join("");

        return `
        <figure class='rest-options'>
            <figcaption>${restOptionSectionName}</figcaption>
            <ul>
                ${restOptionsHTML}
            </ul>
        </figure>
        `;
    })
    .join("");

    return `
    <div class='rest-options-list vertical-spacing-container'>
        ${restOptionSectionsHTML}
    </div>
    `;
}

function GenerateCombatOptionsListHTML(character)
{
    const combatOptionSectionsHTML = Object.keys(character.CombatOptions).map(combatOptionSectionName =>
    {
        const combatOptionSection = character.CombatOptions[combatOptionSectionName];

        const combatOptionsHTML = combatOptionSection.map(combatOption =>
        {
            return `<li>${combatOption}</li>`;
        })
        .join("");

        if( combatOptionsHTML == "" )
        {
            // Don't have anything in that section so don't show anything.
            return "";
        }

        return `
        <figure class='combat-options'>
            <figcaption>${combatOptionSectionName}</figcaption>
            <ul>
                ${combatOptionsHTML}
            </ul>
        </figure>
        `;
    })
    .join("");

    return `
    <div class='combat-options-list vertical-spacing-container'>
        ${combatOptionSectionsHTML}
    </div>
    `;
}

function GenerateExternalLinksHTML(character)
{
    const externalLinksHTML = Object.keys(character.ExternalLinks).map(linkTitle =>
    {
        const linkContents = character.ExternalLinks[linkTitle];

        return `<li><a href='${linkContents}' target="_blank">${linkTitle}</a></li>`;
    })
    .join("");

    return `
    <figure class='external-links'>
        <figcaption>External Links</figcaption>
        <ul>
            ${externalLinksHTML}
        </ul>
    </figure>
    `;
}

function GenerateFeaturesListHTML(character)
{
    const featureSectionsHTML = Object.keys(character.Features).map(featureSectionName =>
    {
        const featureSection = character.Features[featureSectionName];

        const featureSectionHTML = featureSection.map(feature =>
        {
            return `
                <li><b>${feature.Name}</b></li>
                <ul>
                    ${feature.Desc.map(d => `<li>${ReplaceExpressionsWithEvaluatedTooltips(d)}</li>`).join("")}
                </ul>
            `;
        })
        .join("");

        return `
        <figure class='feature-section'>
            <figcaption>${featureSectionName}</figcaption>
            <ul>
                ${featureSectionHTML}
            </ul>
        </figure>
        `;
    })
    .join("");

    return `
    <div class='features-list vertical-spacing-container'>
        ${featureSectionsHTML}
    </div>
    `;
}

function GenerateCampaignNotesHTML(character)
{
    const rightNow = new Date();

    const sessionSectionsHTML = Object.keys(character.CampaignNotes).map( (sessionNotesSectionName, index) =>
    {
        const sessionNotesSection = character.CampaignNotes[sessionNotesSectionName];

        const sessionDate = new Date(sessionNotesSectionName);

        // https://stackoverflow.com/a/3224854
        //
        // Not 100% correct in all cases but we don't care about things
        // like timezones, leap years, leap seconds, etc...
        const diffTime = rightNow - sessionDate;
        const diffDays = (diffTime / (1000 * 60 * 60 * 24)).toFixed(0);

        const sessionNotesSectionHTML = sessionNotesSection.map(sessionNote =>
        {
            return `<li>${sessionNote}</li>`;
        })
        .join("");

        const sessionNumber = index + 1;

        return `
        <figure class='session-notes'>
            <figcaption>#${sessionNumber} ${sessionDate.toDateString()} (${diffDays} days ago)</figcaption>
            <ul>
                ${sessionNotesSectionHTML}
            </ul>
        </figure>
        `;
    })
    .join("");

    if( sessionSectionsHTML == "" )
    {
        // Don't have any notes so don't show anything.
        return "";
    }

    return `
    <details open>
        <summary><span>Campaign Notes<span></summary>
        <div class='campaign-notes vertical-spacing-container'>
            ${sessionSectionsHTML}
        </div>
    </details>
    `;
}

function GenerateInventoryHTML(character)
{
    const inventorySectionsHTML = Object.keys(character.Inventory).map(inventorySectionName =>
    {
        const inventorySection = character.Inventory[inventorySectionName];

        let totalCost = 0;
        let totalWeight = 0;

        const inventorySectionHTML = inventorySection.map(entry =>
        {
            totalCost   += entry.Cost   * entry.Amount;
            totalWeight += entry.Weight * entry.Amount;

            return `
            <tr class='inventory-entry'>
                <td class='inventory-entry-name'       >${entry.Name}</td>
                <td class='inventory-entry-amount'     >${entry.Amount}</td>
                <td class='inventory-entry-weight'     >${entry.Weight}</td>
                <td class='inventory-entry-cost'       >${entry.Cost}</td>
                <td class='inventory-entry-description'>${entry.Description}</td>
            </tr>`;
        })
        .join("");

        return `
        <table class='inventory-section'>
            <caption>${inventorySectionName}</caption>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Weight (lbs)</th>
                    <th>Cost (GP)</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                ${inventorySectionHTML}

                <!-- Add a summary of everything in the section as a table footer.        -->
                <!-- Keep it as a row in the table (even though we're using just a single -->
                <!-- table cell) so that we still get the zebra-striping style applied.   -->
                <tr>
                    <td colspan='5'>
                        <div class='inventory-section-footer'>
                            <span>Total Cost: ${totalCost.toFixed(1)} GP</span>
                            <span>Total Weight: ${totalWeight.toFixed(1)} lbs</span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        `;
    })
    .join("");

    return `
    <details open>
        <summary><span>Inventory</span></summary>
        <div class='inventory-container vertical-spacing-container'>
            ${inventorySectionsHTML}
        </div>
    </details>
    `;
}

function GenerateProficienciesListHTML(character)
{
    const GenerateProficiencySectionHTML = function(proficiencySectionName)
    {
        const proficiencySection = character.Proficiencies[proficiencySectionName];

        if( proficiencySection.length == 0 )
        {
            // No proficiencies, don't bother showing anything.
            return;
        }

        const GenerateSingleProficiencyHTML = function(proficiency)
        {
            return `<li>${proficiency}</li>`;
        };

        const proficiencySectionHTML = proficiencySection.map(GenerateSingleProficiencyHTML).join("");

        return `
        <li><b>${proficiencySectionName}</b>
            <ul>
                ${proficiencySectionHTML}
            </ul>
        </li>
        `;
    };

    const proficienciesListHTML = Object.keys(character.Proficiencies).map(GenerateProficiencySectionHTML).join("");

    return `
    <div class='proficiencies'>
        <figure>
            <figcaption>Proficiencies</figcaption>
            <ul>
                ${proficienciesListHTML}
            </ul>
        </figure>
    </div>
    `;
}

function GenerateCharacterSheetHTML(character)
{
    return `
    <!-- Make it easier to see if content is going to be displayed -->
    <!-- below the fold on my specific laptop screen. See the CSS  -->
    <!-- file for a more detailed explanation.                     -->
    <div class='laptop-window-size-marker'></div>

    <div class='vertical-spacing-container'>
        <!-- The 'header-container' has a lot of whitespace in it     -->
        <!-- already so we can put the 'stats-container' as a sibling -->
        <!-- element so that the 'vertical-spacing-container' doesn't -->
        <!-- add even more space between them.                        -->
        <div>
            <div class='header-container'>
                <div class='name-and-health-container'>
                    ${GenerateCharacterNameHTML(character)}
                    ${GenerateHealthBarHTML(character)}
                </div>
                ${GenerateCharacterLevelHTML(character)}
            </div>

            ${GenerateStatsListHTML(character)}
        </div>

        <div class='above-fold-container'>
            <div class='vertical-flex-horizontal-center vertical-spacing-container'>
                ${GenerateSavingThrowsListHTML(character)}
                ${GenerateMiscAttributesHTML(character)}
                ${GenerateHasInspirationHTML(character)}
            </div>

            ${GenerateSkillsListHTML(character)}

            <div class='vertical-flex-horizontal-center vertical-spacing-container'>
                ${GenerateAttacksListHTML(character)}
                ${GenerateSavingThrowAttacksListHTML(character)}

                <div class='resources-hit-dice-spell-slots-container'>
                    ${GenerateResourceListHTML(character)}
                    ${GenerateHitDiceListHTML(character)}
                    ${GenerateSpellSlotsListHTML(character)}
                </div>
            </div>
        </div>

        <details open>
            <summary><span>Character Options</span></summary>
            <div class='all-options-container'>
                ${GenerateFeaturesListHTML(character)}

                <div class='vertical-flex-horizontal-center vertical-spacing-container'>
                    ${GenerateCombatOptionsListHTML(character)}
                    ${GenerateExternalLinksHTML(character)}
                </div>

                <!-- None of these elements are expected to be very long -->
                <!-- so we can stack them on top of each other.          -->
                <div class='vertical-flex-horizontal-center vertical-spacing-container'>
                    ${GenerateRestOptionsListHTML(character)}
                    ${GenerateProficienciesListHTML(character)}
                    ${GeneratePhysicalAttributesHTML(character)}
                <div>
            </div>
        </details>

        ${GenerateInventoryHTML(character)}
        ${GenerateSpellsListHTML(character)}
        ${GenerateCharacterBioHTML(character)}
        ${GenerateCampaignNotesHTML(character)}
    </div>
    `;
}

function SetupElementTooltipPosition(element)
{
    // If it has a tooltip...
    if( element.hasAttribute("data-tooltip-text") )
    {
        // Figure out what side of the screen it is on.
        var rect = element.getBoundingClientRect();

        const rectCenterX = (rect.left + rect.right) / 2;

        const windowCenterX = window.innerWidth / 2;

        const isOnLeftSideOfWindow = rectCenterX < windowCenterX;

        // Setup a "marker" attribute so that we can position the tooltip
        // correctly in the CSS. This is to try and reduce the chance that
        // that the tooltip text overflows off the side of the window and
        // becomes unreadable.
        //
        // NOTE: This check is only done once, so even though an element is
        // on a given half of the window right now, that may change in the future
        // and the tooltip would end up being incorrectly positioned.
        //
        // This limitation is OK because we hardcode all of our widths and we
        // don't expect to do much horizontal scrolling, so the element horizontal
        // positions shouldn't chance too much. If this changes in the future then
        // we will have to do all the tooltip displaying dynamically in JS rather
        // than having it done automatically inside the CSS.
        const newAttributeName = "data-tooltip-justify-" + (isOnLeftSideOfWindow ? "right" : "left");

        element.setAttribute(newAttributeName, "");
    }

    // Recurse deeper into the DOM.
    for (const c of element.children)
    {
        SetupElementTooltipPosition(c);
    }
}

function CheckForUndefinedOrNan(element)
{
    const innerHTML = element.innerHTML;

    const badStringsList = ["undefined", "NaN", "Infinity"];

    for( const badString of badStringsList )
    {
        const index = innerHTML.search(badString);
        if( index == -1 )
        {
            // Bad string not found.
            continue;
        }

        const contexLength = 200;

        const contextStart = Math.max(index - contexLength, 0               );
        const contextEnd   = Math.min(index + contexLength, innerHTML.length);

        const context = innerHTML.substring(contextStart, contextEnd);

        CrashThePage(`Found the string "${badString}"!`, context);
    }
}

function GenerateCharacterSheet(character)
{
    var t0 = performance.now();

    // Setup favicon since it can't be set in the CSS and we don't want
    // to force each character sheet to do it manually themselves.
    var faviconLink = document.createElement("link");
    faviconLink.rel = "shortcut icon";
    faviconLink.href = "../../favicon.ico";
    document.head.appendChild(faviconLink);

    // Finish setting up the properties.
    console.log("Generating character sheet for:");
    console.log(character);

    DefineAutoGeneratedAndGlobalProperties(character);

    console.log("After auto-generated and global properties:");
    console.log(character);
    console.log(window);

    // Misc page setup.
    document.title = character.Name;

    // Build up the HTML text for the character sheet
    const characterSheetHTML = GenerateCharacterSheetHTML(character);

    // Actually add it to the webpage.
    const node = document.createElement("div");
    node.innerHTML = characterSheetHTML;
    document.body.appendChild(node);

    // Now that all the HTML is attached to the DOM we can start querying
    // the element's layout position to decide where to position the tooltips.
    SetupElementTooltipPosition(node);

    // Sanity check to catch any math or formula errors.
    CheckForUndefinedOrNan(node);

    var t1 = performance.now();

    console.log(`GenerateCharacterSheet took ${t1 - t0}ms`);
}

GenerateCharacterSheet(Character);
