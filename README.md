# README #

### What is this repository for? ###

This is a character sheet viewer for D&D 5e.

I made it because none of the existing character sheets I found solved all these problems:

* Free. Eliminates [D&D Beyond](https://www.dndbeyond.com/).
* Auto-calculate fields (e.g. skill modifiers, remaining health, attack bonuses) so that I don't have to remember what needs to change every time I level up. Eliminates any [paper-based solution](https://dnd.wizards.com/products/tabletop-games/trpg-resources/character-sheets).
* Responsive design so that fields can expand or shrink to fit the content. This makes having lists of things (e.g. inventory, character features, spells, etc...) much easier. Eliminates [PDF-based solutions](https://dnd.wizards.com/products/tabletop-games/trpg-resources/character-sheets).
* Having information be easy to find, generally by having only a single page. It's a huge pain to have to constantly click through 10 different tabs trying to find where the heck your saving throw modifiers are. Eliminates [Adventurer's Codex](https://app.adventurerscodex.com).
* Easy to get up and running. Manually setting up a rules engine is tedious and solves problems that I don't generally encounter in my D&D games. Eliminates [DiceCloud](https://dicecloud.com) and [http://crobi.github.io/misc/charsheet](http://crobi.github.io/misc/charsheet).
* Not horribly ugly. Having a little graphic design helps display information in a way that makes it easier to digest. Eliminates any spreadsheet-based solution, such as [Forged Anvil](http://www.theroguecollective.net/scrolls-o-info/forged-anvil-sheets/).
* Actually works. I used to use [https://character-sheet-dnd5.appspot.com/](https://character-sheet-dnd5.appspot.com/) and it randomly stopped working one day. I was very sad.

Additional features that are nice but not required are:

* Usable without Internet access. Eliminates most web-based solutions.
* Easy to share with other people (e.g. the DM). Eliminates most web-based or paper-based solutions.

The downsides of using this character sheet compared to existing ones are:

* Not very user friendly to edit the character sheet (it's JSON). There are no UI controls in the character sheet that let you edit it, it's all controlled by the text file.
* No built-in sync/backup functionality. This makes it more difficult to use it across multiple different computers.
* Not mobile/tablet friendly.
* Very dense design, lots of information displayed in a small area.

These are trade-offs that I am OK with making but you may not be.

### How do I view the character sheet? ###

Open `/characters/<Your Character>/<Your Character>.html` in your favorite web browser.

The character sheet has been tested on the following browsers, other browsers may or may not work.

* Firefox 71.0 (Windows 7)
* Google Chrome 79.0 (Windows 7)

**NOTE:** The webpage is configured to automatically reload the page whenever it gains focus. This is intentional, it makes the user flow of "looking at the webpage, tab away to edit it, tab back to view changes" seamless, since the user doesn't have to explicitly reload the page and there's no risk of seeing stale data.

### How do I edit the character sheet? ###

Open `/characters/<Your Character>/<Your Character>.js` in your favorite text editor.

The character data is stored as a giant Javascript object literal so knowing Javascript or JSON would be helpful. It is explicitly a .js file rather than .json so that comments can be included.

Any fields that end in "\_Formula" are treated specially. They will instead have their calculated value displayed in the webpage with an underline, and if you hover over these values a tooltip will appear showing you the original formula.

You can also specify an inline formula by wrapping the text in "\{" and "\}". Currently this is only supported for text inside the "Features" section.

Variables you can reference in a formula are

* Level
* \[CLASS\]Level (e.g. "DruidLevel")
* ProficiencyBonus or ProfBonus
* \[STAT\] (e.g. "Strength")
* \[SHORT STAT\] (e.g. "Str")
* \[STAT\]Modifier (e.g. "StrengthModifier")
* \[STAT\]Mod (e.g. "StrengthMod")
* \[SHORT STAT\]Modifier (e.g. "StrModifier")
* \[SHORT STAT\]Mod (e.g. "StrMod")
* AttackBonus
* SaveDC
* \[SKILL\] (e.g. "Acrobatics" or "AnimalHandling")
* HitDiceMax
* HitDiceSpent
* Height
* MoveSpeed
* FlyingSpeed
* SwimSpeed
* ClimbSpeed

### How do I make a new character sheet? ###

The easiest way is to make a copy of the folder `/characters/New Character Template` and edit the data in the template to match your new character.

Assuming your new character's name is "Dungeon McDragon", you will need to make these changes to your copied files:

* Rename the folder `/characters/New Character Template` to `/characters/Dungeon McDragon`
* Rename the file `New Character Template Headshot.png` to `Dungeon McDragon Headshot.png`
* Rename the file `New Character Template.png` to `Dungeon McDragon.png`
* Rename the file `New Character Template.html` to `Dungeon McDragon.html`
* Rename the file `New Character Template.js` to `Dungeon McDragon.js`

Inside `New Character Template.html` you'll need to change the line

`<script src="New Character Template.js"></script>` to `<script src="Dungeon McDragon.js"></script>`

### Common Problems ###

**I edited my character sheet and now it's blank when I look at it in the web browser!**

This is almost certainly because the web browser is failing to parse your character sheet as syntactically correct Javascript. There are many ways this can happen and lots of them are fairly subtle like forgetting a comma somewhere.

The second most common cause is one of the formulas in the character sheet not evaluating to a proper value (e.g. it's the empty string).

In either case, something that might be helpful is opening the web browser's debug console to look at the error message directly. The error message can sometimes be pretty cryptic, so this doesn't always work.

If you're making large edits to your character sheet the best suggestion is to check the character sheet webpage often while making the changes. That way if it breaks you'll have a good idea of where the break is (i.e. the section you just edited) which greatly helps with debugging.

### What does it look like? ###

A screenshot of an example character is below.

![](./screenshot.png "Screenshot")

