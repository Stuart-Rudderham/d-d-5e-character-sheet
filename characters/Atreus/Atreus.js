//
// Data that need to be updated often (e.g. every round of combat) is at the
// top of the file for easy editing.
//
const DataThatChangesOften =
{
    ////////////////////////
    // This is your heath-related info.
    ////////////////////////
    Health:
    {
        CurrentDamage_Formula : "11 + 5 - 4 - 11 - 10 + 5 + 9 + 7 + 10 + 11 - 20",
        TempHP                : 0,
        NumHitDiceSpent       : 2,
    },

    ////////////////////////
    // These are the resources that you can spend in or out of combat which replenish after resting.
    ////////////////////////
    Resources:
    [
        { Name: "Fey Step"                , NumSpent: 1, Max_Formula: "1", Recovery: "Short Rest" }, // Eladrin.
        { Name: "Hellish Rebuke"          , NumSpent: 1, Max_Formula: "1", Recovery: "Long Rest" }, // Jake's homebrew Common Fire enchantment for Jewelry.
        { Name: "Cold Arrow Infusion"     , NumSpent: 2, Max_Formula: "3", Recovery: "Long Rest" }, // Jake's homebrew Common Ice enchantment for Ranged Weapons.
        { Name: "Lightning Pants Infusion", NumSpent: 0, Max_Formula: "3", Recovery: "Long Rest" }, // Jake's homebrew Common Electric enchantment for Armor.
    ],

    ////////////////////////
    // These are your spell slots.
    ////////////////////////
    SpellSlots:
    [
    ],

    ////////////////////////
    // Record if you have inspiration, and if so, how you got it.
    ////////////////////////
    InspirationReason: "",

    ////////////////////////
    // These are the things that have happened to you, organized by session.
    //
    // Each session header is the date that it happened. The date format is not
    // ISO 8601 (e.g. "2019-12-25") because that is always in the UTC timezone.
    // By using an implementation specific format (e.g. "Dec 25, 2019") the local
    // timezone is assumed. Firefox and Chrome seem to give consistent results
    // so that's good enough for us.
    ////////////////////////
    CampaignNotes:
    {
        "September 16, 2021":
        [
            "Made my character sheet!",
        ],

        "September 19, 2021":
        [
            "Keeper Rankings: Retrievers -> Seekers -> Preserver",
            "Preserver Miranda has given us our first quest",
            "Traveling to the Soltaro Mountains to go get DOOMs",
            "Specifically Mountain Koana, home to warring golaith tribes",
            "Don't know if the golaith are the owners or victims of the DOOMs",
            "Circle of teleportation nearby, can't be accessed, powerful magics messing it up",
            "Figure out what the DOOM, retrieve it at ALL COSTS",
            "Return back using the (now operable) teleportation circle",
            "Don't talk about the Keepers, could come up with a lie about why we are there. Could also go loud and get out before people notice",
            "Preserver Blackboot runs the armory. Can buy normal things but she can also craft magic weapons",
            "She gave us a magical tattoo that lets us collect magic energy, that she will be able to use to craft things",
            "Fought some steam Miphets, took their essence",
            "Preserver Flora is the person who extracts the essence from us so that Blackboot can use it",
            "Papa bought me an amulet, which I infused with a Common Fire enchantment",
            "Miranda has suggested to not stay too long, nights get super cold",
            "Gave a scroll of teleportation so that we can get back",
            "Teleported into a large tent, filled with scared golaiths and one (corrupted) golaith killing another",
            "We killed the corrupted one",
            "The corrupted ones have taken the shaman, they want us to rescue them",
            "The corrupted ones are corrupted because their shaman was corrupted and it spreading",
            "Killed all the corrupted golaiths (but not their shaman) and rescued the captured shaman",
            "The corrupted shaman had a foul tome that caused the corruption. He got it from a Dwarven fort at the peak of the mountain",
            "Have two paths to the peak, one has more corrupted golaiths, the other has wild animals (probably magic essence!)",
            "We have 8 hours until nightfall, it will take us 6 hours walking to get where we need to be",
            "The shaman told us where the circle of teleportation we need to escape through. It's where the enemy shaman has their camp",
            "Started hiking the mountain. Saved a merchant from wolves, got some healing potions in reward",
            "Encountered the Cold Embrace of the Mountain, took a bunch of cold damage but bested its trial. It gave us resistance to cold damage and a cold essence each",
            "Did some more fights, lightning birds",
            "Found the shaman camp, started a fight",
            "Shaman was monologuing, mentioned power given by Master Gwine (probably the black tome)",
            "Killed the totems that were protecting the shaman, killed him, got the book (plus a bunch of essence!)",
            "Teleported back, safe and sound",
        ],

        "October 3, 2021":
        [
            "Returned the DOOM to the Keepers",
            "Spent 30 collective gold to infuse 3 items. I got an ice crossbow and electric pants, Krateos got cold pants",
            "The tome is from the dwarven castle (Von Blut) near the golaiths lands",
            "Was the home of 3 powerful wizard who died in mysterious circumstances, is now haunted",
            "We have to go back there and determine what created this spooky tome",
            "Teleported back to the camp, all the golaiths were dead",
            "But zombies! Fought a few, plus one big one that dropped a necrotic essense",
            "Encounted the Cold Embrace of the Mountain again, it gave us its blessing, so we have resistance to cold damage",
            "Entered the castle, met a ghost in the front hall. She screamed that we weren't invited and started a fight",
            "When we beat the ghost her \"evil\" went away and the room of the castle was restored, with a ghost party happening. None of the ghosts could see us or knew we were there.",
            "We talked to her but she didn't have any memories of who she was",
            "We asked about Master Gwine, she knew about him but didn't know where he was at the moment.",
            "At the masquade party there was a mysterious hooded figure ghost. We followed him into another room as he walked around",
            "We tried to continue on, was blocked by a door with green spectral enery, have to backtrack and try and find a way around",
            "Found a spike covered lever that's currently covered by a red glow. We pushed it and the glow switched to green",
            "Continued through a green door, encounted another wizard ghost (probably Stefan, who's crates we looted)",
            "Fought some more zombies",
            "Found Petra's diary, turns out she was fighting with her brothers (the other wizards) and she hired an assassin to kill them the night of this party",
            "Encountered the third ghost, was a real hard fight",
            "Both of us almost died, I had to drop my crossbow, draw a dagger, and kill a corpse mound at 1HP",
            "We learned a little more about Master Gwine, he's in the top floor of the castle",
            "We can't get up there without a scroll of fabricate to repair the stairs",
            "We think Gwine might be the assassin, since he's been living here \"since the party started\"",
            "We teleported back to the Keeper Vault",
            "Leveled up!",
        ],

        "Nov 7, 2021":
        [
            "Spent 5gp to buy Papa the world's ugliest amulet, so he could infuse it with Necrotic essence",
            "Another DOOM present in the jungles just south of the Mountains we were in",
            "We know there are 'barbaric elves' living in the jungles",
            "Traded the 'Tome of Alchemical Recipes' to Preserver Flora for a Greater Healing Potion",
            "Flora also identified some potions for us",
            "Teleported into a swamp, saw an elf-ish person/thing being attacked by lizard folk",
            "Kind of tough battle, we missed a lot",
            "Some swamp plants came alive and one mushroom exploded and hurt the lizards",
            "The lizards ran away and we've saved the elf(thing), who's name is Strak",
            "They led us to the rest of the elf-things, where we could take a rest",
            "During the rest, we found two Tabaxi imprisoned in a Venus-Fly-Trap jail. They were trespassing, we're going to try and help them",
            "We tried to talk to the leader Ichora, she didn't like us and wouldn't release the prisoners",
            "Father distracted the guards and I stole their nectar, releasing the prisoners",
            "They told us where their merchant cart is, we went there to loot it, got into a fight with some plants. Strak got beat up a little, but survived",
            "We went to the Sunken Pyramid that the lizard folk are doing their rituals at",
            "Fought some lizards along the way",
            "Saw carvings on the walls that implied that the Bog Mother is on the side of the lizards, since she's helping birth the elves just so the lizards can eat them",
            "Killed the super corrupted lizard, it dropped an uncommon essence!",
            "I took the DOOM idol out of the wall, water started leaking in as the temple started to sink, we're dashing to the exit",
            "We made it out, the temple sinks into the swamp",
            "Turns out the idol probably wasn't the DOOM, the other elves want to go explore the jungle to find the source of corruption that is making the plants alive",
            "Took a long rest inside a tree, we'll go exploring in the morning",
        ],

        "Nov 14, 2021":
        [
            "Went to sleep in a flower pod, had a dream where we spoke to the Bog Mother",
            "She explained that an unknown female Human came recently to the bog, left it and traveled into the jungle, and left the DOOM somewhere there",
            "The Human promised the jungle Elves much, but delivered them nothing",
            "She's happy we're going to remove it so that she can continue to maintain the balance of the bog",
            "She wants her \"children\" (the jungle elves) to come back to the bog. If we can convince them to do this she would reward us",
            "Traveled into the jungle, ran into a Warforged (Harvester) who was trapped in quicksand, we saved them using rope and the Immovable Rod",
            "Talked with Harvester more about what his group is doing, he won't talk about what they're doing, how many there are, or the fact that they're staying in the area",
            "Came across a jungle elf (Lossom) who was being attacked by corrupted plants",
            "The elf refused to travel with the Warforged, we decided to side with the elves over the gnomes. Harvester felt very betrayed",
            "We talked to the elder (Deedle), she explained how the gnomes were going to attack their sacred temple so they warded it",
            "Their temple still got surrounded by the plant corruption, but the inside of the temple should be ok",
            "We're going with their scouting party to try and capture a gnome and get some info about what they want",
            "We found a gnome surrounded by Warforged, Papa distracted them enough that we were able to surround most of them before fighting began",
            "We defeated all the Warforged and captured the gnome, brought them back to the jungle gnomes",
            "Questioned the gnome, learned that the gnomes want the elven artifact in the temple. Leader is JimJim WiddleMiddle",
            "Deedle described the human woman: Light brown skin, blue clothes, black hair, dark eyes, rather thin, taller than the elves",
            "We looked at the scrap of cloak, it had an 8 pointed star of Occam on it!",
            "Took a short rest, going to defend the temple",
            "The temple has traps, don't step on the tiles that have elephants",
            "The temple looks pretty similar to the sunken lizard temple...",
            "Lossom and the elves distract most of the warforged as we attack the main temple",
            "Fought some warforged as we went to the top of the temple (outside staircase)",
            "Then fought some plants as we descended through the middle of the temple (inner staircase)",
            "There is a giant plant at the bottom, with a laser beam coming off of it that is sweeping around the room",
            "JimJim is also there",
            "We beat the plant, killed JimJim",
            "I stole his Cog of Authority, used it to command the warforged to left us leave",
            "My ruse fell apart when another gnome showed up, so we took the anti-magic gnome artifact and ran into the forest",
        ],
    },

    ////////////////////////
    // These are the things you own, organized by where they are stored.
    ////////////////////////
    Inventory:
    {
        "Coin Purse":
        [
            { Name: "CP", Amount:  0, Weight: 0.02, Cost: 0.01, Description: "" },
            { Name: "SP", Amount:  2, Weight: 0.02, Cost: 0.1 , Description: "" },
            { Name: "GP", Amount: 47, Weight: 0.02, Cost: 1   , Description: "" },
        ],

        "On Person":
        [
            { Name: "Amulet"            , Amount:  1, Weight:  1    , Cost:  5   , Description: "Infused with a Common Fire Enchantment, purchased by Papa" },
            { Name: "Clothes, travelers", Amount:  1, Weight:  4    , Cost:  2   , Description: "" },
            { Name: "Crossbow bolts"    , Amount: 40, Weight:  0.075, Cost:  0.05, Description: "" },
            { Name: "Dagger"            , Amount:  1, Weight:  1    , Cost:  2   , Description: "" },
            { Name: "Light Crossbow"    , Amount:  1, Weight:  5    , Cost: 25   , Description: "Infused with a Common Ice Enchantment" },
            { Name: "Quiver"            , Amount:  1, Weight:  1    , Cost:  1   , Description: "" },
            { Name: "Studded Leather"   , Amount:  1, Weight: 13    , Cost: 45   , Description: "Infused with a Common Lightning Enchantment" },
        ],

        "In Backpack":
        [
            { Name: "Backpack"                   , Amount:  1, Weight:  5   , Cost:  2   , Description: "" },
            { Name: "Bedroll"                    , Amount:  1, Weight:  7   , Cost:  1   , Description: "" },
            { Name: "Blanket"                    , Amount:  1, Weight:  3   , Cost:  0.5 , Description: "" },
            { Name: "Bog Water Vial"             , Amount:  0, Weight:  1   , Cost:  0   , Description: "Gives 2d4 + 2 temp health, given by Ichora" },
            { Name: "Chain Shirt"                , Amount:  1, Weight: 20   , Cost: 50   , Description: "Probably belonged to Diego the Tabaxi" },
            { Name: "Crowbar"                    , Amount:  1, Weight:  5   , Cost:  2   , Description: "" },
            { Name: "Disguise kit"               , Amount:  1, Weight:  3   , Cost:  25  , Description: "" },
            { Name: "Dragonchess set"            , Amount:  1, Weight:  0.5 , Cost:  1   , Description: "" },
            { Name: "Essence (Electric, Common)" , Amount:  0, Weight:  0   , Cost:  0   , Description: "" },
            { Name: "Essence (Fire, Common)"     , Amount:  2, Weight:  0   , Cost:  0   , Description: "" },
            { Name: "Essence (Fire, Uncommon)"   , Amount:  1, Weight:  0   , Cost:  0   , Description: "" },
            { Name: "Anti-Magic gnome gun"       , Amount:  1, Weight:  0   , Cost:  0   , Description: "Built by JimJim, taken from the sunken temple" },
            { Name: "Essence (Ice, Common)"      , Amount:  5, Weight:  0   , Cost:  0   , Description: "" },
            { Name: "Essence (Necrotic, Common)" , Amount:  1, Weight:  0   , Cost:  0   , Description: "" },
            { Name: "Essence (Poison, Common)"   , Amount:  3, Weight:  0   , Cost:  0   , Description: "" },
            { Name: "Cog"                        , Amount:  1, Weight:  0   , Cost:  0   , Description: "Taken from the corpse JimJim, maybe controls the warforged?" },
            { Name: "Essence (Poison, Uncommon)" , Amount:  2, Weight:  0   , Cost:  0   , Description: "" },
            { Name: "Evil Lizard Idol"           , Amount:  1, Weight:  0   , Cost:  0   , Description: "Taken from the Sunken Lizard's Temple. Used to corrupt the lizards. DOOM?" },
            { Name: "Fancy Raw Materials"        , Amount:  1, Weight:  0   , Cost: 20   , Description: "Stolen from the Tabaxi merchants" },
            { Name: "Forgery kit"                , Amount:  1, Weight:  5   , Cost:  15  , Description: "" },
            { Name: "Hammer"                     , Amount:  1, Weight:  3   , Cost:  1   , Description: "" },
            { Name: "Healing Potion (Greater)"   , Amount:  0, Weight:  0   , Cost:  0   , Description: "Traded from Preserver Flora, 4d4 + 4" },
            { Name: "Healing Potion (Minor)"     , Amount:  1, Weight:  0   , Cost: 50   , Description: "Taken from boxes belonging to Stefan Von Blut" },
            { Name: "Hempen rope (50ft)"         , Amount:  1, Weight: 10   , Cost:  1   , Description: "" },
            { Name: "Immovable Rod"              , Amount:  1, Weight:  0   , Cost:  0   , Description: "Stolen from the Tabaxi merchants" },
            { Name: "Pitons"                     , Amount: 10, Weight:  0.25, Cost:  0.05, Description: "" },
            { Name: "Potion of Animal Friendship", Amount:  1, Weight:  1   , Cost:  0   , Description: "Taken from the alchemist lab in the Dwarven Castle" },
            { Name: "Potion of Fire Breath"      , Amount:  1, Weight:  1   , Cost:  0   , Description: "Taken from the alchemist lab in the Dwarven Castle" },
            { Name: "Rations"                    , Amount: 10, Weight:  2   , Cost:  0.5 , Description: "" },
            { Name: "Ring of Jumping"            , Amount:  1, Weight:  0   , Cost:  0   , Description: "Probably belonged to Diego the Tabaxi" },
            { Name: "Teleportation Scroll"       , Amount:  1, Weight:  0   , Cost:  0   , Description: "To go back to the Keeper's Vault" },
            { Name: "Tent"                       , Amount:  1, Weight: 20   , Cost:  2   , Description: "Holds 2 people" },
            { Name: "The Diary of Petra Von Blut", Amount:  1, Weight:  0   , Cost:  0   , Description: "Taken from Petra's bedroom" },
            { Name: "Thieves' tools"             , Amount:  1, Weight:  1   , Cost: 25   , Description: "" },
            { Name: "Tinderbox"                  , Amount:  1, Weight:  1   , Cost:  0.5 , Description: "" },
            { Name: "Torches"                    , Amount: 10, Weight:  1   , Cost:  0.01, Description: "" },
            { Name: "Torn Scrap of Cloak"        , Amount:  1, Weight:  1   , Cost:  0   , Description: "From the human woman who left a DOOM in the Jungle. Has an eight pointed star on it (Occam!)" },
            { Name: "Venus Fly Trap Nectar"      , Amount:  1, Weight:  0   , Cost:  0   , Description: "Entices a venus fly trap prison to open" },
            { Name: "Waterskin"                  , Amount:  1, Weight:  5   , Cost:  0.2 , Description: "" },
        ],
    },
};

//
// This is data that is generally changed more that once each level.
//
const DataThatChangesLessOften =
{
    ////////////////////////
    // This is your AC.
    ////////////////////////
    AC_Formula: "12 + DexMod", // Studded Leather.

    ////////////////////////
    // These are reminders about special circumstances where your skills may be more or less effective.
    ////////////////////////
    SkillExceptionReminders:
    {
    },

    ////////////////////////
    // These are reminders about special circumstances where your saving throws may be more or less effective.
    ////////////////////////
    SavingThrowExceptionReminders:
    {
        Wisdom : "Advantage against being charmed (Fey Ancestry)",
    },

    ////////////////////////
    // These are attacks that are done against your opponent's AC.
    ////////////////////////
    Attacks:
    [
        { Name: "Dagger"                        , HitBonus_Formula: "AttackBonus"    , DamageDice: "1d4"      , DamageBonus_Formula: "DexMod"     , Range: "Melee"   , DamageType: "Piercing" },
        { Name: "Light Crossbow (Sneak)"        , HitBonus_Formula: "AttackBonus"    , DamageDice: "1d8 + 2d6", DamageBonus_Formula: "DexMod"     , Range: "80/320ft", DamageType: "Piercing" },
        { Name: "Light Crossbow"                , HitBonus_Formula: "AttackBonus"    , DamageDice: "1d8"      , DamageBonus_Formula: "DexMod"     , Range: "80/320ft", DamageType: "Piercing" },
        { Name: "(Sharp) Light Crossbow (Sneak)", HitBonus_Formula: "AttackBonus - 5", DamageDice: "1d8 + 2d6", DamageBonus_Formula: "DexMod + 10", Range: "80/320ft", DamageType: "Piercing" },
        { Name: "(Sharp) Light Crossbow"        , HitBonus_Formula: "AttackBonus - 5", DamageDice: "1d8"      , DamageBonus_Formula: "DexMod + 10", Range: "80/320ft", DamageType: "Piercing" },
    ],

    ////////////////////////
    // These are attacks that force your opponent to make a saving throw.
    ////////////////////////
    SavingThrowAttacks:
    [
        { Name: "Hellish Rebuke", DC_Formula: "13", Stat: "Dex", DamageDice: "2d10", DamageBonus_Formula: "0", Range: "60ft", DamageType: "Fire", OnSuccess: "Half Damage" },
    ],

    ////////////////////////
    // These are the spells that you know or can prepare, organized by the spell's level.
    ////////////////////////
    Spells:
    [
    ],
};

//
// This is data that is generally changed only when a new level is gained.
//
const DataThatChangesOnLevelUp =
{
    ////////////////////////
    // This records what class you took at each level up.
    ////////////////////////
    Levels:
    [
        "Rogue",    // Level 1
        "Rogue",    // Level 2
        "Rogue",    // Level 3
        "Rogue",    // Level 4
    ],

    ////////////////////////
    // This allows you to override the default max HP formula with your own.
    // Use this if you are rolling for health.
    ////////////////////////
    OverrideMaxHP_Formula: undefined,

    ////////////////////////
    // These are the things that you can do when taking a short or long rest.
    ////////////////////////
    RestOptions:
    {
        "On Short Rest":
        [
        ],

        "On Long Rest":
        [
        ],
    },

    ////////////////////////
    // These are things that you can do during your turn in combat.
    ////////////////////////
    CombatOptions:
    {
        "Action":
        [
        ],

        "Bonus Action":
        [
            "Dash",       // Rogue 2, Cunning Action.
            "Disengage",  // Rogue 2, Cunning Action.
            "Fey Step",   // Eladrin.
            "Help"      , // Rogue 3, Master Of Tactics.
            "Hide",       // Rogue 2, Cunning Action.
            "Steady Aim", // Rogue 3, Steady Aim.
        ],

        "Reaction":
        [
        ],

        "On Turn":
        [
        ],

        "Any Time":
        [
        ],

        "Movement":
        [
        ],
    },

    ////////////////////////
    // These are all the things that your race, class, and background allow you to do.
    ////////////////////////
    Features:
    {
        "Eladrin (Elf)":
        [
            { Name: "Keen Senses"           , Desc: [ "Proficiency in the Perception skill"] },

            { Name: "Fey Ancestry"          , Desc: [ "Advantage on saving throws against being charmed"
                                                    , "Magic cannot put me to sleep" ] },

            { Name: "Trance"                , Desc: [ "Don't sleep, go into a semiconscious meditative trance instead"
                                                    , "Only takes 4 hours to do"
                                                    , "Conflicting sources about whether or not this means the whole long rest is shorter (PHB errata vs SA-Compendium)" ] },

            { Name: "Fey Step"              , Desc: [ "Magically teleport up to 30 ft to and unoccupied space I can see"
                                                    , "Costs a Bonus Action to do"
                                                    , "Recharge on short or long rest"
                                                    , "Has an extra effect based on my season, DC is {8 + ProfBonus + ChaMod}" ] },

            { Name: "Fey Step (Autumn)"     , Desc: [ "After teleporting, 2 creatures within 10 ft make a Wisdom save"
                                                    , "On fail, they are charmed for 1 minute, or until they take damage"] },

            { Name: "Fey Step (Winter)"     , Desc: [ "Before teleporting, 1 creature within 5 ft makes a Wisdom save"
                                                    , "On fail, they are frightened of me until the end of my next turn"] },

            { Name: "Fey Step (Spring)"     , Desc: [ "Before teleporting, can touch 1 <b>willing</b> creature within 5 ft"
                                                    , "They teleport instead of me"] },

            { Name: "Fey Step (Summer)"     , Desc: [ "After teleporting, creatures of my choice within 5 ft take {ChaMod} fire damage" ] },

            { Name: "Feat: Elven Accuracy"  , Desc: [ "If have advantage on a Dex/Int/Wis/Cha attack roll, get super advantage instead" ] },
        ],

        "Spy":
        [
            { Name: "Criminal Contact"      , Desc: [ "Have a contact who acts as my liaison to a network of other criminals"
                                                    , "This contact is reliable and trustworthy"
                                                    , "I know how to get messages to and from this contact, even over great distances"
                                                    , "Know lots of seedy people (messengers, drivers, sailors) who can deliver the message for me" ] },
        ],

        Rogue:
        [
            // Level 1.
            { Name: "Expertise"             , Desc: [ "Picked Deception and Sleight of Hand" ] },

            { Name: "Sneak Attack"          , Desc: [ "Once per turn can deal <b>2d6</b> extra damage to one creature you hit with an attack"
                                                    , "The attack must use a finesse or a ranged weapon"
                                                    , "Must have advantage on the attack roll, or have an enemy of the target is within 5 feet of them"
                                                    , "That enemy can't be incapacitated"
                                                    , "I can't have disadvantage on the attack roll" ] },

            { Name: "Thieves'Cant"          , Desc: [ "Can encode secret messages into seemingly normal conversation"
                                                    , "Takes 4x longer to get the meaning across compared to talking normally"
                                                    , "Also understand a set of secret signs/symbols that Thieves leave places"
                                                    , "Explain if area is dangerous, owned by the Thieves Guide, cool loot nearby, etc..." ] },
            // Level 2.
            { Name: "Cunning Action"        , Desc: [ "Can Dash, Disengage, or Hide as a Bonus Action" ] },

            // Level 3.
            { Name: "Master Of Intrigue"    , Desc: [ "Gain proficiency with Disguise Kit, Forgery Kit, and a gaming set"
                                                    , "Gain proficiency in two extra languages"
                                                    , "Can perfectly mimic speech patterns and accent of a creature I've heard speak for at least 1 minute"
                                                    , "Lets me pretend to be a native speaker of a particular land, assuming I know the language" ] },

            { Name: "Master Of Tactics"     , Desc: [ "Can Help as a Bonus Action"
                                                    , "Can Help an ally 30 ft away in attacking an enemy" ] },
            // Level 4.
            { Name: "Feat: Sharpshooter"    , Desc: [ "Can do ranged attacks at long range without disadvantage"
                                                    , "Ranged weapon attacks ignore half and three-quarter cover"
                                                    , "When attacking with a ranged weapon, can take -5 to the attack roll to add +10 to the damage roll" ] },
        ]
    },
};

//
// This is data that generally only set when the character is first created.
//
const DataThatIsSetOnCharacterCreation =
{
    ///////////////////
    // This is your name.
    ///////////////////
    Name: "Atreus",

    ///////////////////
    // This is the small image that is displayed next to your name.
    // It looks best if the image is square.
    ///////////////////
    HeadshotPicture: "Atreus Headshot.jpg",

    ///////////////////
    // This is your initiative.
    ///////////////////
    Initiative_Formula: "DexMod",

    ///////////////////
    // This is your attack bonus and save DC.
    //
    // It makes sense for both magical and physical things. For example: weapon attacks,
    // spell attacks, Battle Master Fighter maneuvers, Monk Stunning Strike, etc...
    ///////////////////
    AttackBonus_Formula: "DexMod + ProfBonus",     // Rogue 1.
    SaveDC_Formula     : undefined,

    ///////////////////
    // This is how many spells you can prepare.
    //
    // Set it to "undefined" if you can't prepare any spells.
    ///////////////////
    NumSpellsPrepared_Formula: undefined,

    ///////////////////
    // This is how much extra HP you recover when you level up.
    ///////////////////
    ExtraHealthGainOnLevel_Formula: "ConstitutionModifier",

    ///////////////////
    // This is how much extra HP you recover when you roll a hit die.
    ///////////////////
    HitDiceRollModifier_Formula: "ConstitutionModifier",

    ///////////////////
    // These are the things that you are proficient with.
    ///////////////////
    Proficiencies:
    {
        Weapons:
        [
            "Simple Weapons", // Rogue 1.
            "Hand Crossbows", // Rogue 1.
            "Longswords",     // Rogue 1.
            "Rapiers",        // Rogue 1.
            "Shortswords",    // Rogue 1.
        ],
        Armor:
        [
            "Light Armor", // Rogue 1.
        ],
        Tools:
        [
            "Disguise Kit",             // Rogue 3, Master Of Tactics.
            "Forgery Kit",              // Rogue 3, Master Of Tactics.
            "Gaming Set (Dragonchess)", // Rogue 3, Master Of Tactics.
            "Thieves' tools",           // Rogue 1.
        ],
        Languages:
        [
            "Common",   // Eladrin.
            "Draconic", // Spy Background.
            "Dwarvish", // Rogue 3, Master Of Tactics.
            "Elvish",   // Eladrin.
            "Gnomish",  // Rogue 3, Master Of Tactics.
            "Halfling", // Spy Background.
        ]
    },

    ///////////////////
    // These are your physical attributes.
    ///////////////////
    PhysicalAttributes:
    {
        MoveSpeed              : 30,              // Eladrin.
        FlyingSpeed            : 0,               // Eladrin.
        SwimSpeed_Formula      : "MoveSpeed / 2", // Eladrin.
        ClimbSpeed_Formula     : "MoveSpeed / 2", // Eladrin.
        StandFromProne_Formula : "MoveSpeed / 2",
        Height                 : 4.75,            // Eladrin.
        Weight                 : 70,              // Eladrin.
        Size                   : "Medium",        // Eladrin.
        CarryingCapacitySize   : "Medium",        // Eladrin.
        Age                    : 12,
        DarkVisionDistance     : 60,              // Eladrin.
    },

    ///////////////////
    // These are your stats.
    ///////////////////
    Stats:
    {
        Strength     : { Value_Formula: "5 + 1"      }, // Rolled + Eladrin.
        Dexterity    : { Value_Formula: "15 + 2 + 1" }, // Rolled + Eladrin + Elven Accuracy Feat.
        Constitution : { Value_Formula: "16"         }, // Rolled.
        Intelligence : { Value_Formula: "14"         }, // Rolled.
        Wisdom       : { Value_Formula: "10"         }, // Rolled.
        Charisma     : { Value_Formula: "12"         }, // Rolled.
    },

    ///////////////////
    // These are the saving throws that you are proficient in.
    ///////////////////
    ProficientSavingThrows:
    [
        "Dexterity",    // Rogue 1.
        "Intelligence", // Rogue 1.
    ],

    ///////////////////
    // These are the skills that you are proficient in. Set the multiplier to 2 if you have Expertise.
    ///////////////////
    ProficientSkillMultipliers:
    {
        Arcana        : 1, // Spy Background.
        Deception     : 2, // Rogue 1 + Expertise.
        History       : 1, // Spy Background.
        Investigation : 1, // Rogue 1.
        Perception    : 1, // Eladrin.
        SleightOfHand : 2, // Rogue 1 + Expertise.
        Stealth       : 1, // Rogue 1.
    },

    ///////////////////
    // This is pretty much only for supporting the Bard's "Jack of all Trades" feature by setting it to 0.5.
    ///////////////////
    DefaultSkillProficiencyMultiplier: 0,

    ///////////////////
    // These are links to external websites that might have useful info.
    ///////////////////
    ExternalLinks:
    {
        "Adventure Doc" : "https://docs.google.com/document/d/1BYwbEjSUsP5rgbHeLi8oa8dOrZPJhjHcuwb8Hh-Wfi4/edit?usp=sharing",
    },

    ///////////////////
    // Roleplay info about your character.
    ///////////////////
    Bio:
    {
        "Ideals":
        [
            "I have a strong respect for authority figures and will generally do what they say",
            "The two strongest authority figures in my life are my father and Brother Alric. What happens if they get into conflict? I guess we'll find out",
            "Brother Alric has taught me to value (other people's) secrets above all things. Why risk stealing a man's wealth, when you have his secrets he will be falling over himself to give the wealth to you."
        ],

        "Bonds":
        [
            "The Temple of Occam where I was raised is the most important place in the world to me. I don't have many memories of living out in the wilds with my family, I was quite young then",
            "Brother Alric took care of me and taught me all I know at a time in my life when I had no one else. He is a father figure to me",
            "My father has unexpectedly come back into my life and is the only family I have left"
        ],

        "Flaws":
        [
            "Brother Alric has taught me to always be on the lookout for secrets. I don't steal wealth, but any sort of paper left unattended might find its way into my pockets",
            "I am terrified that my father might be taken away from me again. I don't like to let him out of my sight"
        ],

        "Jake's Info":
        [
            "Fear: That my father will be taken away from me again",
            "True Rumour: You should be careful with your secrets around the boy, or they might end up making their way to Brother Alric",
            "True Rumour: The boy is deceptively good at Dragonchess, don't fall for his \"I'm a small child\" act and play for money against him",
            "False Rumour: People have been whispering that Krateos isn't my real father. How can he be, we look and act so differently? (Is Brother Alric encouraging this rumour?)",
            "Reason for joining the Keepers: Brother Alric said I would be good at it and it would make him proud if I joined. It also gives my father something to do, temple life isn't really his style",
        ],

        Picture: "Atreus.jpg",

        "Backstory":
        [
            "Happy family life for the first 5 years, living in a house out in the wilderness",
            "Then a mob came and kidnapped my father",
            "Both my mother (Fraye) and I were wounded in the attack",
            "We went to the nearest temple for shelter and healing, which was a temple of Occam",
            "They weren't able to save my Mother",
            "I was adopted by the temple and given to the care of Brother Alric who was in charge of the orphans",
            "I spend 7 happy years living in the temple, growing up and being taught by Brother Alric",
            "Turns out Brother Alric was also a member of the Keepers and has been molding me into the perfect little child spy",
            "6 months ago father Krateos showed up at the temple, he has escaped his kidnappers!",
            "We've still been hanging out at the temple, it seems like he might stay!",
        ],
    },
};

//
// Smush all the data together into a single object, since it's only spread
// across multiple objects to make editing the data easier for the user.
//
const Character = Object.assign
(
    DataThatChangesOften,
    DataThatChangesLessOften,
    DataThatChangesOnLevelUp,
    DataThatIsSetOnCharacterCreation
);
