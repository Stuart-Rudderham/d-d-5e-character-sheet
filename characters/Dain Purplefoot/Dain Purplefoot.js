//
// Data that need to be updated often (e.g. every round of combat) is at the
// top of the file for easy editing.
//
const DataThatChangesOften =
{
    ////////////////////////
    // This is your heath-related info.
    ////////////////////////
    Health:
    {
        CurrentDamage_Formula : "0",
        TempHP                : 0,
        NumHitDiceSpent       : 0,
    },

    ////////////////////////
    // These are the resources that you can spend in or out of combat which replenish after resting.
    ////////////////////////
    Resources:
    [
        { Name: "Hexblade's Curse"    , NumSpent: 0, Max_Formula: "1", Recovery: "Short Rest" }, // Hexblade 1 (Hexblade's Curse).
        { Name: "Accursed Specter"    , NumSpent: 0, Max_Formula: "1", Recovery: "Long Rest"  }, // Hexblade 6 (Accursed Specter).
      //{ Name: "Mystic Arcanum (6th)", NumSpent: 0, Max_Formula: "1", Recovery: "Long Rest"  }, // Warlock 11 (Mystic Arcanum).
    ],

    ////////////////////////
    // These are your spell slots.
    ////////////////////////
    SpellSlots:
    [
        { NumSpent: 0, Max: 0 },    // Level 1
        { NumSpent: 0, Max: 0 },    // Level 2
        { NumSpent: 0, Max: 0 },    // Level 3
        { NumSpent: 0, Max: 2 },    // Level 4
        { NumSpent: 0, Max: 0 },    // Level 5
    ],

    ////////////////////////
    // Record if you have inspiration, and if so, how you got it.
    ////////////////////////
    InspirationReason: "Putting on the moves to Lady Atalia during our casino visit",

    ////////////////////////
    // These are the things that have happened to you, organized by session.
    //
    // Each session header is the date that it happened. The date format is not
    // ISO 8601 (e.g. "2019-12-25") because that is always in the UTC timezone.
    // By using an implementation specific format (e.g. "Dec 25, 2019") the local
    // timezone is assumed. Firefox and Chrome seem to give consistent results
    // so that's good enough for us.
    ////////////////////////
    CampaignNotes:
    {
        "Nov 25, 2021":
        [
            "Created my character!",
        ],

        "Nov 28, 2021":
        [
            "Arrived in Nightstone, it looks like it's currently being raided by goblins and wargs",
            "There are a bunch of medium sized rocks strews all around (3 feet in diameter)",
            "The drawbridge to the castle in Nightstone is damaged",
            "Looks like something scared all the towns-people away, and then the goblins came in to loot it",
            "Maybe the goblin's boss captured the towns-people?",
            "Found an injured woman (Kella) with a snake on her arm in the inn. She lied to us about being injured by the rocks falling",
            "Looks like she is a Zhentarim agent who just happened to be in town when the attack happened",
            "Tried to convince her to help us kill the goblins, she refused, we escorted her out of town so she couldn't loot everything",
            "Went around the town and killed the rest of the goblins. There were 12 in all, along with 2 wargs",
            "We spoke to an intelligent cat (tressym), it told us that a flying castle rained down the rocks in the town, then giants came and took the special \"obsidian stone\" that was in the middle of the town",
            "We know that Cloud Giants live and travel around in flying castles",
            "Tried to go to the castle, the bridge has a 15ft gap in it from a rock",
            "We tried to call across but no one responded. We're going to try to figure out a way to cross it",
            "Leveled up!",
        ],

        "Dec 5, 2021":
        [
            "After short rest, guards came out from the castle and we chatted",
            "The leader of Nightstone was crushed by a rock, the castle is partially collapsed",
            "The guards see a dust cloud in the distance, coming towards the town",
            "It's a hoard of orcs, ~20 of them with a shaman and chieftain",
            "They tried jumped into the river, tried to to climb the village walls but they were too slippery",
            "They started to swim around to the castle's broken bridge, it might be easier for them to climb there",
            "We told the guards to help shoot them, and we decided to make our stand at the drawbridge, shoot them as they climb up",
            "Dain transformed into a draft horse, moved some big rocks to try and create more choke points",
            "It was a tough battle, the orcs were climbing the bridge as we attacked them",
            "Otis cast Entangle on the bridge, making it even harder",
            "I used Hex on the Orc chieftain and managed to push him off the bridge. He was super scary so it was a good choice",
            "Turns out the Orcs were being chased by a band of Elves, so when the Elves showed up it scared off the rest of the Orcs",
            "We ended up killing 10 of them, which is pretty good. And no one died!",
            "Level up!",
            "Took a long rest overnight in one of the houses",
            "We traveled to a cave the guards told us about that they think the villagers might have gone",
            "Otis turned into a bat and scouted out, it's filled with goblins and ogres",
            "Snuck in, fought the goblin boss and a bunch of giant rats",
            "The boss took a villager hostage, tried to bargain with us to let him go",
            "We didn't like that, I tried to Hold Person the boss and save the villager. It failed, the villager died :(",
            "Fought some more goblins and ogres. It was tough, Rusty ended up going down (but was later stabilized)",
            "Rescued the villagers",
            "Turns out the slain hostage was the handmaid of the lady of the castle (who is also deceased)",
            "Leveled up!",
        ],

        "Jan 2, 2022":
        [
            "Brought the villagers back home",
            "Spent the rest of the day helping them repair the town and bridge",
            "Took a long rest, were planning to head to Goldenfields in the morning",
            "In the morning, a floating wizard's tower flew overhead. This is a different thing than the floating castle that dropped the rocks earlier",
            "A stairway of clouds come down, seemingly inviting us in. We go up the stairs. It's 1000 feet up, our calves hurt.",
            "We enter the tower, call out, and a cloud giant wearing a wizard robe comes out to greet us",
            "His name is Zephyrus, and he's a little kooky",
            "He says \"The Ordning has been shattered\" and he's looking for answers about why. Giant society has been thrown into shambles",
            "He's been speaking to entities on other planes and they told him to \"speak to the small folk\", which is us",
            "We told him who were are, and he claims that we were the \"Chosen Ones\" who will \"restore us\"",
            "\"The Ordning\" is the hierarchy of giants. Storm -> Cloud -> Fire -> Stone -> Hill",
            "The giants are doing whatever they want, and going out into the world more often",
            "Zephyrus volunteered to take us to Goldenfields, it will take 3 days to travel there.",
            "Stonecunning: the castle is made of stone from the Spine of the World, quarried at least 100 years",
            "Asked him about the Nightstone the other giants stole. Zephyrus thinks the stone had arcane glyphs carved on it, and the other giants took it so they could study it further",
            "We're flying along to Goldenfields, and after a couple of days we are attacked by human flying on a flock giant vultures",
            "They are an (evil?) cult asking Zephyrus for some help with some weird things. \"Return the world to its primordial state\"",
            "He says no, they blame us, we fight.",
            "It was actually a tough fight. They summoned a small air elemental that was invisible and has resistance to most of our attacks",
            "Zephyrus came down and swatted it away with his +15 to hit and 3d8+10 damage. He scary",
            "We reach Goldenfields, level up!",
            "Goldenfields is a very fortified city, but it was recently attacked by Hill Giants. It's fine though",
            "We told Miros that his parents died and passed over the tressym to him. He did not take the news well",
            "Talked to a wizard lady from Waterdeep, apparently giants and dragons historically have a feud. Maybe not relevant?",
            "Talked to the abbot and the head of security, told them about the Cloud Giants, they weren't concerned",
            "Went to bed, woken up in the middle of the night. The town is being attacked!",
            "Fought goblins, hobgoblins, and ogres. Slew two big groups of them",
            "Had a few towns-people come up us they were pretty competent",
            "Then two hill giants appear!",
            "They were pretty scary, but we got them. Dain got the final kill on the last giant",
            "Leveled up!",
        ],

        "Jan 30, 2022":
        [
            "Quest #1:",
            "Miros tells us about Arleosa Starhenge, a friend from his circle days.",
            "She's an owner of the Staghorn Flagon, an inn in the town of Amphail",
            "He wants us to speak to her, mention that we helped him, there's a good chance she'll give us a magic item that she has",
            "Quest #2:",
            "Naxine (wizard lady) thinks that we could get the dragons to help us against the giants",
            "Go to Waterdeep, seek out Chazlauth Yarghon who is a dragon expert who can help",
            "Quest #3:",
            "Litherlass (the tree) tells us about Aerglas, an adventurer known for being a giant slayer who might be able to help us",
            "Last seen going to Shadowtop Cathedral in the High Forest, but this was 30 years ago",
            "Quest #4:",
            "Zhi (monk lady) gave us a pendant that belongs to her noble family.",
            "Go to Waterdeep, show the pendant to Cauldar Marskyl, the head butler of House Than",
            "He'll give us some of her inheritance that she doesn't want",
            "We started walking towards Waterdeep, since we had 2 quests there",
            "Got attacked by some stuff on the way, they weren't too difficult",
            "Got Zhi's inheritance, it was two consumable items. We might be able to sell them for lots of money at least",
            "Spoke to Chaz about dragons, he told us about Old Knawbone, a green dragon",
            "Old Knawbone has lots of crystal balls and is very knowledgeable. It might help us if we're against the giants",
            "Old Knawbone lives in the Moon Wood. Someone in Silvery Moon would know more about it.",
            "Sold a Bead of Force to a merchant, got a bunch of gold. Time to buy Plate Mail!",
        ],

        "Feb 6, 2022":
        [
            "Sold some more loot (stuff from the giants)",
            "Bought some fine Dwarven made Plate Mail",
            "Paid my guide fees, 10GP",
            "Spent a nice night in a Dwarven inn. Left Waterdeep in the morning, started towards Amphail",
            "Ran into a group of Halflings being kidnapped by orcs, saved them. They gave us a Wand of Web as a reward",
            "Ran into a Knight, his squire had been killed by a stone giant. He beseeched us to help us avenge him",
            "Dain had a lot of trouble getting into the fight, lots of walking and dashing",
            "But once he got there, was able to do 55 damage in a single attack (crit + eldritch smite) and slew the giant",
            "Made it to Amphail, Otis got into a scuffle with the son of a noble lord and made himself an enemy",
            "It's the Lord's nameday, so the town is in the middle of a celebration",
            "I won 100gp at an archery context, beating out 2 peasants and one retired adventurer",
            "Both Otis and I lost at a pie eating contest, we pushed our luck and threw up",
            "As the night went on, the party was attacked by 3 Hill Giants! They were drawn in by all the food",
            "We slew them, it wasn't too bad. They do hit hard though",
            "The Lord of Amphail said that we were heroes",
        ],

        "Feb 27, 2022":
        [
            "Arleosa gave us a magic ring, for helping Miros as well as saving Amphail",
            "It has a magic safeword: Keltar",
            "We spoke the word, and a Halfling ghost came out and told us about some treasure they had buried. So the ring is actually a treasure map",
            "East along the stone trail from Westbridge, there's a tower there that has Keltar's treasure",
            "This is along the path we were already going, we we can stop along the way",
            "Encountered a fire giant for the first time on the road. It could be pretty scary but we killed it, I did a huge Eldritch Smith",
            "We ran into another adventurer on the road, a rogue Lark. They said they know about the world so we invited them to join the party",
            "We were ambushed by assassins sent by the Lord that Otis slighted. He shot a poisoned arrow at Otis and downed him with 1 shot",
            "It was spooky, but we defeated them and healed Otis",
            "We ran into Gum-Gum, she had set up a Shoe Shop. She's been on her best behaviour, and Dain is impressed by her enterprising spirit",
            "After 4 days of traveling we made it to Red Larch",
            "Learned about some Cultists with Giant Vultures nearby, there is a 50GP reward for dealing with them",
            "The cult leader is pretty spooky apparently",
            "Another group of adventurers had already tried to defeat them, they've asked us to try and get a silver ring that was dropped by their killed companion",
            "They're giving us info about the Cult in exchange for getting this ring back to them",
            "The giant vultures are always scouting around, so be careful or they will raise the alarm",
            "We infiltrated the cult's fortress at night, the door was suspiciously unlocked...",
            "Found a bunch of cultists in a training room, they summoned an air elemental. We deal with it without too much trouble",
            "We still need to find their leader",
        ],

        "March 6, 2022":
        [
            "Went into the next room, found the cult leader",
            "He put up a spooky Spirit Guardians (5d8 damage) but Dain paralyzed him with Hold Person to make him lose concentration",
            "We all just wailed on the paralyzed cult leader and he didn't put up much trouble",
            "We got the silver ring that we were looking for as well",
            "One of the cultist trainees managed to escape on a Giant Vulture",
            "Otis used Speak With Beasts to talk to the other Giant Vultures, he convinced them to leave and never come back",
            "Leveled up!",
            "The mayor asked what our adventuring group name is, we said: Purplefoot Adventuring Co. (subsidiary of Purplefoot Brewing Co.) ",
            "We got our (small) reward from the town, and then headed out north towards Westbridge",
            "Walked into the middle of a giant couple's date, they were upset. I got jumped on by the lady giant, it hurt a lot (32 damage)",
            "We encountered a noble on the road, we convinced him to buy our silk robe for his fiance for 2x value (he was desperate)",
            "Encountered a tribe of barbarians, a lot of them were fodder but it still hurt a bit",
            "Made it to Westbridge, learned that there is a hunt for a white stag going on. Otis is unhappy, and signed up to join the hunt",
        ],

        "March 20, 2022":
        [
            "There is a hippy lady (Janessa) protesting the hunt for the white stag",
            "She is sure the white stag is magical and is a guardian of the Kryptgarden forest nearby",
            "There are 3 hunters who are pretty hardcore, those are the ones we should be worried about. The nobles are pretty drunk and probably aren't a problem",
            "Dain managed to convince one of the hunters that alcohol is a performance enhancing drug that helps with aiming a bow, got one of the hunters drunk for 3 hours",
            "One of the hunters was really good at tracking us, turns out he cast Hunters Mark on Otis which made tracking easier",
            "Eventually we realized that he was following us rather than tracking the stag, so Otis managed to lead him away and save the stag",
            "The stag was grateful and gave Otis a magic Ring Of Protection",
            "Encountered some enterprising trolls guarding a bridge. Dain supported their small business, Otis went the long way",
            "Encountered some ogres that huck goblins at people",
            "The ogres ended up killing most of the goblins themselves, we took some damage",
            "We found Keltar's tower, it was inhabited by a sad lady hill giant named Moog",
            "We tried to find a way to bargain with Moog, failed, so we had to fight her :(",
            "After searching the (now empty) tower we were able to find Keltar's treasure",
            "Moved on to Stone Bridge (it's impressive) and found a weird 6ft cocoon",
            "It had a man in it! The cocoon is made of some weird membrane material, not from a natural beast",
            "His name is Artan and he's a young noble from Yartar",
            "Last thing he remembers is something about a golden goose. There was definitely a beautiful woman involved",
            "He wants us to take him back to Yartar, his mother will give us whatever we want",
            "There's something real weird with his skin, if he doesn't get doused in water every 10 minutes he'll die",
            "We need 6 waterskins worth of water per 24 hours",
            "We went to a nearby town, Beliard, to get some supplies for Artan. Then a 5 day trip up to Yartar",
        ],

        "May 1, 2022":
        [
            "Encountered a fair maiden who fell down a hill and hurt her ankle",
            "This was suspicious, turns out there were hags nearby!",
            "I banished one with a 4th level spell, breaking their coven. That was fun!",
            "Hags have a ton of HP and AC, they are pretty spooky",
            "Fought a stone giant, it hurled Rusty 60ft away which was mean",
            "We reached Yartar, Artan is bringing us to his estate to meet his mother",
            "His mother gave us 100gp for bringing him back",
            "His mother wants us to find their family pendant, Artan was wearing it when he went missing",
            "She'll give us 500gp if we figure out why Artan went missing, and 500gp if we get the pendant back",
            "She'll give us an introduction to The Water Baron, the ruler of Yartar",
            "Their butler, Kamed, is accompanying us. He looks like he can hold his own",
            "We identified that the rod we looted from the Fire Giants, it's really good at finding Adamantine",
            "Adamantine is a black metal from meteorites, we think that's what the stolen stone from Nightstone was!",
            "Turns out it took a 6th level Heal spell to fix Artan. It wouldn't have been so hard to cure if we had gotten him home sooner",
            "Turns out there is a floating casino, named the Grand Dame, their chips are called Golden Geese",
            "During the night it travels up and down the river while people inside gamble. Artan definitely went there",
            "Got 250gp to go to the casino, we'll get 500gp if we figure out and stop more disappearances",
            "We're doing a casino infiltration! Got some clothes, a makeover, and Kamed spread some rumors to help get us in the door",
            "We came up with fake names, Lark is Lady Nightingale, Dain is Lord Rumblebottom, Otis will be a cat",
            "We found a beautiful woman, Lady Atalia, playing a game, we sat down to play against her",
            "Turns out she leaves with a different man each night, so we're using Dain as a honeypot",
            "We left the boat together, we went into an alley, she tried to trap me in a sewer but we stopped her",
            "She claims there is a evil sorcerer Oosith in the sewer that was forcing her to bring the young nobles here",
            "She doesn't know what happens to them once she traps them in the sewer",
            "We're going to use Lark as bait, have her go to Oosith and we'll follow",
            "We got into a fight with some of the fish people that live in the sewer and worship Oosith",
            "Some people walked in the sewer water and maybe got sick with something? We'll find out later",
        ],

        "May 7, 2022":
        [
            "Turns out Oosith is maybe an Aboleth?",
            "He's in a big cistern filled with water. There are 4 closed grates in the bottom of it and 4 wheels in the middle platform",
            "There are two big fish people next to more wheels next to pipes in the walls",
            "They turned the wheels and water started pouring out of pipes",
            "We started opening the grates, so the water only rose a little bit",
            "Dain cast Fly so that him and Lark could move around the watery area more easily",
            "The Aboleth tried to enslave Otis, he managed to avoid it",
            "The Aboleth managed to enslave Lark, uh oh...",
            "We managed to drain all the water, so the Aboleth was just kind of sitting there like a fish in a barrel",
            "We managed to whittle it down and eventually defeated it",
            "Otis, Rusty, and Phil were all attacked by the Aboleth and now they have the same disease/curse that Artan had",
            "Got 1000gp from Artan's mother",
            "Paid 3500gp to a priest to cure all the diseases everyone had",
            "Went to the Water Baron, got 500gp reward and 3000gp worth of expenses",
            "The Water Baron declined to cover the 500gp for the Sewer Plague that Lark contracted",
            "Dain tried to argue, it didn't work",
            "Lady Atalia managed to escape from Kamed, some of her accomplices threw smoke bombs to help.",
            "It's slightly suspicious, but oh well, what can we do",
            "Level up!",
            "Started east traveling to Shadowtop Cathedral",
            "Ran into a group of teenagers who accidentally summoned a demon",
            "It was all the idea of 'Clarissa', a 'teenager' who recently showed up in town and is nowhere to be seen now",
            "Maybe we'll run into her later?",
            "Sold the giant waterskin to a traveling circus for 10gp",
            "Reached the town of Calling Horns",
        ],

        "May 29, 2022":
        [
            "Continued on to Shadowtop Cathedral",
            "Crossing a river, Otis transformed into a fish and found a driftglobe buried in the bottom of river bank",
            "Made it to Shadowtop Cathedral, it's an important meeting place to the Emerald Enclave",
            "Encountered a sad Satyr named Greenwhistle, pining for a lady dryad. We cheered him up with Purplefoot ale",
            "Talked with another Treant (Turlang) and a Half-Elf named Tharra, looking for where Aerglas is",
            "A tavern owner in Everlund named Quin, who adventured with Aerglas a bunch in the past, might know more about it",
            "On the way to Everlund, encountered a Fire Giant overseeing an excavation site where they found a big chunk of Adamantine",
            "The giant thinks were trying to steal the ore, yelling about how 'the Vonindod is not for the likes of us'",
            "The giant one shot Dain, doing 69 damage with a crit",
            "Lark charged into combat, pouring a health potion down Dain's throat, saving his life",
            "We slew the Fire Giant, taking its tuning fork",
            "We've heard a story about a construct that the giant's used to have, that was made of Adamantine",
            "This construct was destroyed at the end of the war between the giants and the dragons, the pieces scattered across the world and buried",
            "It looks like the giants are trying to rebuild this construct (the Vonindod?)",
            "We ended up leaving the chunk of Adamantine in the pit, it was way too heavy to move, and too big of a pit to fill in",
            "Quin doesn't know where Aerglas is, but is giving us an item that belonged to him, a Belt of Hill Giant Strength",
            "The trail has gone cold, but maybe we'll run into him at some point",
            "On to Silvery Moon to learn more about this dragon",
        ],
    },

    ////////////////////////
    // These are the things you own, organized by where they are stored.
    ////////////////////////
    Inventory:
    {
        "Coin Purse":
        [
            { Name: "CP", Amount:   0, Weight: 0.02, Cost:  0.01, Description: "" },
            { Name: "SP", Amount: 212, Weight: 0.02, Cost:  0.1 , Description: "" },
            { Name: "GP", Amount: 638, Weight: 0.02, Cost:  1   , Description: "" },
            { Name: "PP", Amount: 520, Weight: 0.02, Cost: 10   , Description: "" },
        ],

        "On Person":
        [
            { Name: "Arcane Focus"         , Amount: 1, Weight:  1, Cost:   10, Description: "Shard of a broken magic weapon that was forged by my Patron long ago" },
            { Name: "Backpack"             , Amount: 1, Weight:  5, Cost:    2, Description: "" },
            { Name: "Battleaxe"            , Amount: 1, Weight:  4, Cost:   10, Description: "" },
            { Name: "Eye In Platinum Vial" , Amount: 1, Weight:  0, Cost:  400, Description: "Used to cast Summon Aberration" },
            { Name: "Plate Mail"           , Amount: 1, Weight: 65, Cost: 1500, Description: "" },
            { Name: "Traveler's Clothes"   , Amount: 1, Weight:  4, Cost:    2, Description: "" },
            { Name: "Undead Eye In Gem"    , Amount: 1, Weight:  0, Cost:  150, Description: "Used to cast Shadow Of Moil" },
        ],

        "In Backpack":
        [
            { Name: "Brewer's supplies"          , Amount:   1, Weight:   9   , Cost:  20   , Description: "" },
            { Name: "Chef's Snack"               , Amount:   1, Weight:   0   , Cost:   0   , Description: "Bonus action to eat, get 3 temp HP" },
            { Name: "Crowbar"                    , Amount:   1, Weight:   5   , Cost:   2   , Description: "Gives advantage to Strength checks where the crowbar can be used" },
            { Name: "Dragon Scales"              , Amount:   1, Weight:   0   , Cost:   0   , Description: "Looted from the Goldenfields hill giants" },
            { Name: "Dust of Sneezing"           , Amount:   1, Weight:   0   , Cost:   0   , Description: "1 use in the pouch. From Keltar's treasure" },
            { Name: "Fairy Dust (Pinch)"         , Amount:  10, Weight:   0   , Cost:   0   , Description: "Taken from the evil cult member that attacked Zephyrus's tower, <a href=\"https://carouselddstormkingsthunder.obsidianportal.com/items/pixie-dust\">Effects Link</a>" },
            { Name: "Fine Clothes"               , Amount:   1, Weight:   4   , Cost:  15   , Description: "" },
            { Name: "Flask"                      , Amount:  20, Weight:   1   , Cost:   0.02, Description: "Filled with various ales and wines made by the Purplefoot clan" },
            { Name: "Hammer"                     , Amount:   1, Weight:   3   , Cost:   1   , Description: "" },
            { Name: "Healing Potion"             , Amount:   2, Weight:   1   , Cost:  50   , Description: "2d4 + 2" },
            { Name: "Healing Potion (Greater)"   , Amount:   1, Weight:   1   , Cost: 150   , Description: "4d4 + 4" },
            { Name: "Hempen Rope"                , Amount:  50, Weight:   0.2 , Cost:   0.02, Description: "" },
            { Name: "Keltar's Ring"              , Amount:   1, Weight:   5   , Cost:   0.2 , Description: "Given by Arleosa" },
            { Name: "Lantern of Revealing"       , Amount:   1, Weight:   0   , Cost:   0   , Description: "From Keltar's treasure" },
            { Name: "Mysterious Magic Rod"       , Amount:   1, Weight: 100   , Cost:   0   , Description: "Looted from a fire giant. 10 charges, Regain 1d6+4 per day. Can cast Locate Object with it. 10 mile range when finding Adamantine" },
            { Name: "Pitons"                     , Amount:  10, Weight:   0.25, Cost:   0.05, Description: "" },
            { Name: "Potion of Invulnerability"  , Amount:   1, Weight:   0   , Cost:   0   , Description: "From Zhi's inheritance" },
            { Name: "Potion of Poison Resistance", Amount:   3, Weight:   0   , Cost:   0   , Description: "From Chaz, to help against Old Knawbone" },
            { Name: "Rations"                    , Amount:  10, Weight:   2   , Cost:   0.5 , Description: "" },
            { Name: "Thieves tools"              , Amount:   1, Weight:   1   , Cost:  25   , Description: "Taken from goblin boss Hurk's horde. Bloodstained" },
            { Name: "Tinderbox"                  , Amount:   1, Weight:   1   , Cost:   0.5 , Description: "" },
            { Name: "Giant Necklace (Iron Ingot)", Amount:   1, Weight:   1   , Cost:   0   , Description: "Looted from a fire giant" },
            { Name: "Torches"                    , Amount:  10, Weight:   1   , Cost:   0.01, Description: "" },
            { Name: "Waterskin"                  , Amount:  11, Weight:   5   , Cost:   0.2 , Description: "Got a bunch to help out Artan" },
        ],
    },
};

//
// This is data that is generally changed more that once each level.
//
const DataThatChangesLessOften =
{
    ////////////////////////
    // This is your AC.
    ////////////////////////
    AC_Formula: "18", // Plate Mail Armor.

    ////////////////////////
    // These are reminders about special circumstances where your skills may be more or less effective.
    ////////////////////////
    SkillExceptionReminders:
    {
        History : "Expertise on checks related to the origin of stone work (Stonecunning, Mountain Dwarf)",
        Stealth : "Disadvantage (Plate Mail Heavy Armor)",
    },

    ////////////////////////
    // These are reminders about special circumstances where your saving throws may be more or less effective.
    ////////////////////////
    SavingThrowExceptionReminders:
    {
        Constitution : "Advantage on Concentration saves\nAdvantage if against poison (Dwarven Resilience, Mountain Dwarf)",
    },

    ////////////////////////
    // These are attacks that are done against your opponent's AC.
    ////////////////////////
    Attacks:
    [
        { Name: "Battleaxe"         , HitBonus_Formula: "AttackBonus    ", DamageDice: "1d8" , DamageBonus_Formula: "ChaMod"    , Range: "Melee", DamageType: "Slashing" },
        { Name: "Halberd (PoB)"     , HitBonus_Formula: "AttackBonus + 1", DamageDice: "1d10", DamageBonus_Formula: "ChaMod + 1", Range: "Reach", DamageType: "Slashing" },
        { Name: "Halberd Butt (PoB)", HitBonus_Formula: "AttackBonus + 1", DamageDice: "1d4" , DamageBonus_Formula: "ChaMod + 1", Range: "Reach", DamageType: "Bludgeoning" },
        { Name: "Magic Stone"       , HitBonus_Formula: "AttackBonus"    , DamageDice: "1d6" , DamageBonus_Formula: "ChaMod"    , Range: "60ft" , DamageType: "Bludgeoning" },
    ],

    ////////////////////////
    // These are attacks that force your opponent to make a saving throw.
    ////////////////////////
    SavingThrowAttacks:
    [
        { Name: "Mind Sliver"   , DC_Formula: "SaveDC", Stat: "Int", DamageDice: "2d6", DamageBonus_Formula: "0", Range: "60ft", DamageType: "Psychic", OnSuccess: "No Damage" },
        { Name: "Create Bonfire", DC_Formula: "SaveDC", Stat: "Dex", DamageDice: "2d8", DamageBonus_Formula: "0", Range: "60ft", DamageType: "Fire"   , OnSuccess: "No Damage" },
    ],

    ////////////////////////
    // These are the spells that you know or can prepare, organized by the spell's level.
    ////////////////////////
    Spells:
    [
        // Cantrip.
        [
            { Name: "Create Bonfire", IsPrepared: true, CastingTime: "Action"      , Range: "60ft" , Damage: "2d8", Type: "Dex Save", Components: "V S", Duration: "1m"     , IsConcentration: true , IsRitual: false },
            { Name: "Magic Stone"   , IsPrepared: true, CastingTime: "Bonus Action", Range: "Touch", Damage: "1d6", Type: "Attack"  , Components: "V S", Duration: "1m"     , IsConcentration: false, IsRitual: false }, // Warlock 1.
            { Name: "Mind Sliver"   , IsPrepared: true, CastingTime: "Action"      , Range: "60ft" , Damage: "2d6", Type: "Int Save", Components: "V"  , Duration: "1 round", IsConcentration: false, IsRitual: false }, // Warlock 1.

            /*
            // All possible spells.
            { Name: "Blade Ward"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Booming Blade"    , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Chill Touch"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Create Bonfire"   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Eldritch Blast"   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Friends"          , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Frostbite"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Green-Flame Blade", IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Infestation"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Lightning Lure"   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mage Hand"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Magic Stone"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mind Sliver"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Minor Illusion"   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Poison Spray"     , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Prestidigitation" , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Sword Burst"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Thunderclap"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Toll the Dead"    , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "True Strike"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            */
        ],

        // Level 1.
        [
          //{ Name: "Hex"             , IsPrepared: true, CastingTime: "Bonus Action", Range: "90 ft", Damage: "1d6" , Type: ""    , Components: "V S M", Duration: "1h", IsConcentration:  true, IsRitual: false }, // Warlock 1 (Forgotten in Warlock 5).
          //{ Name: "Hellish Rebuke"  , IsPrepared: true, CastingTime: "Reaction"    , Range: "60 ft", Damage: "2d10", Type: "Fire", Components: "V S"  , Duration: ""  , IsConcentration: false, IsRitual: false }, // Warlock 1 (Forgotten in Warlock 8).
          //{ Name: "Armor of Agathys", IsPrepared: true, CastingTime: "Action"      , Range: "Self" , Damage: ""    , Type: ""    , Components: "V S M", Duration: "1h", IsConcentration: false, IsRitual: false }, // Warlock 2 (Forgotten in Warlock 3).
          //{ Name: "False Life"      , IsPrepared: true, CastingTime: "Action"      , Range: "Self" , Damage: ""    , Type: ""    , Components: "V S M", Duration: "1h", IsConcentration: false, IsRitual: false }, // Warlock 2 (Fiendish Vigor Invocation).

            /*
            // All possible spells.
            { Name: "Armor of Agathys"             , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Arms of Hadar"                , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Cause Fear"                   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Charm Person"                 , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Comprehend Languages"         , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Expeditious Retreat"          , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Hellish Rebuke"               , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Hex"                          , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Illusory Script"              , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Protection from Evil and Good", IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Unseen Servant"               , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Witch Bolt"                   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Shield"                       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false }, // Hexblade 1 (Expanded Spell List)
            { Name: "Wrathful Smite"               , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false }, // Hexblade 1 (Expanded Spell List)
            */
        ],

        // Level 2.
        [
          //{ Name: "Blur"       , IsPrepared: true, CastingTime: "Action"      , Range: "Self" , Damage: "", Type: ""        , Components: "V"    , Duration: "1m" , IsConcentration: true , IsRitual: false }, // Warlock 3 (Forgotten in Warlock 7).
            { Name: "Hold Person", IsPrepared: true, CastingTime: "Action"      , Range: "60 ft", Damage: "", Type: "Wis Save", Components: "V S M", Duration: "1m" , IsConcentration: true , IsRitual: false }, // Warlock 3 (Replaced Armor of Agathys learned at Warlock 1).
            { Name: "Misty Step" , IsPrepared: true, CastingTime: "Bonus Action", Range: "30 ft", Damage: "", Type: ""        , Components: "V"    , Duration: ""   , IsConcentration: false, IsRitual: false }, // Warlock 4.

            /*
            // All possible spells.
            { Name: "Cloud of Daggers"   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Crown of Madness"   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Darkness"           , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Earthbind"          , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Enthrall"           , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Hold Person"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Invisibility"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mind Spike"         , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mirror Image"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Misty Step"         , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Ray of Enfeeblement", IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Shadow Blade"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Shatter"            , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Spider Climb"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Suggestion"         , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Blur"               , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false }, // Hexblade 3 (Expanded Spell List)
            { Name: "Branding Smite"     , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false }, // Hexblade 3 (Expanded Spell List)
            */
        ],

        // Level 3.
        [
            { Name: "Spirit Shroud" , IsPrepared: true, CastingTime: "Bonus Action", Range: "Self" , Damage: "1d8", Type: "", Components: "V S"  , Duration: "1m" , IsConcentration: true, IsRitual: false }, // Warlock 5.
            { Name: "Fly"           , IsPrepared: true, CastingTime: "Action"      , Range: "Touch", Damage: ""   , Type: "", Components: "V S M", Duration: "10m", IsConcentration: true, IsRitual: false }, // Warlock 5 (Replaced Hex learned at Warlock 1).
            { Name: "Enemies Abound", IsPrepared: true, CastingTime: "Action"      , Range: "120ft", Damage: ""   , Type: "", Components: "V S"  , Duration: "1m" , IsConcentration: true, IsRitual: false }, // Warlock 6.

            /*
            // All possible spells.
            { Name: "Counterspell"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Dispel Magic"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Enemies Abound"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Fear"                , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Fly"                 , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Gaseous Form"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Hunger of Hadar"     , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Hypnotic Pattern"    , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Intellect Fortress"  , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Magic Circle"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Major Image"         , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Remove Curse"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Spirit Shroud"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Fey"          , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Lesser Demons", IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Shadowspawn"  , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Undead"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Thunder Step"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Tongues"             , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Vampiric Touch"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Blink"               , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false }, // Hexblade 5 (Expanded Spell List)
            { Name: "Elemental Weapon"    , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false }, // Hexblade 5 (Expanded Spell List)
            */
        ],

        // Level 4.
        [
            { Name: "Shadow of Moil"       , IsPrepared: true, CastingTime: "1 action", Range: "Self" , Damage: "", Type: ""        , Components: "V S M", Duration: "1m", IsConcentration: true , IsRitual: false }, // Warlock 7 (Replaced Blur learned at Warlock 3).
            { Name: "Banishment"           , IsPrepared: true, CastingTime: "1 action", Range: "60ft" , Damage: "", Type: "Cha Save", Components: "V S M", Duration: "1m", IsConcentration: true , IsRitual: false }, // Warlock 7.
            { Name: "Summon Aberration"    , IsPrepared: true, CastingTime: "1 action", Range: "90ft" , Damage: "", Type: ""        , Components: "V S M", Duration: "1h", IsConcentration: true , IsRitual: false }, // Warlock 8.
            { Name: "Dimension Door"       , IsPrepared: true, CastingTime: "1 action", Range: "500ft", Damage: "", Type: ""        , Components: "V"    , Duration: ""  , IsConcentration: false, IsRitual: false }, // Warlock 8 (Replaced Hellish Rebuke learned at Warlock 1).
            
            /*
            // All possible spells.
            { Name: "Banishment"           , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Blight"               , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Charm Monster"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Dimension Door"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Elemental Bane"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Hallucinatory Terrain", IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Shadow of Moil"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Sickening Radiance"   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Aberration"    , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Greater Demon" , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Phantasmal Killer"    , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false }, // Hexblade 7 (Expanded Spell List)
            { Name: "Staggering Smite"     , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false }, // Hexblade 7 (Expanded Spell List)
            */
        ],

        // Level 5.
        [
          //{ Name: "Haste"                 , IsPrepared: false, CastingTime: "Action"      , Range: "30 ft" , Damage: ""             , Type: ""        , Components: "V S M", Duration: "1m" , IsConcentration: true , IsRitual: false }, // Warlock 9.
          //{ Name: "Leomund's Tiny Hut"    , IsPrepared: false, CastingTime: "1m"          , Range: "Self"  , Damage: ""             , Type: ""        , Components: "V S M", Duration: "8h" , IsConcentration: false, IsRitual: true  }, // Warlock 11.

            /*
            // All possible spells.
            { Name: "Contact Other Plane"  , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Danse Macabre"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Dream"                , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Enervation"           , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Far Step"             , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Hold Monster"         , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Infernal Calling"     , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Negative Energy Flood", IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Scrying"              , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Synaptic Static"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Wall of Light"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Banishing Smite"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false }, // Hexblade 9 (Expanded Spell List)
            { Name: "Cone of Cold"         , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false }, // Hexblade 9 (Expanded Spell List)
            */
        ],

        // Level 6.
        [
          //{ Name: "Leomund's Tiny Hut"    , IsPrepared: false, CastingTime: "1m"          , Range: "Self"  , Damage: ""             , Type: ""        , Components: "V S M", Duration: "8h" , IsConcentration: false, IsRitual: true  }, // Warlock 11 (Mystic Arcanum).

            /*
            // All possible spells.
            { Name: "Arcane Gate"               , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Circle of Death"           , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Conjure Fey"               , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Create Undead"             , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Eyebite"                   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Flesh to Stone"            , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Investiture of Flame"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Investiture of Ice"        , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Investiture of Stone"      , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Investiture of Wind"       , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mass Suggestion"           , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mental Prison"             , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Scatter"                   , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Soul Cage"                 , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Fiend"              , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Tasha's Otherworldly Guise", IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "True Seeing"               , IsPrepared: false, CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            */
        ],
    ],
};

//
// This is data that is generally changed only when a new level is gained.
//
const DataThatChangesOnLevelUp =
{
    ////////////////////////
    // This records what class you took at each level up.
    ////////////////////////
    Levels:
    [
        "Warlock", // Level 1
        "Warlock", // Level 2
        "Warlock", // Level 3
        "Warlock", // Level 4
        "Warlock", // Level 5
        "Warlock", // Level 6
        "Warlock", // Level 7
        "Warlock", // Level 8
    ],

    ////////////////////////
    // This allows you to override the default max HP formula with your own.
    // Use this if you are rolling for health.
    ////////////////////////
    OverrideMaxHP_Formula: undefined,

    ////////////////////////
    // These are the things that you can do when taking a short or long rest.
    ////////////////////////
    RestOptions:
    {
        "On Short Rest":
        [
            "Restore all spell slots", // Warlock 1 (Pact Magic).
        ],

        "On Long Rest":
        [
            "Change weapon I can use Charisma to attack with", // Hexblade 1 (Hex Warrior).
        ],
    },

    ////////////////////////
    // These are things that you can do during your turn in combat.
    ////////////////////////
    CombatOptions:
    {
        "Action":
        [
            "Cast a spell",       // Warlock 1 (Pact Magic).
            "Summon Pact Weapon", // Warlock 3 (Pact of the Blade).
        ],

        "Bonus Action":
        [
            "Hexblade's Curse",      // Hexblade 1 (Hexblade's Curse).
            "Buttend melee attack",  // Warlock 4 (Polearm Master feat)
          //"Eat \"special treat\"", // Warlock 4 (Chef Feat).
        ],

        "Reaction":
        [
            "Opportunity attack on <b>entered</b> reach", // Warlock 4 (Polearm Master feat)
          //"Armor of Hexes",                             // Hexblade 10 (Armor of Hexes).
        ],

        "On Turn":
        [
        ],

        "Any Time":
        [
        ],

        "Movement":
        [
        ],
    },

    ////////////////////////
    // These are all the things that your race, class, and background allow you to do.
    ////////////////////////
    Features:
    {
        "Mountain Dwarf":
        [
            { Name: "Speed"                 , Desc: [ "Your speed is not reduced by wearing heavy armor" ] },

            { Name: "Dwarven Resilience"    , Desc: [ "Advantage on saving throws against poison"
                                                    , "Resistance to poison damage" ] },

            { Name: "Stonecunning"          , Desc: [ "Have expertise for History checks related to the origin of stone work" ] },

            { Name: "Feat: Heavily Armored" , Desc: [ "+1 Strength"
                                                    , "Gain proficiency with heavy armor" ] },
        ],

        "Guild Artisan":
        [
            { Name: "Guild Membership"      , Desc: [ "As a member of a guild, get certain benefits"
                                                    , "My fellow build members will provide me with food and lodging if needed"
                                                    , "The guildhall offers a central place to meet people like potential patrons, allies, hirelings"
                                                    , "If accused of a crime, the guild will support me if they think I'm innocent or the crime was justified"
                                                    , "Gain access to powerful political figures, this may require donation of money or magical items to the guild though"
                                                    , "Guild membership costs 5gp per month" ] },
        ],

        Warlock:
        [
            // Level 1.
            { Name: "Pact Magic"            , Desc: [ "Get back spell slots on a short or long rest"
                                                    , "When leveling up can also replace a known spell with another spell from the list"
                                                    , "Has \"known spells\", doesn't prepare them"
                                                    , "Can use an arcane focus as a spellcasting focus" ] },

            { Name: "Hexblade's Curse"      , Desc: [ "As a bonus action, curse a creature within 30ft of me for 1 minute"
                                                    , "The curse ends if the target dies, I die, or if I'm incapacitated"
                                                    , "Damage rolls against the cursed creature get +{ProfBonus} extra damage"
                                                    , "Attack rolls against the cursed creature crit on a 19 or 20"
                                                    , "When the cursed creature dies, heal {WarlockLevel + ChaMod} HP" ] },

            { Name: "Hex Warrior"           , Desc: [ "After a long rest, can touch 1 weapon that isn't two-handed"
                                                    , "With this weapon, can use Charisma for attack and damage rolls"
                                                    , "If Pack of the Blade, then this can be applied to any type of weapon" ] },
            // Level 2.
            { Name: "Eldritch Invocations"  , Desc: [ "Gain <b>4</b> invocations"
                                                    , "When gaining a level, can forget one to learn another" ] },

            { Name: "I:Improved Pact Weapon", Desc: [ "Requires <b>Pact of the Blade</b>"
                                                    , "Can use Pact Weapon as a spellcasting focus"
                                                    , "Pact Weapon gets +1 to attack and damage rolls"
                                                    , "Pack Weapon can be a shortbow, longbow, light crossbow, or heavy crossbow" ] },

            { Name: "I:Thirsting Blade"     , Desc: [ "Requires <b>Pact of the Blade</b>"
                                                    , "Requires Warlock Level 5"
                                                    , "Can attack twice when taking the Attack action" ] },

            { Name: "I:Eldritch Smite"      , Desc: [ "Requires <b>Pact of the Blade</b>"
                                                    , "Requires Warlock Level 5"
                                                    , "After hitting a creature with my Pact Weapon, can expend a warlock spell slot"
                                                    , "Deals 1d8 + Nd8 extra damage to the target, where N is the level of the expended spell slot"
                                                    , "If the creature is Huge or smaller, can also knock them prone (no save!)" ] },

            { Name: "I:Eldritch Mind"       , Desc: [ "Advantage on Concentration saves" ] },

            /*
            { Name: "I:Tomb of Levistus"    , Desc: [ "Requires Warlock Level 5"
                                                    , "Big complicated explanation, lets you block a big attack once per short rest" ] },

            { Name: "I:Sculptor Of Flesh"   , Desc: [ "Requires Warlock Level 7"
                                                    , "Cast <b>Polymorph</b> once per long rest using a warlock spell slot" ] },

            { Name: "I:Life Drinker"        , Desc: [ "Requires <b>Pact of the Blade</b>"
                                                    , "Requires Warlock Level 12"
                                                    , "When hitting a create with my Pact Weapon, do an extra {ChaMod} necrotic damage" ] },
            */
            // Level 3.
            { Name: "Pact of the Blade"     , Desc: [ "Can use an action to create a melee Pact Weapon in my empty hand"
                                                    , "Can change the form of the weapon each time it's created"
                                                    , "Counts as magical for purposes of ignoring resistances"
                                                    , "Can spend 1 hour doing a ritual that turns an existing magic weapon into my Pact Weapon"
                                                    , "Afterwards, can summon this magic weapon as usual" ] },
            // Level 4.
            { Name: "Feat: Polearm Master"  , Desc: [ "When attacking with a glaive, halberd, or quarterstaff, can use a Bonus Action to make a special melee attack"
                                                    , "Attack does 1d4 bludgeoning damage"
                                                    , "Can spend reaction to do opportunity attack when a creature <b>enters</b> my reach" ] },
            // Level 5.
            // Learn Level 3 spells
            // Get 1 extra invocation

            // Level 6.
            { Name: "Accursed Specter"      , Desc: [ "After slaying a humanoid, can summon it's spirit as a Specter"
                                                    , "This Specter uses the stats from the Monster Manual"
                                                    , "It gets {Math.floor(WarlockLevel / 2)} temp HP"
                                                    , "Roll initiative for the specter, it takes its own turns"
                                                    , "It obeys my verbal commands"
                                                    , "It gets +{ChaMod} to its attack rolls"
                                                    , "It stays summoned until the end of my next long rest"
                                                    , "Can only do this one time per long rest" ] },
            /*
            // Level 7.
            // Learn Level 4 spells
            // Get 1 extra invocation

            // Level 9.
            // Learn Level 5 spells
            // Get 1 extra invocation

            // Level 10.
            { Name: "Armor of Hexes"        , Desc: [ "Can spend reaction when the target cursed by my Hexblade's Curse hits me with an attack roll"
                                                    , "Roll a d6, on a 4 or higher (i.e. 50% chance) the attack automatically misses me, regardless of what they rolled"] },
            // Level 11.
            // A 3rd spell slot
            { Name: "Mystic Arcanum (6th)"  , Desc: [ "Select one 6th level spell from the warlock spell list as my Arcanum"
                                                    , "Can cast this spell 1 time per long rest, without spending a spell slot"] },
            // Level 12.
            // ASI
            // Get 1 extra invocation
            { Name: "Feat: Chef"            , Desc: [ "+1 Constitution"
                                                    , "Gain proficiency with cook's utensils"
                                                    , "As part of a short rest, can cook special food for {4 + ProfBonus} creatures"
                                                    , "Must have cook's utensils and \"ingredients\" (whatever that means...)"
                                                    , "Anyone who eats the food and spends at least 1 Hit Die regains an extra 1d8 hit points"
                                                    , "Can spend 1 hour to cool {ProfBonus} \"special treats\", which last for 8 hours"
                                                    , "A creature can eat a special treat as a bonus action to gain {ProfBonus} temp HP" ] },
            */
        ],
    },
};

//
// This is data that generally only set when the character is first created.
//
const DataThatIsSetOnCharacterCreation =
{
    ///////////////////
    // This is your name.
    ///////////////////
    Name: "Dain Purplefoot",

    ///////////////////
    // This is the small image that is displayed next to your name.
    // It looks best if the image is square.
    ///////////////////
    HeadshotPicture: "Dain Purplefoot Headshot.png",

    ///////////////////
    // This is your initiative.
    ///////////////////
    Initiative_Formula: "DexMod",

    ///////////////////
    // This is your attack bonus and save DC.
    //
    // It makes sense for both magical and physical things. For example: weapon attacks,
    // spell attacks, Battle Master Fighter maneuvers, Monk Stunning Strike, etc...
    //
    // Set the formula to "undefined" if it doesn't make sense for your character (e.g. a barbarian without any save DCs).
    //
    // If you have more than one formula for your Attack Bonus or Save DC (e.g. you can
    // use different stats to cast spells) that is not well supported at the moment.
    ///////////////////
    AttackBonus_Formula: "ChaMod + ProfBonus",
    SaveDC_Formula     : "8 + ChaMod + ProfBonus",

    ///////////////////
    // This is how many spells you can prepare.
    //
    // Set it to "undefined" if you can't prepare any spells.
    ///////////////////
    NumSpellsPrepared_Formula: undefined,

    ///////////////////
    // This is how much extra HP you recover when you level up.
    ///////////////////
    ExtraHealthGainOnLevel_Formula: "ConMod",

    ///////////////////
    // This is how much extra HP you recover when you roll a hit die.
    ///////////////////
    HitDiceRollModifier_Formula: "ConMod",

    ///////////////////
    // If you're using the sidekick rules from Tasha's, this is how you specify the
    // starting health + hit dice that your creature's Monster Manual statblock has.
    //
    // These only apply if you have sidekick class levels (i.e. Expert, Warrior, or Spellcaster).
    //
    // Set all to "undefined" if this character isn't a sidekick.
    ///////////////////
    TashaSidekickHitDieSize         : undefined,
    TashaSidekickStartingNumHitDice : undefined,
    TashaSidekickStartingHealth     : undefined,

    ///////////////////
    // These are the things that you are proficient with.
    ///////////////////
    Proficiencies:
    {
        Weapons:
        [
            "Simple Weapons",  // Warlock 1.
            "Martial Weapons", // Hexblade 1 (Hex Warrior).
            "Battleaxe",       // Mountain Dwarf (Dwarven Combat Training).
            "Handaxe",         // Mountain Dwarf (Dwarven Combat Training).
            "Throwing Hammer", // Mountain Dwarf (Dwarven Combat Training).
            "Warhammer"        // Mountain Dwarf (Dwarven Combat Training).
        ],
        Armor:
        [
            "Light Armor",  // Warlock 1 & Mountain Dwarf (Dwarven Armor Training).
            "Medium Armor", // Hexblade 1 (Hex Warrior) & Mountain Dwarf (Dwarven Armor Training).
            "Heavy Armor",  // Warlock 1 (Heavily Armored Feat).
            "Shields",      // Hexblade 1 (Hex Warrior).
        ],
        Tools:
        [
            "Brewer's Supplies", // Mountain Dwarf (Tool Proficiency).
        ],
        Languages:
        [
            "Common",   // Mountain Dwarf.
            "Dwarvish", // Mountain Dwarf.
            "Giant",    // Guild Artisan background.
            "Elvish",   // Guild Artisan background.
        ]
    },

    ///////////////////
    // These are your physical attributes.
    ///////////////////
    PhysicalAttributes:
    {
        MoveSpeed              : 25,              // Mountain Dwarf.
        FlyingSpeed            : 0,               // Mountain Dwarf.
        SwimSpeed_Formula      : "MoveSpeed / 2", // Mountain Dwarf.
        ClimbSpeed_Formula     : "MoveSpeed / 2", // Mountain Dwarf.
        StandFromProne_Formula : "MoveSpeed / 2",
        Height                 : 4.5,             // Mountain Dwarf.
        Weight                 : 150,             // Mountain Dwarf.
        Size                   : "Medium",        // Mountain Dwarf.
        CarryingCapacitySize   : "Medium",        // Mountain Dwarf.
        Age                    : 100,
        DarkVisionDistance     : 60,              // Mountain Dwarf.
    },

    ///////////////////
    // These are your stats.
    ///////////////////
    Stats:
    {
        Strength     : { Value_Formula: "9 + 1"      }, // Rolled Stats + Heavily Armored Feat.
        Dexterity    : { Value_Formula: "10"         }, // Rolled Stats.
        Constitution : { Value_Formula: "15 + 2"     }, // Rolled Stats + Mountain Dwarf.
        Intelligence : { Value_Formula: "9"          }, // Rolled Stats.
        Wisdom       : { Value_Formula: "8"          }, // Rolled Stats.
        Charisma     : { Value_Formula: "16 + 2 + 2" }, // Rolled Stats + Mountain Dwarf + ASI.
    },

    ///////////////////
    // These are the saving throws that you are proficient in.
    ///////////////////
    ProficientSavingThrows:
    [
        "Charisma", // Warlock 1.
        "Wisdom",   // Warlock 1.
    ],

    ///////////////////
    // These are the skills that you are proficient in. Set the multiplier to 2 if you have Expertise.
    ///////////////////
    ProficientSkillMultipliers:
    {
        History       : 1, // Warlock 1.
        Investigation : 1, // Warlock 1.
        Persuasion    : 1, // Guild Artisan Background.
        Deception     : 1, // Guild Artisan Background.
    },

    ///////////////////
    // This is pretty much only for supporting the Bard's "Jack of all Trades" feature by setting it to 0.5.
    ///////////////////
    DefaultSkillProficiencyMultiplier: 0,

    ///////////////////
    // These are links to external websites that might have useful info.
    ///////////////////
    ExternalLinks:
    {
        "Campaign GDoc"       : "https://docs.google.com/document/d/1W6J3tFuKFKtTq3ICyNszw3RiHXsXOPEXK5x5S_D8uLI/edit",
        "Sword Mountains Map" : "https://images.app.goo.gl/YYfENLpaHJPaoCt18",
    },

    ///////////////////
    // Roleplay info about your character.
    ///////////////////
    Bio:
    {
        "Ideals":
        [
            "Trustworthy: a dwarf is only as good as their word",
            "Loyalty: you don't leave your friends or clan behind",
            "Quality: something worth doing is worth doing well",
        ],

        "Bonds":
        [
            "My clan",
            "My mountain",
            "The pact my clan has made",
            "Treasured possession: shard of patron's weapon (i.e. my arcane focus)",
        ],

        "Flaws":
        [
            "Pride",
            "Dain takes great pride in himself, his clan, and dwarves as a whole",
            "If someone thinks any of those things aren't amazing and great Dain becomes very defensive",
        ],

        "Backstory":
        [
            "Lives in the Sword Mountains, just north-west of Waterdeep",
            "A member of the Purplefoot clan, who are known all over the Sword Coast for their skill at brewing wine and ale",
            "Long ago, while mining deep in the mountain a member of the Purplefoot clan found a shard of a ancient magical weapon that had been shattered",
            "The mysterious being that originally forged the weapon entered into a pact with the clan. It would protect the dwarves and their mountain in exchange for them finding all the shards of the weapon so it could be reforged",
            "Recently, the Stone Giants who also live in the Sword Mountains have become more aggressive and started to make trouble",
            "Dain volunteered to go to the town of Nightstone, south-east of Waterdeep. A dwarf innkeeper named Morak Ur'gray lives there and he knows many things, he might have heard rumours of what is causing the giants to act so strangly",
            "The patron of the Purplefoot clan has granted Dain his Hexblade powers while he is on this adventure to help keep him safe and to satisfy its side of the pact",
        ],

        Picture: "Dain Purplefoot.png",

        "Emerald Enclave":
        [
            "Dain agrees with the goals and beliefs of the Emerald Enclave and tries to apply them in his daily life",
            "He hasn't interacted with other members on the Enclave very much, but then again, it's not the most process-heavy organization in the first place",
            "Belief 1: The natural order must be respected and preserved",
            "Belief 2: Forces that seek to upset the natural balance must be destroyed",
            "Belief 3: The wilderness can be harsh. Not everyone can survive in it without assistance",
            "Goal 1: To restore and preserve the natural order",
            "Goal 2: Keep the elemental forces of the world in check",
            "Goal 3: Keep civilization and the wilderness from destroying one another",
            "Goal 4: Help others survive the perils of the wilderness",
        ],
    },
};

//
// Smush all the data together into a single object, since it's only spread
// across multiple objects to make editing the data easier for the user.
//
const Character = Object.assign
(
    DataThatChangesOften,
    DataThatChangesLessOften,
    DataThatChangesOnLevelUp,
    DataThatIsSetOnCharacterCreation
);
