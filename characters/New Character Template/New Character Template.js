//
// Data that need to be updated often (e.g. every round of combat) is at the
// top of the file for easy editing.
//
const DataThatChangesOften =
{
    ////////////////////////
    // This is your heath-related info.
    ////////////////////////
    Health:
    {
        CurrentDamage_Formula : "3 + 4 + 7",
        TempHP                : 5,
        NumHitDiceSpent       : 5,
    },

    ////////////////////////
    // These are the resources that you can spend in or out of combat which replenish after resting.
    ////////////////////////
    Resources:
    [
        { Name: "Action Surge"        , NumSpent: 0, Max_Formula: "1"               , Recovery: "Short Rest" }, // Fighter 2.
        { Name: "Bardic Inspiration"  , NumSpent: 1, Max_Formula: "CharismaModifier", Recovery: "Short Rest" }, // Bard 1.
        { Name: "Champion of the Moon", NumSpent: 0, Max_Formula: "1"               , Recovery: "Long Rest"  }, // Champion of Selûne.
        { Name: "Combat Superiority"  , NumSpent: 4, Max_Formula: "5"               , Recovery: "Short Rest" }, // Fighter 3 + Fighter 7.
        { Name: "Indomitable"         , NumSpent: 1, Max_Formula: "1"               , Recovery: "Long Rest"  }, // Fighter 9.
        { Name: "Lucky"               , NumSpent: 2, Max_Formula: "3"               , Recovery: "Long Rest"  }, // Fighter 8.
        { Name: "Rage"                , NumSpent: 1, Max_Formula: "2"               , Recovery: "Long Rest"  }, // Barbarian 1.
        { Name: "Second Wind"         , NumSpent: 0, Max_Formula: "1"               , Recovery: "Short Rest" }, // Fighter 1.
    ],

    ////////////////////////
    // These are your spell slots.
    ////////////////////////
    SpellSlots:
    [
        { NumSpent: 4, Max: 5 },    // Level 1
        { NumSpent: 4, Max: 3 },    // Level 2
        { NumSpent: 1, Max: 3 },    // Level 3
        { NumSpent: 0, Max: 3 },    // Level 4
        { NumSpent: 0, Max: 0 },    // Level 5
        { NumSpent: 0, Max: 0 },    // Level 6
        { NumSpent: 2, Max: 3 },    // Level 7
        { NumSpent: 3, Max: 3 },    // Level 8
        { NumSpent: 1, Max: 3 },    // Level 9
    ],

    ////////////////////////
    // Record if you have inspiration, and if so, how you got it.
    ////////////////////////
    InspirationReason: "I have some inspiration! I did something unique and creative.",

    ////////////////////////
    // These are the things that have happened to you, organized by session.
    //
    // Each session header is the date that it happened. The date format is not
    // ISO 8601 (e.g. "2019-12-25") because that is always in the UTC timezone.
    // By using an implementation specific format (e.g. "Dec 25, 2019") the local
    // timezone is assumed. Firefox and Chrome seem to give consistent results
    // so that's good enough for us.
    ////////////////////////
    CampaignNotes:
    {
        "Oct 10, 2019":
        [
            "Did a thing",
            "Did another thing",
            "Did a third thing",
        ],

        "Nov 10, 2019":
        [
            "Did a thing",
            "Did another thing",
            "Did a third thing",
        ],

        "Dec 10, 2019":
        [
        ],

        "Dec 25, 2019":
        [
            "TPK",
        ],
    },

    ////////////////////////
    // These are the things you own, organized by where they are stored.
    // You can add new sections as desired.
    ////////////////////////
    Inventory:
    {
        "Coin Purse":
        [
            { Name: "CP", Amount: 16, Weight: 0.02, Cost: 0.01, Description: "" },
            { Name: "SP", Amount: 24, Weight: 0.02, Cost: 0.1 , Description: "" },
            { Name: "GP", Amount: 95, Weight: 0.02, Cost: 1   , Description: "" },
        ],

        "On Person":
        [
            { Name: "Arcane Focus"            , Amount: 1, Weight: 1, Cost:  10, Description: "Forehead gem" },
            { Name: "Deed to Willowdale Farm" , Amount: 1, Weight: 0, Cost:   0, Description: "1/4 mile^2 farm next to the Dragon's Flight B&B " },
            { Name: "Horse Tattoo, Lower Back", Amount: 1, Weight: 0, Cost:   0, Description: "Given by Orc tribe" },
            { Name: "Keyleth's research notes", Amount: 1, Weight: 0, Cost:   0, Description: "Taken from her tower in the swamp" },
            { Name: "Pearl"                   , Amount: 1, Weight: 0, Cost: 100, Description: "Perfect for use in the Identify spell" },
            { Name: "Shabby Robes"            , Amount: 1, Weight: 4, Cost:   1, Description: "Gifted by Dwayne" },
            { Name: "Spellbook"               , Amount: 1, Weight: 0, Cost:   0, Description: "Body tattoo/carving" },
        ],

        "In Backpack":
        [
            { Name: "Crooken Dagger"               , Amount:  1, Weight: 1  , Cost:   0  , Description: "+1 to damage/attack rolls <br> On hit, next spell cast within 1 minute gets double Prof Bonus added to DC/Attack roll <br> Taken from Orrick Honeyseed" },
            { Name: "Dagger"                       , Amount:  1, Weight: 1  , Cost:   3  , Description: "Bought from Dereval so he would shutup" },
            { Name: "Fine Inks"                    , Amount:  1, Weight: 0  , Cost:  50  , Description: "" },
            { Name: "Healer's Kit"                 , Amount: 28, Weight: 0.3, Cost:   0.5, Description: "" },
            { Name: "Moonstone"                    , Amount:  1, Weight: 0  , Cost:  50  , Description: "Taken by Orc tribe from Lizard Shaman's eye <br> White translucent gemstone with pale blue glow" },
            { Name: "Nightcap (magic?)"            , Amount:  1, Weight: 0  , Cost:   0  , Description: "Given as a a reward from from Aneleyal" },
            { Name: "Orc Tribe Friendship Talisman", Amount:  1, Weight: 0  , Cost:   0  , Description: "It's a horse's tail" },
            { Name: "Poison Vial (basic)"          , Amount:  4, Weight: 0  , Cost: 100  , Description: "Looted from Poison Jay's crew" },
            { Name: "Old Spellbook"                , Amount:  1, Weight: 3  , Cost:  50  , Description: "Very old, given by Loziver the Gnome <br> Heavily damaged, written in elvish <br> Has Glyph of warding, Ice storm, Ice knife" },
            { Name: "Swimming trophy"              , Amount:  1, Weight: 0  , Cost:   0  , Description: "I may have cheating using Alter Self..." },
            { Name: "Tree Seeds"                   , Amount:  1, Weight: 0  , Cost:   0  , Description: "Plant a tree for Jock" },
            { Name: "W.A.G.E Buttons"              , Amount:  8, Weight: 0  , Cost:   0  , Description: "Made by Deraval <br> Given to: Deraval, Dwayne, Spade, Trim" },
        ],

        "Somewhere":
        [
            { Name: "Chicken", Amount: 3, Weight: 0, Cost: 0, Description: "They are somewhere..." },
            { Name: "Cow"    , Amount: 1, Weight: 0, Cost: 0, Description: "They are somewhere..." },
            { Name: "Pig"    , Amount: 2, Weight: 0, Cost: 0, Description: "They are somewhere..." },
        ],
    },
};

//
// This is data that is generally changed more that once each level.
//
const DataThatChangesLessOften =
{
    ////////////////////////
    // This is your AC.
    ////////////////////////
    AC_Formula: "10 + DexterityModifier + DexterityModifier",

    ////////////////////////
    // These are reminders about special circumstances where your skills may be more or less effective.
    ////////////////////////
    SkillExceptionReminders:
    {
        Athletics     : "+5 for swimming or climbing (Gloves of Swimming and Climbing)\nAdvantage when Raging (Rage, Barbarian 1)",
        Investigation : "Advantage (Eyes of Minute Seeing)",
        Perception    : "Passive +5 (Observant Feat)",
        Stealth       : "Disadvantage (Scale Mail)",
    },

    ////////////////////////
    // These are reminders about special circumstances where your saving throws may be more or less effective.
    ////////////////////////
    SavingThrowExceptionReminders:
    {
        Strength     : "Advantage when Raging (Rage, Barbarian 1)",
        Dexterity    : "Advantage against effects you can see (Danger Sense, Barbarian 2)",
        Constitution : "Advantage if against poison (Stout Resilience, Stout Halfling)",
    },

    ////////////////////////
    // These are attacks that are done against your opponent's AC.
    ////////////////////////
    Attacks:
    [
        { Name: "Shortsword"    , HitBonus_Formula: "StrengthModifier + ProficiencyBonus + 1", DamageDice: "1d8" , DamageBonus_Formula: "StrengthModifier"    , Range: "Melee" , DamageType: "Slashing" },
        { Name: "Shortsword + 1", HitBonus_Formula: "StrengthModifier + ProficiencyBonus + 1", DamageDice: "1d8" , DamageBonus_Formula: "StrengthModifier + 1", Range: "Melee" , DamageType: "Slashing" },
        { Name: "Hand Crossbow" , HitBonus_Formula: "AttackBonus"                            , DamageDice: "1d10", DamageBonus_Formula: "CharismaModifier + 1", Range: "80/300", DamageType: "Piercing" },
        { Name: "Rapier"        , HitBonus_Formula: "AttackBonus"                            , DamageDice: "1d10", DamageBonus_Formula: "0"                   , Range: "80/300", DamageType: "Piercing" },
        { Name: "Spear"         , HitBonus_Formula: "AttackBonus"                            , DamageDice: "1d10", DamageBonus_Formula: "5"                   , Range: "80/300", DamageType: "Piercing" },
    ],

    ////////////////////////
    // These are attacks that force your opponent to make a saving throw.
    ////////////////////////
    SavingThrowAttacks:
    [
        { Name: "Fire Bolt"              , DC_Formula: "6"                                , Stat: "Int", DamageDice: "2d10"     , DamageBonus_Formula: "DexMod"              , Range: "Melee" , DamageType: "Slashing", OnSuccess: "No Damage"   },
        { Name: "Toll the Dead"          , DC_Formula: "8 + StrMod + ProficiencyBonus + 1", Stat: "Wis", DamageDice: "1d8"      , DamageBonus_Formula: "StrengthModifier + 1", Range: "Melee" , DamageType: "Slashing", OnSuccess: "No Damage"   },
        { Name: "Toll the Dead (damaged)", DC_Formula: "SaveDC"                           , Stat: "Dex", DamageDice: "1d12"     , DamageBonus_Formula: "CharismaModifier"    , Range: "80/300", DamageType: "Piercing", OnSuccess: "Half Damage" },
        { Name: "Thunder Wave"           , DC_Formula: "10"                               , Stat: "Con", DamageDice: "2d10"     , DamageBonus_Formula: "0"                   , Range: "Melee" , DamageType: "Slashing", OnSuccess: "No Damage"   },
        { Name: "Acid Splash"            , DC_Formula: "25"                               , Stat: "Cha", DamageDice: "1d6 + 1d4", DamageBonus_Formula: "-5"                  , Range: "Melee" , DamageType: "Slashing", OnSuccess: "No Damage"   },
    ],

    ////////////////////////
    // These are the spells that you know or can prepare, organized by the spell's level.
    ////////////////////////
    Spells:
    [
        // Cantrip.
        [
            { Name: "Eldritch Blast"        , IsPrepared: true , CastingTime: "Action"      , Range: "120 ft", Damage: "1d10"         , Type: "Attack"  , Components: "V S M", Duration: ""   , IsConcentration: false, IsRitual: false }, // Magic Initiate (Warlock) feat.
            { Name: "Friends"               , IsPrepared: true , CastingTime: "Action"      , Range: "Self"  , Damage: ""             , Type: ""        , Components: "S M"  , Duration: "1m" , IsConcentration: true , IsRitual: false }, // Wizard 1.
            { Name: "Light"                 , IsPrepared: true , CastingTime: "Action"      , Range: "Touch" , Damage: ""             , Type: ""        , Components: "V M"  , Duration: "1h" , IsConcentration: false, IsRitual: false }, // Wizard 4.
            { Name: "Mold Earth"            , IsPrepared: true , CastingTime: "Action"      , Range: "30 ft" , Damage: ""             , Type: ""        , Components: "S"    , Duration: ""   , IsConcentration: false, IsRitual: false }, // Wizard 1.
            { Name: "Prestidigitation"      , IsPrepared: true , CastingTime: "Action"      , Range: "10 ft" , Damage: ""             , Type: ""        , Components: "V S"  , Duration: "1h" , IsConcentration: false, IsRitual: false }, // Wizard 1.
            { Name: "Toll the Dead"         , IsPrepared: true , CastingTime: "Action"      , Range: "60 ft" , Damage: "1d8 or 1d12"  , Type: "Wis Save", Components: "V S"  , Duration: ""   , IsConcentration: false, IsRitual: false }, // Magic Initiate (Warlock) feat.
        ],

        // Level 1.
        [
            { Name: "Alarm"                 , IsPrepared: false, CastingTime: "1m"          , Range: "30 ft" , Damage: ""             , Type: ""        , Components: "V S M", Duration: "8h" , IsConcentration: false, IsRitual: true  }, // Wizard 1.
            { Name: "Comprehend Languages"  , IsPrepared: false, CastingTime: "Action"      , Range: "Self"  , Damage: ""             , Type: ""        , Components: "V S M", Duration: "1h" , IsConcentration: false, IsRitual: true  }, // Wizard 1.
            { Name: "Detect Magic"          , IsPrepared: false, CastingTime: "Action"      , Range: "Self"  , Damage: ""             , Type: ""        , Components: "V S"  , Duration: "10m", IsConcentration: true , IsRitual: true  }, // Taught by Dragonborn Wizard friend.
            { Name: "Find Familiar"         , IsPrepared: false, CastingTime: "1h"          , Range: "10 ft" , Damage: ""             , Type: ""        , Components: "V S M", Duration: ""   , IsConcentration: false, IsRitual: true  }, // Wizard 2.
            { Name: "Hex"                   , IsPrepared: false, CastingTime: "Bonus Action", Range: "90 ft" , Damage: ""             , Type: ""        , Components: "V S M", Duration: "1h" , IsConcentration: true , IsRitual: false }, // Magic Initiate (Warlock) feat.
            { Name: "Identify"              , IsPrepared: false, CastingTime: "1m"          , Range: "Touch" , Damage: ""             , Type: ""        , Components: "V S M", Duration: ""   , IsConcentration: false, IsRitual: true  }, // Wizard 2.
            { Name: "Magic Missile"         , IsPrepared:  true, CastingTime: "Action"      , Range: "120 ft", Damage: "3 * (1d4 + 1)", Type: "Auto Hit", Components: "V S"  , Duration: ""   , IsConcentration: false, IsRitual: false }, // Wizard 1.
            { Name: "Sleep"                 , IsPrepared:  true, CastingTime: "Action"      , Range: "90 ft" , Damage: "5d8"          , Type: "Auto Hit", Components: "V S M", Duration: "1m" , IsConcentration: false, IsRitual: false }, // Wizard 1.
            { Name: "Tenser's Floating Disk", IsPrepared: false, CastingTime: "Action"      , Range: "30 ft" , Damage: ""             , Type: ""        , Components: "V S M", Duration: "1h" , IsConcentration: false, IsRitual: true  }, // Wizard 1.
            { Name: "Unseen Servant"        , IsPrepared: false, CastingTime: "Action"      , Range: "60 ft" , Damage: ""             , Type: ""        , Components: "V S M", Duration: "8h" , IsConcentration: false, IsRitual: true  }, // Wizard 1.
        ],

        // Level 2.
        [
            { Name: "Alter Self"            , IsPrepared:  true, CastingTime: "Action"      , Range: "Self"  , Damage: ""             , Type: ""        , Components: "V S"  , Duration: "1h" , IsConcentration: true , IsRitual: false }, // Wizard 3.
            { Name: "Misty Step"            , IsPrepared: false, CastingTime: "Bonus Action", Range: "30 ft" , Damage: ""             , Type: ""        , Components: "V"    , Duration: ""   , IsConcentration: false, IsRitual: false }, // Wizard 4.
            { Name: "Rope Trick"            , IsPrepared: false, CastingTime: "Action"      , Range: "Touch" , Damage: ""             , Type: ""        , Components: "V S M", Duration: "1h" , IsConcentration: false, IsRitual: false }, // Wizard 4.
            { Name: "Skywrite"              , IsPrepared: false, CastingTime: "Action"      , Range: "Sight" , Damage: ""             , Type: ""        , Components: "V S"  , Duration: "1h" , IsConcentration: true , IsRitual: true  }, // Wizard 3.
        ],

        // Level 3.
        [
            { Name: "Animate Dead"          , IsPrepared:  true, CastingTime: "1m"          , Range: "10 ft" , Damage: ""             , Type: ""        , Components: "V S M", Duration: ""   , IsConcentration: false, IsRitual: false }, // Wizard 5.
            { Name: "Haste"                 , IsPrepared: false, CastingTime: "Action"      , Range: "30 ft" , Damage: ""             , Type: ""        , Components: "V S M", Duration: "1m" , IsConcentration: true , IsRitual: false }, // Wizard 6.
            { Name: "Remove Curse"          , IsPrepared: false, CastingTime: "Action"      , Range: "Touch" , Damage: ""             , Type: ""        , Components: "V S"  , Duration: ""   , IsConcentration: false, IsRitual: false }, // Wizard 5.
            { Name: "Leomund's Tiny Hut"    , IsPrepared: false, CastingTime: "1m"          , Range: "Self"  , Damage: ""             , Type: ""        , Components: "V S M", Duration: "8h" , IsConcentration: false, IsRitual: true  }, // Wizard 6.
            { Name: "Water Breathing"       , IsPrepared: false, CastingTime: "Action"      , Range: "30 ft" , Damage: ""             , Type: ""        , Components: "V S M", Duration: "24h", IsConcentration: false, IsRitual: true  }, // Wizard 6 Undead Thralls.
        ],

        // Level 4.
        [],

        // Level 5.
        [],

        // Level 6.
        [],

        // Level 7.
        [],

        // Level 8.
        [],

        // Level 9.
        [],
    ],
};

//
// This is data that is generally changed only when a new level is gained.
//
const DataThatChangesOnLevelUp =
{
    ////////////////////////
    // This records what class you took at each level up.
    ////////////////////////
    Levels:
    [
        "Barbarian",    // Level 1
        "Barbarian",    // Level 2
        "Warlock",      // Level 3
        "Barbarian",    // Level 4
        "Sorcerer",     // Level 5
        "Bard",         // Level 6
        "Warlock",      // Level 7
        "Expert",       // Level 8
    ],

    ////////////////////////
    // This allows you to override the default max HP formula with your own.
    // Use this if you are rolling for health, otherwise leave it undefined.
    ////////////////////////
    OverrideMaxHP_Formula: undefined,

    ////////////////////////
    // These are the things that you can do when taking a short or long rest.
    ////////////////////////
    RestOptions:
    {
        "On Short Rest":
        [
            "Song of Rest", // Bard 2.
        ],

        "On Long Rest":
        [
            "Change Prepared Spells", // Wizard 1.
        ],
    },

    ////////////////////////
    // These are things that you can do during your turn in combat.
    ////////////////////////
    CombatOptions:
    {
        "Action":
        [
            "Cast a spell", // Wizard 1.
        ],

        "Bonus Action":
        [
            "Enter a Rage",                                          // Barbarian 1.
            "Charger Feat: Melee Weapon Attack or Shove after Dash", // Stout Halfling, initial feat.
        ],

        "Reaction":
        [
        ],

        "On Turn":
        [
            "Reckless Attack", // Barbarian 2.
            "Lucky",           // Stout Halfling.
        ],

        "Any Time":
        [
            "Danger Sense", // Barbarian 2.
        ],

        "Movement":
        [
            "Halfling Nimbleness", // Stout Halfling.
        ],
    },

    ////////////////////////
    // These are all the things that your race, class, and background allow you to do.
    ////////////////////////
    Features:
    {
        "Stout Halfling":
        [
            { Name: "Lucky"                 , Desc: [ "Can re-roll, <b>one time</b>, a 1 on an attack roll, ability check, or saving throw"] },

            { Name: "Brave"                 , Desc: [ "Advantage on saving throws against being frightened"] },

            { Name: "Halfling Nimbleness"   , Desc: [ "Can move through the space of any creature that is a size larger than me"] },

            { Name: "Stout Resilience"      , Desc: [ "Advantage on saving throws against poison"
                                                    , "Resistance against poison damage" ] },

            { Name: "Feat: Charger"         , Desc: [ "If use action to Dash, can use bonus action to make one melee weapon attack or shove a creature"
                                                    , "If move 10ft in straight line first, get +5 to attack damage or push target 10ft" ] },
        ],

        Soldier:
        [
            { Name: "Military Rank"         , Desc: [ "Have a military rank: Cavalry"
                                                    , "Soldier loyal to that military recognize my authority and influence"
                                                    , "Defer to me if lower rank"
                                                    , "Can requisition simple equipment and horses for temporary use"
                                                    , "Can usually gain access to <b>friendly</b> military encampments and fortresses" ] },
        ],

        Barbarian:
        [
            // Level 1.
            { Name: "Rage"                  , Desc: [ "Advantage on Strength checks and saving throws"
                                                    , "Melee weapon attacks using strength get +{2} damage"
                                                    , "Resistance to bludgeoning, piercing, and slashing damage"
                                                    , "Can't cast spells or concentrate on them"
                                                    , "Lasts for 1 minute"
                                                    , "Ends early if unconscious, or haven't attacked or taken damage since end of last turn" ] },

            { Name: "Unarmored Defense"     , Desc: [ "AC = {10 + DexMod + ConMod}"
                                                    , "Can't be wearing armor"
                                                    , "Can be wearing a shield" ] },
            // Level 2.
            { Name: "Reckless Attack"       , Desc: [ "Get advantage on all melee weapon attacks using Strength"
                                                    , "All enemy attack rolls have advantage until my next turn"
                                                    , "Must decide when making first attack" ] },

            { Name: "Danger Sense"          , Desc: [ "Advantage on Dex saving throws against effects I can see (traps, spells, etc...)"
                                                    , "Can't be blinded, deafened, or incapacitated" ] },
        ],

        Paladin:
        [
            { Name: "Divine Sense"          , Desc: [ "As an action you know the location of any celestial, fiend, or undead within 60 feet that isn't behind total cover"
                                                    , "Can use it {CharismaModifier} times"
                                                    , "Recharge on long rest" ] },
        ],

        "Expression Test":
        [
            { Name: "TEST"                  , Desc: [ ``
                                                    , ` `
                                                    , `a`
                                                    , `{`
                                                    , `}`
                                                    , `{1}`
                                                    , ` {1}`
                                                    , `{1} `
                                                    , ` {1} `
                                                    , ` { 1 } `
                                                    , `{1}{2}`
                                                    , `{1}{2 + 3}`
                                                    , `{1} + {2} + {3}`
                                                    , `{1}+{2}+{3}`
                                                    , `{1 + 2 + 3}`
                                                    , `abc{1+(2*3)-4}def`
                                                    , `You can cast this Math.min(1, 7) times`
                                                    , `You can cast this {Math.min(1, 7)} times`
                                                    , `You can cast this {Math.min(1, 7) times`
                                                    , `You can cast this Math.min(1, 7)} times` ] },
        ]
    },
};

//
// This is data that generally only set when the character is first created.
//
const DataThatIsSetOnCharacterCreation =
{
    ///////////////////
    // This is your name.
    ///////////////////
    Name: "!!! CHANGE ME !!!",

    ///////////////////
    // This is the small image that is displayed next to your name.
    // It looks best if the image is square.
    ///////////////////
    HeadshotPicture: "New Character Template Headshot.png",

    ///////////////////
    // This is your initiative.
    ///////////////////
    Initiative_Formula: "5 + DexterityModifier + IntelligenceModifier",

    ///////////////////
    // This is your attack bonus and save DC.
    //
    // It makes sense for both magical and physical things. For example: weapon attacks,
    // spell attacks, Battle Master Fighter maneuvers, Monk Stunning Strike, etc...
    //
    // Set the formula to "undefined" if it doesn't make sense for your character (e.g. a barbarian without any save DCs).
    //
    // If you have more than one formula for your Attack Bonus or Save DC (e.g. you can
    // use different stats to cast spells) that is not well supported at the moment.
    ///////////////////
    AttackBonus_Formula: "CharismaModifier + ProficiencyBonus",
    SaveDC_Formula     : "8 + CharismaModifier + ProficiencyBonus",

    ///////////////////
    // This is how many spells you can prepare.
    //
    // Set it to "undefined" if you can't prepare any spells.
    ///////////////////
    NumSpellsPrepared_Formula: "Math.max(1, CharismaModifier + Math.floor(PaladinLevel / 2))",

    ///////////////////
    // This is how much extra HP you recover when you level up.
    ///////////////////
    ExtraHealthGainOnLevel_Formula: "ConstitutionModifier + 1",

    ///////////////////
    // This is how much extra HP you recover when you roll a hit die.
    ///////////////////
    HitDiceRollModifier_Formula: "ConstitutionModifier + 1",

    ///////////////////
    // If you're using the sidekick rules from Tasha's, this is how you specify the
    // starting health + hit dice that your creature's Monster Manual statblock has.
    //
    // These only apply if you have sidekick class levels (i.e. Expert, Warrior, or Spellcaster).
    //
    // Set all to "undefined" if this character isn't a sidekick.
    ///////////////////
    TashaSidekickHitDieSize         : 3,
    TashaSidekickStartingNumHitDice : 2,
    TashaSidekickStartingHealth     : 1,

    ///////////////////
    // These are the things that you are proficient with.
    ///////////////////
    Proficiencies:
    {
        Weapons:
        [
            "Simple Weapons", // Fighter 1.
        ],
        Armor:
        [
            "Light Armor", // Fighter 1.
        ],
        Tools:
        [

        ],
        Languages:
        [
            "Common",  // Warforged.
            "Gnomish", // Warforged.
            "Elvish",  // Slave background.
        ]
    },

    ///////////////////
    // These are your physical attributes.
    ///////////////////
    PhysicalAttributes:
    {
        MoveSpeed              : 25,              // Stout Halfling.
        FlyingSpeed            : 50,              // Stout Halfling.
        SwimSpeed_Formula      : "MoveSpeed / 2", // Stout Halfling.
        ClimbSpeed_Formula     : "MoveSpeed / 2", // Stout Halfling.
        StandFromProne_Formula : "MoveSpeed / 2",
        Height                 : 5.5,             // Stout Halfling.
        Weight                 : 180,             // Stout Halfling.
        Size                   : "Medium",        // Stout Halfling.
        CarryingCapacitySize   : "Large",         // Stout Halfling.
        Age                    : 70,
        DarkVisionDistance     : 60,              // Stout Halfling.
    },

    ///////////////////
    // These are your stats.
    ///////////////////
    Stats:
    {
        Strength     : { Value_Formula: "7 + 1"  }, // Point Buy.
        Dexterity    : { Value_Formula: "9"      }, // Point Buy.
        Constitution : { Value_Formula: "13"     }, // Point Buy.
        Intelligence : { Value_Formula: "12 - 1" }, // Point Buy.
        Wisdom       : { Value_Formula: "12"     }, // Point Buy.
        Charisma     : { Value_Formula: "16"     }, // Point Buy.
    },

    ///////////////////
    // These are the saving throws that you are proficient in.
    ///////////////////
    ProficientSavingThrows:
    [
        "Strength",     // Fighter 1.
        "Constitution", // Fighter 1.
        "Charisma",     // Fighter 1.
    ],

    ///////////////////
    // These are the skills that you are proficient in. Set the multiplier to 2 if you have Expertise.
    ///////////////////
    ProficientSkillMultipliers:
    {
        Acrobatics :  1, // Fighter 1.
        Stealth    :  2, // Fighter 1.
        Deception  :  4, // Fighter 1.
        Religion   : -1, // Fighter 1.
    },

    ///////////////////
    // This is pretty much only for supporting the Bard's "Jack of all Trades" feature by setting it to 0.5.
    ///////////////////
    DefaultSkillProficiencyMultiplier: 0.5, // Jack of all Trades

    ///////////////////
    // These are links to external websites that might have useful info.
    ///////////////////
    ExternalLinks:
    {
        "Willowdale"  : "https://docs.google.com/document/d/1Lcg_ONElDKa5VXuRwVjBSbaCdg91g43_SHOjtFPOu8s/edit#",
        "Warforged"   : "http://homebrewery.naturalcrit.com/share/SkgIesXIG",
        "Skill Feats" : "https://media.wizards.com/2017/dnd/downloads/UA-SkillFeats.pdf",
        "Jake's Feats": "https://docs.google.com/document/d/15QYw_1cUztvFhacsFftqmUKBoJg9f3ddSDmysRh1cSE/edit#",
    },

    ///////////////////
    // Roleplay info about your character.
    ///////////////////
    Bio:
    {
        "Ideals":
        [
            "Your ideals are the things that you believe in most strongly, the fundamental moral and ethical principles that compel you to act as you do",
            "Ideals encompass everything from your life goals to your core belief system.",
            "Ideals might answer any of these questions:",
            "What are the principles that you will never betray?",
            "What would prompt you to make sacrifices?",
            "What drives you to act and guides your goals and ambitions?",
            "What is the single most important thing you strive for?",
        ],

        "Bonds":
        [
            "Bonds represent a character's connections to people, places, and events in the world.",
            "They tie you to things from your background.",
            "They might inspire you to heights of heroism, or lead you to act against your own best interests if they are threatened.",
            "They can work very much like ideals, driving a character's motivations and goals.",
            "Bonds might answer any of these questions:",
            "Whom do you care most about?",
            "To what place do you feel a special connection?",
            "What is your most treasured possession?",
        ],

        "Flaws":
        [
            "Your character's flaw represents some vice, compulsion, fear, or weakness-in particular, anything that someone else could exploit to bring you to ruin or cause you to act against your best interests.",
            "More significant than negative personality traits, a flaw might answer any of these questions:",
            "What enrages you?",
            "What's the one person, concept, or event that you are terrified of?",
            "What are your vices?",
        ],

        "Physical Description":
        [
            "Tall",
            "Dark",
            "Handsome",
        ],

        Picture: "New Character Template.png",

        "Personality Traits":
        [
            "Tall",
            "Dark",
            "Handsome",
        ],
    },
};

//
// Smush all the data together into a single object, since it's only spread
// across multiple objects to make editing the data easier for the user.
//
const Character = Object.assign
(
    DataThatChangesOften,
    DataThatChangesLessOften,
    DataThatChangesOnLevelUp,
    DataThatIsSetOnCharacterCreation
);
