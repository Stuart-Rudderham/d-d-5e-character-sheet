//
// Data that need to be updated often (e.g. every round of combat) is at the
// top of the file for easy editing.
//
const DataThatChangesOften =
{
    ////////////////////////
    // This is your heath-related info.
    ////////////////////////
    Health:
    {
        CurrentDamage_Formula : "0",
        TempHP                : 0,
        NumHitDiceSpent       : 0,
    },

    ////////////////////////
    // These are the resources that you can spend in or out of combat which replenish after resting.
    ////////////////////////
    Resources:
    [
    ],

    ////////////////////////
    // These are your spell slots.
    ////////////////////////
    SpellSlots:
    [
    ],

    ////////////////////////
    // Record if you have inspiration, and if so, how you got it.
    ////////////////////////
    InspirationReason: "",

    ////////////////////////
    // These are the things that have happened to you, organized by session.
    //
    // Each session header is the date that it happened. The date format is not
    // ISO 8601 (e.g. "2019-12-25") because that is always in the UTC timezone.
    // By using an implementation specific format (e.g. "Dec 25, 2019") the local
    // timezone is assumed. Firefox and Chrome seem to give consistent results
    // so that's good enough for us.
    ////////////////////////
    CampaignNotes:
    {
        "Nov 25, 2021":
        [
            "Created my character!",
        ],
    },

    ////////////////////////
    // These are the things you own, organized by where they are stored.
    ////////////////////////
    Inventory:
    {
        "Coin Purse":
        [
            { Name: "CP", Amount: 0, Weight: 0.02, Cost: 0.01, Description: "" },
            { Name: "SP", Amount: 8, Weight: 0.02, Cost: 0.1 , Description: "" },
            { Name: "GP", Amount: 0, Weight: 0.02, Cost: 1   , Description: "" },
        ],

        "On Person":
        [
            { Name: "Backpack"     , Amount: 1, Weight:  5, Cost:  2, Description: "" },
            { Name: "Leather Armor", Amount: 1, Weight: 10, Cost: 10, Description: "AC = 11 + DexMod" },
            { Name: "Shortbow"     , Amount: 1, Weight:  2, Cost: 25, Description: "" },
            { Name: "Shortsword"   , Amount: 1, Weight:  2, Cost: 10, Description: "" },
        ],

        "In Backpack":
        [
          { Name: "Pan Flute"     , Amount: 1, Weight: 2, Cost: 12, Description: "Not magical, just an ordinary instrument" },
          { Name: "Thieves' tools", Amount: 1, Weight: 1, Cost: 25, Description: "" },
        ],
    },
};

//
// This is data that is generally changed more that once each level.
//
const DataThatChangesLessOften =
{
    ////////////////////////
    // This is your AC.
    ////////////////////////
    AC_Formula: "11 + DexMod", // Leather Armor.

    ////////////////////////
    // These are reminders about special circumstances where your skills may be more or less effective.
    ////////////////////////
    SkillExceptionReminders:
    {
    },

    ////////////////////////
    // These are reminders about special circumstances where your saving throws may be more or less effective.
    ////////////////////////
    SavingThrowExceptionReminders:
    {
        Strength     : "Advantage against spells and other magical effects (Magic Resistance, Satyr)",
        Dexterity    : "Evasion\nAdvantage against spells and other magical effects (Magic Resistance, Satyr)",
        Constitution : "Advantage against spells and other magical effects (Magic Resistance, Satyr)",
        Intelligence : "Advantage against spells and other magical effects (Magic Resistance, Satyr)",
        Wisdom       : "Advantage against spells and other magical effects (Magic Resistance, Satyr)",
        Charisma     : "Advantage against spells and other magical effects (Magic Resistance, Satyr)",
    },

    ////////////////////////
    // These are attacks that are done against your opponent's AC.
    ////////////////////////
    Attacks:
    [
        { Name: "Ram"       , HitBonus_Formula: "StrMod + ProfBonus", DamageDice: "2d4", DamageBonus_Formula: "StrMod", Range: "Melee"   , DamageType: "Bludgeoning" },
        { Name: "Shortsword", HitBonus_Formula: "DexMod + ProfBonus", DamageDice: "1d6", DamageBonus_Formula: "DexMod", Range: "Melee"   , DamageType: "Piercing" },
        { Name: "Shortbow"  , HitBonus_Formula: "DexMod + ProfBonus", DamageDice: "1d6", DamageBonus_Formula: "DexMod", Range: "80/320ft", DamageType: "Piercing" },
    ],

    ////////////////////////
    // These are attacks that force your opponent to make a saving throw.
    ////////////////////////
    SavingThrowAttacks:
    [
    ],

    ////////////////////////
    // These are the spells that you know or can prepare, organized by the spell's level.
    ////////////////////////
    Spells:
    [
    ],
};

//
// This is data that is generally changed only when a new level is gained.
//
const DataThatChangesOnLevelUp =
{
    ////////////////////////
    // This records what class you took at each level up.
    ////////////////////////
    Levels:
    [
        "Expert", // Level 1
        "Expert", // Level 2
        "Expert", // Level 3
        "Expert", // Level 4
        "Expert", // Level 5
        "Expert", // Level 6
        "Expert", // Level 7
        "Expert", // Level 8
    ],

    ////////////////////////
    // This allows you to override the default max HP formula with your own.
    // Use this if you are rolling for health.
    ////////////////////////
    OverrideMaxHP_Formula: undefined,

    ////////////////////////
    // These are the things that you can do when taking a short or long rest.
    ////////////////////////
    RestOptions:
    {
        "On Short Rest":
        [
        ],

        "On Long Rest":
        [
        ],
    },

    ////////////////////////
    // These are things that you can do during your turn in combat.
    ////////////////////////
    CombatOptions:
    {
        "Action":
        [
        ],

        "Bonus Action":
        [
            "Help",      // Expert 1 (Helpful)
            "Dash",      // Expert 2 (Cunning Action)
            "Disengage", // Expert 2 (Cunning Action)
            "Hide",      // Expert 2 (Cunning Action)
        ],

        "Reaction":
        [
        ],

        "On Turn":
        [
        ],

        "Any Time":
        [
        ],

        "Movement":
        [
        ],
    },

    ////////////////////////
    // These are all the things that your race, class, and background allow you to do.
    ////////////////////////
    Features:
    {
        "Satyr":
        [
            { Name: "Magic Resistance"      , Desc: [ "Advantage on saving throws against spells and other magical effects" ] },
        ],

        Expert:
        [
            // Level 1.
            { Name: "Helpful"               , Desc: [ "Can take the Help action as a bonus action" ] },

            // Level 2.
            { Name: "Cunning Action"        , Desc: [ "Can take the Dash, Disengage, or Hide actions as a bonus action" ] },

            // Level 3.
            //     Expertise

            // Level 4.
            //     ASI

            // Level 5.
            //     Proficiency Bonus increase

            // Level 6.
            { Name: "Coordinated Strike"    , Desc: [ "When using the <i>Helpful</i> feature, the targeted enemy can be 30ft away"
                                                    , "The next time I hit the targeted enemy with an attack it does an extra 2d6 damage" ] },
            // Level 7.
            { Name: "Evasion"               , Desc: [ "When doing a Dex save for half damage, take no damage on a success and only half damage on a fail"
                                                    , "Doesn't work if incapacitated" ] },
            // Level 8.
            //     ASI

            // Level 9.
            //     Proficiency Bonus increase

            // Level 10.
            //     ASI

            // Level 11.
            //     Inspiring Help (1d6)

            // Level 12.
            //     ASI
        ],
    },
};

//
// This is data that generally only set when the character is first created.
//
const DataThatIsSetOnCharacterCreation =
{
    ///////////////////
    // This is your name.
    ///////////////////
    Name: "Philoctetes",

    ///////////////////
    // This is the small image that is displayed next to your name.
    // It looks best if the image is square.
    ///////////////////
    HeadshotPicture: "Philoctetes Headshot.png",

    ///////////////////
    // This is your initiative.
    ///////////////////
    Initiative_Formula: "DexMod",

    ///////////////////
    // This is your attack bonus and save DC.
    //
    // It makes sense for both magical and physical things. For example: weapon attacks,
    // spell attacks, Battle Master Fighter maneuvers, Monk Stunning Strike, etc...
    //
    // Set the formula to "undefined" if it doesn't make sense for your character (e.g. a barbarian without any save DCs).
    //
    // If you have more than one formula for your Attack Bonus or Save DC (e.g. you can
    // use different stats to cast spells) that is not well supported at the moment.
    ///////////////////
    AttackBonus_Formula: "DexMod + ProfBonus",
    SaveDC_Formula     : undefined,

    ///////////////////
    // This is how many spells you can prepare.
    //
    // Set it to "undefined" if you can't prepare any spells.
    ///////////////////
    NumSpellsPrepared_Formula: undefined,

    ///////////////////
    // This is how much extra HP you recover when you level up.
    ///////////////////
    ExtraHealthGainOnLevel_Formula: "ConMod",

    ///////////////////
    // This is how much extra HP you recover when you roll a hit die.
    ///////////////////
    HitDiceRollModifier_Formula: "ConMod",

    ///////////////////
    // If you're using the sidekick rules from Tasha's, this is how you specify the
    // starting health + hit dice that your creature's Monster Manual statblock has.
    //
    // These only apply if you have sidekick class levels (i.e. Expert, Warrior, or Spellcaster).
    //
    // Set all to "undefined" if this character isn't a sidekick.
    ///////////////////
    TashaSidekickHitDieSize         :  8,
    TashaSidekickStartingNumHitDice :  7,
    TashaSidekickStartingHealth     : 31,

    ///////////////////
    // These are the things that you are proficient with.
    ///////////////////
    Proficiencies:
    {
        Weapons:
        [
            "Shortsword",     // Satyr.
            "Shortbow",       // Satyr.
            "Simple Weapons", // Expert 1 (Bonus Proficiencies).
        ],
        Armor:
        [
            "Light Armor", // Expert 1 (Bonus Proficiencies).
        ],
        Tools:
        [
            "Pan Flute",      // Expert 1 (Bonus Proficiencies).
            "Thieves' Tools", // Expert 1 (Bonus Proficiencies).
        ],
        Languages:
        [
            "Common", // Satyr.
            "Elvish", // Satyr.
            "Sylvan", // Satyr.
        ]
    },

    ///////////////////
    // These are your physical attributes.
    ///////////////////
    PhysicalAttributes:
    {
        MoveSpeed              : 40,              // Satyr.
        FlyingSpeed            : 0,               // Satyr.
        SwimSpeed_Formula      : "MoveSpeed / 2", // Satyr.
        ClimbSpeed_Formula     : "MoveSpeed / 2", // Satyr.
        StandFromProne_Formula : "MoveSpeed / 2",
        Height                 : 5.5,             // Satyr.
        Weight                 : 170,             // Satyr.
        Size                   : "Medium",        // Satyr.
        CarryingCapacitySize   : "Medium",        // Satyr.
        Age                    : 400,
        DarkVisionDistance     : 0,               // Satyr.
    },

    ///////////////////
    // These are your stats.
    ///////////////////
    Stats:
    {
        Strength     : { Value_Formula: "12"         }, // Satyr.
        Dexterity    : { Value_Formula: "16 + 2 + 2" }, // Satyr + ASI + ASI.
        Constitution : { Value_Formula: "11"         }, // Satyr.
        Intelligence : { Value_Formula: "12"         }, // Satyr.
        Wisdom       : { Value_Formula: "10"         }, // Satyr.
        Charisma     : { Value_Formula: "14"         }, // Satyr.
    },

    ///////////////////
    // These are the saving throws that you are proficient in.
    ///////////////////
    ProficientSavingThrows:
    [
        "Dexterity", // Expert 1 (Bonus Proficiencies).
    ],

    ///////////////////
    // These are the skills that you are proficient in. Set the multiplier to 2 if you have Expertise.
    ///////////////////
    ProficientSkillMultipliers:
    {
        Arcana        : 1, // Expert 1 (Bonus Proficiencies).
        Investigation : 2, // Expert 1 (Bonus Proficiencies) + Expert 3 (Expertise).
        Nature        : 1, // Expert 1 (Bonus Proficiencies).
        Perception    : 1, // Satyr.
        Performance   : 2, // Satyr.
        Religion      : 1, // Expert 1 (Bonus Proficiencies).
        SleightOfHand : 1, // Expert 1 (Bonus Proficiencies).
        Stealth       : 2, // Satyr + Expert 3 (Expertise).
    },

    ///////////////////
    // This is pretty much only for supporting the Bard's "Jack of all Trades" feature by setting it to 0.5.
    ///////////////////
    DefaultSkillProficiencyMultiplier: 0,

    ///////////////////
    // These are links to external websites that might have useful info.
    ///////////////////
    ExternalLinks:
    {
        "Campaign GDoc"       : "https://docs.google.com/document/d/1W6J3tFuKFKtTq3ICyNszw3RiHXsXOPEXK5x5S_D8uLI/edit",
        "Sword Mountains Map" : "https://images.app.goo.gl/YYfENLpaHJPaoCt18",
    },

    ///////////////////
    // Roleplay info about your character.
    ///////////////////
    Bio:
    {
        "Ideals / Bonds / Flaws":
        [
            "Satyrs crave the strongest drink, the most fragrant spices, and the most dizzying dances.",
            "A satyr feels starved when it can't indulge itself, and it goes to great lengths to sate its desires.",
            "It might kidnap a fine minstrel to hear lovely songs, sneak through a well-defended garden to gaze upon a beautiful lad or lass, or infiltrate a palace to taste the finest food in the land.",
            "Satyrs allow no festivity to pass them by. They partake in any holiday they've heard of. Civilizations of the world have enough festivals and holy days among them to justify nonstop celebration.",
            "Inebriated on drink and pleasure, satyrs give no thought to the consequences of the hedonism they incite in others. They leave such creatures mystified at their own behavior.",
        ],

        Picture: "Philoctetes.png",

        "Backstory":
        [
            "Has been hanging out with the Purplefoot clan for the last couple of years, sampling all their wares",
            "Starting to get bored",
            "When he heard that Dain Purplefoot was leaving the mountain he decided to follow on a whim",
            "Who knows what excitement he'll get up to?",
        ],
    },
};

//
// Smush all the data together into a single object, since it's only spread
// across multiple objects to make editing the data easier for the user.
//
const Character = Object.assign
(
    DataThatChangesOften,
    DataThatChangesLessOften,
    DataThatChangesOnLevelUp,
    DataThatIsSetOnCharacterCreation
);
