//
// Data that need to be updated often (e.g. every round of combat) is at the
// top of the file for easy editing.
//
const DataThatChangesOften =
{
    ////////////////////////
    // This is your heath-related info.
    ////////////////////////
    Health:
    {
        CurrentDamage_Formula : "0 + 8 + 24 - 2 - 20 + 7 - 11 + 4 + 3",
        TempHP                : 0,
        NumHitDiceSpent       : 2,
    },

    ////////////////////////
    // These are the resources that you can spend in or out of combat which replenish after resting.
    ////////////////////////
    Resources:
    [
        { Name: "Arcane Recovery"              , NumSpent: 1, Max_Formula: "1"        , Recovery: "Long Rest"  }, // Wizard 1 (Arcane Recovery).
        { Name: "Bladesong"                    , NumSpent: 2, Max_Formula: "ProfBonus", Recovery: "Long Rest"  }, // Bladesinger 2 (Bladesong).
        { Name: "Fate"                         , NumSpent: 3, Max_Formula: "3"        , Recovery: "Long Rest"  }, // Jake's Homebrew Rules.
        { Name: "Fury of the Small"            , NumSpent: 1, Max_Formula: "1"        , Recovery: "Short Rest" }, // Goblin.
      //{ Name: "Pearl of Power"               , NumSpent: 0, Max_Formula: "1"        , Recovery: "Long Rest"  }, // Item.
      //{ Name: "Sword of the Frost Elves"     , NumSpent: 0, Max_Formula: "1"        , Recovery: "Long Rest"  }, // Item.
        { Name: "War Pick of the Ember Dwarves", NumSpent: 1, Max_Formula: "1"        , Recovery: "Long Rest"  }, // Item.
        { Name: "Darkness"                     , NumSpent: 0, Max_Formula: "3"        , Recovery: "Long Rest"  }, // Item.
        { Name: "Bane"                         , NumSpent: 1, Max_Formula: "1"        , Recovery: "Long Rest"  }, // Item.
        { Name: "Yangen's Rocket Boots"        , NumSpent: 0, Max_Formula: "3"        , Recovery: "Long Rest"  }, // Item.
    ],

    ////////////////////////
    // These are your spell slots.
    ////////////////////////
    SpellSlots:
    [
        { NumSpent: 4, Max: 4 }, // Level 1
        { NumSpent: 3, Max: 3 }, // Level 2
    ],

    ////////////////////////
    // Record if you have inspiration, and if so, how you got it.
    ////////////////////////
    InspirationReason: "",

    ////////////////////////
    // These are the things that have happened to you, organized by session.
    //
    // Each session header is the date that it happened. The date format is not
    // ISO 8601 (e.g. "2019-12-25") because that is always in the UTC timezone.
    // By using an implementation specific format (e.g. "Dec 25, 2019") the local
    // timezone is assumed. Firefox and Chrome seem to give consistent results
    // so that's good enough for us.
    ////////////////////////
    CampaignNotes:
    {
        "Aug 4, 2022":
        [
            "Character creation!",
        ],

        "Aug 7, 2022":
        [
            "Session #1!",
            "Rumour I have heard #1: Lord Allistor's justice is swift and absolute. It would be a mistake to cause too much mischief in New Emsworth",
            "Rumour I have heard #2: An Aarakokra adventurer — Caw McGraw — took a 'Vow of Flightlessness' while studying at a monastery. They may also have some parrot blood in them",
            "Rumour I have heard #3: A Halfling woman in New Emsworth going by Piper owns the Pi-rate pie shop and it is believed her pies contain unnatural ingredients",
            "We're staying at the Plot and Hook inn, it costs a Modest amount of living each day (1gp)",
            "This is where our big map is that we're filling out",
            "Vivian Warner is the owner of the inn",
            "She loves books and knowledge, one wall of the inn is a big wall of books",
            "The sheriff, Captain Hand of Brass, is a warforged. He put up a Wanted poster in the Inn",
            "Gretchen Bailey, Thief, Wanted Dead. Her and her band of ruffians are squatting in the abandoned quarry",
            "The quarry is 1 hour northwest of the town of New Emsworth",
            "The reward for killing her is 100gp",
            "My party members are: Caw McGraw an Aarakokra 'cowboy', and Piper a halfling",
            "We learned that Paladin Thornseeker, who Caw is looking to find, left to go to a temple of Gailian, which is 2 hours northeast of town",
            "Encountered 2 vultures eating a couple of goblin corpses",
            "Used mage hand to try and take some loot from the corpses, got a couple of items but it attacted a vulture, which we had to fight",
            "The temple is carved into a cliff face, tie a rope to a nearby tree and climb down 15ft to the door of the temple",
            "During a fight, a desk I was standing on collapsed. I failed my acrobatics check and got Broken Fingers",
            "Broken Fingers: You can no longer hold anything with two hands, and you can hold only a single object at a time",
            "We explored the rest of the temple, it wasn't super large",
            "We didn't find the Pool of Gailian, which Caw and Paladin Thornseeker are looking for",
            "Thornseeker seemed to have been frustrated, they trashed the room a bit",
            "Found a new location for a holy site, some meditation stones. They are to the east of some purple flowers",
            "We went back to town, Piper healed my Broken Fingers with a Cure Wounds",
            "Level up!",
            "We decided we're going to go to the quarry to find and kill Gretchen",
            "Went into the quarry, fought Gretchen",
            "Scab pushed her into a 60ft deep pit. We thought she died but she landed on some soft fungus and looks to be ok",
            "There is a damaged crane, once we repair it we could get down there",
            "We read some letters to Gretchen from M, talking about another place Gretchen should send some people. Elven ruins?",
            "Went back to town, going to try and find someone to repair the crane"
        ],

        "Aug 21, 2022":
        [
            "Found a dwarf in town who could repair the crane for us. Olga and her dwarf crew. She charged us 50gp in total",
            "She repaired the crane in record time, and left an Ironborn to operate it for us",
            "We go down into the pit",
            "This cavern is made of brick and tile, it's rectangular. This is clearly humanoid-made",
            "All the walls are covered in black handprints, like the room we found Gretchen in above",
            "There two exits from the room, and we can see *2* paths coming from the patch where Gretchen fell",
            "We take the path that didn't exist when we saw Gretchen fall, since it's newer",
            "As we step into the mushrooms we fail a Con saving throw and start coughing. We are probably diseased",
            "We explore through the cave, and come across a decaying corpse that seems to be in the process of being consumed",
            "The corpse has some sweet magic shoes on though, which Scab wants!",
            "It seems like this elf was an adventurer (bard) who somehow came down here after we pushed Gretchen",
            "Scab tries to get the boots off with his mage hand. I failed to check but spent a Fate token to succeed",
            "I get the boots, but we ended up having to fight some large centipedes",
            "We go back down the other path, the one that we think Gretchen went down",
            "We entered a large cavern, lots of mushrooms, centipedes, and egg sacks",
            "There was a treasure chest in the cavern, Scab ran through swarms of enemies to get it, it has gold and healing potions. A little disappointing",
            "We encountered a deep pool of water, runes around it, turns out this used to be a temple of Scintilla, a goddess of light and protection",
            "The pool has some sort of magic item at the bottom of it. Use mage hand to get it, a small emblem in the shape of a shield",
            "We reached the end of of the temple. There was a statue of Scintilla that has been corrupted by fungus!",
            "Gretchen is standing under the corrupted statue, she has been half consumed by fungus",
            "She spoke to us about 'Him' and thanked us for pushing her down the pit into the fungus. Who is 'Him'?",
            "We fought Gretchen, killed her, got her sword + head",
            "She was mostly mushroom instead, definitely not natural, some sort of magical corruption. It gave her supernatural strength and toughness",
            "There is Abyssal text make of fungus below the corrupted statue. Saying it is a monument to Kotus, Demon of a Million Hands",
            "We make it back to town, and find a priest (Oswald the Gluttonous) who can cure us of our mushroom disease",
            "Piper bribes him with a steep discount on her pies, so it costs us 10gp each",
            "We went to the sheriff, gave him the mushroom head/sack, got 100gp",
            "Level up!",
        ],

        "Sep 4, 2022":
        [
            "Sold the Holy Emblem of Scintilla to Vivian Warner for 100gp for her to hold onto and display",
            "Hour north of town, wrecked ship, black sails, adventurers went there looking for treasure and encountered some trouble",
            "We're going to go check it out",
            "One of the dead adventurers had a necklace that we're going to try and retrieve for them. We're doing this in exchange for info about the shipwreck",
            "Turns out they were ambushed by a giant crab that had buried itself in the sand",
            "We managed to reverse-ambush the crab, flaming sphere is a pretty good spell",
            "We got the necklace that the adventurers wanted",
            "The ship is 40ft out into the very cold water",
            "We do a dumb plan to try and get out there using Piper's flame spirit's teleport. It doesn't work great, we still get cold and wet",
            "The entrance of the ship is full of bones, and it's very smelly inside. There's probably something spooky in there",
            "Fought some Troglodytes, killed most, scared them off",
            "Looted some chests in the cargo hold, went to investigate the Captain's cabin",
            "There was a very fancy chest in it, I got the key off of the captain's body and opened it",
            "Got some cool magic items + TREASURE MAP!",
            "Went back to town, returned the necklace to the adventurers",
            "Learned more about the treasure map, we have 2 options for where it might be",
            "The sheriff hung up another Wanted poster in the inn",
            "It is calling on people to go after bandits that attacked Prospector Freebeard. They are to the East of New Emsworth. Reward is 200gp"
        ],

        "Sep 25, 2022":
        [
            "Regaled Caw with our adventures without him",
            "Decided to head to the meditation stones to the south-east of New Emsworth",
            "We made it to the purple flower patch, turns out it's a very magical area. The purple flowers are huge, as big as trees",
            "The air is heavy with magic, and it seems potentially related to the Fey",
            "We followed a brook through the area, and came across an Owlbear mama with 2 cubs",
            "The mama bear used a psychic roar on us, she had weird purple spirals in her eyes. It almost stunned all of us, but we used Fate tokens to ignore it",
            "A papa Owlbear appeared, used the same psychic roar, we all used Fate tokens again. Caw still went down after taking 2 damage",
            "We kill the Owlbears, with the last cub being killed by Paladin Thornseeker who charged out of the forest to help us",
            "She wears heavy plate armor, with her helmet having two large, very sharp antlers that she skewered the cub with as she charged",
            "She only has 1 arm",
            "We took a short rest with her",
            "She explains that she's been on this quest for 8 years, she's looking for the Pool of Gailian to try and get her missing arm healed",
            "Caw is doing something similar, trying to heal his broken wings",
            "Thornseeker is very grim, not what you would expect of someone who follows Gailian. It seems like she's grown jaded and weary during her quest",
            "Thornseeker seems to be a Paladin of the Ancients",
            "We make it through the purple forest without any more problems and come out the other side, which is grass covered hills",
            "Scab asks Caw how many people can use the Pool of Gailian, if we might have trouble with Thornseeker. Caw doesn't know",
            "We come to the meditation stones, there are lots of stones flying through the air on orbits",
            "There are people jumping between stones, trying to get up to the top of a mesa that is 40ft up",
            "Some of them fall to their deaths, the rest of the people (worshipers of Gailian) cheer in joy when this happens",
            "We go closer, Scab determines that there are some patterns in the orbits and figures out a path upwards",
            "We also notice that the worshipers are all goblins, who aren't happy to see us and start attacking",
            "We jump on to one of the orbits, Caw doesn't make it on the same rock as us and eventually goes down!",
            "Piper jumps off to save Caw, Thornseeker and Scab going around, make it to the top of the mesa, kill all the goblins themselves",
            "At the top there are 3 pillars with carvings on them, telling a story about how to find the pool",
            "Pictures of priests of Gailian feasting at the top of the mesa, and then using the pool in a place that north of an elven city",
            "We have a party on the mesa with Paladin Thornseeker. Afterwards we know that the Pool is an hour North of the elven ruins",
            "We don't think Thornseeker knows where, do we want to invite her when we go find it? Unsure...",
            "Caw levels up!",
            "We go talk to Prospector Freebeard, he's a dwarf with very fancy gold armor",
            "4 hours east of New Emsworth, he and a group of guards were prospecting in a hilly area, they were ambushed and the guards were killed",
            "He says they were dwarves, not bandits, wearing dark black armor. Freebeard knows all the dwarves in New Emsworth and didn't recognize them",
            "He thinks those dwarves are guarding a treasure",
        ],

        "Oct 2, 2022":
        [
            "Spent some downtime trying to research about the Pool of Gailian",
            "We're still not sure if we want to bring Paladin Thornseeker, what if the pool can only be used once?",
            "Caw got us access to the library of the Church of Gailian, to do research",
            "The pool is a real thing, there are multiple across the world",
            "There is a requirement to use the pool. Each pool is different, but there will be something there that lets you know what to do",
            "The pool is only usable once every few years. It's normally reserved for heroes, or important church folk",
            "So it sounds like only one person will be able to use it, sucks to be Paladin Thornseeker",
            "Caw is unsure, since he's a good person. We go to talk to Thornseeker",
            "She's so happy to hear that we know where the pool is that she hugs Caw!",
            "We decide to travel there together, and make any hard decisions once we get there",
            "We eventually find the elven ruins, they weren't exactly where we had though based on the map, it was more south. The map has been updated",
            "We're going around the ruins to find the Pool, since we don't want to deal with the bandits we know are in the ruins",
            "We do see tracks of bandit patrols around the ruins, this knowledge will come in helpful later",
            "We continue on, encountering goblins who we kill",
            "We go to a clearing an get ambushed by more goblins, they were hiding in the snow",
            "We killed them all, and Scab spent his last Fate token to unlock a fancy chest in their encampment. Alchemist jug!",
            "We find the pool, there are goblins + a shaman there. Scab dislocates his shoulder killing a goblin, gaining disadvantage on melee attacks",
            "We beat the shaman, she revealed with her dying breath that only one person could use the pool. Oh no!",
            "Spent some time doing important things first, looting treasure chests",
            "Used Comprehend Languages to read some runes next to the pool. It (kind of) tells us what to do to use the pool",
            "We all do a group check to try and understand, Caw and Piper fill in the missing gaps",
            "The person to be healed has to be carried into the pool by a group of singing people, and float on their back",
            "We talk to Thornseeker, she realizes that healing her arm wouldn't really bring back her joy, so Caw can use the pool",
            "We heal Caw's wings, and she takes Thornseeker for a flight. We hear Thornseeker laugh for the first time, it's all very sweet",
            "Piper uses Cure Wounds on Scab, healing some damage and his injury",
            "We make it back to New Emsworth, getting a little exhausted on the way back",
            "We attune to the magic items, the wand was cursed! It turned me into a little bunny rabbit",
            "We paid our old friend Oswald the Gluttonous to lift the curse. The wand is still cursed. Maybe a gift for the Goblin King?",
        ],

        "Nov 20, 2022":
        [
            "We decide to go investigate Prospector Freebeard's ambush site. Treasure!",
            "Travel there, find the ambush site. It looks like the black armored dwarves didn't take anything, tools and dead guard armor was still there",
            "Investigated the dead bodies and cliff face, found nothing (we rolled real bad)",
            "Piper found some tracks leading off into the forest, so we follow those",
            "As we're following the tracks we are ambushed by dwarves",
            "The dwarves have white beards and white/pale skin. Black armor",
            "We defeat the dwarves, but in the process the set part of the forest on fire and we're forced to retreat a bit back to the camp site",
            "We take the body of the mage with us, since we've lost the path we were following anyways",
            "We see a opening in the cliff face now, it seems they came out of a secret door",
            "We go into the tunnel, fight some guards guarding a fancy entrance way to the dwarven city",
            "We lay a trap at the front of the door, laying down some ball bearings",
            "The trap went very poorly, Piper went down briefly, but we ended up defeating them",
            "They kept casting burning hands on our clumped up group, it was the worst",
            "Scab opened a flaming treasure chest, got some cool loot",
            "We reach the end of the city, there are some dwarves with a female leader (Dura)",
            "There is a carving of Ignix the Unrelenting, her god. She says the god wants to rule over the land, as is its right. It will consume the land",
            "Probably the volcano on the island will be involved",
            "We fight Dura, kill her and take her flaming War Pick",
            "We went back to town, long rest, talked to Hand of Brass, got the 200gp reward",
        ],

        "Nov 27, 2022":
        [
            "Gave the Short Sword of the Frost Elves to Caw",
            "A Halfling lady adventurer (Lydia Stoutfellow) comes into the Plot and Hook. She's a member of the Vermilion Cloaks, an adventuring group",
            "They've gotten into some trouble. They were hired by a merchant in town to go get 'crafting resources'",
            "They were captured, she just managed to sneak away, she wants us to rescue her friends",
            "They were assaulted by 2 white skinned trolls and other creatures. There were some 'grubs on sleds' that spit things at them",
            "She says there were 6 party members to be rescued, and for each one that's rescued we'll get some of their loot",
            "We're heading out, it's an 8 hour journey. We're probably going to have to camp overnight. We buy tents",
            "We find the clearing where the Vermilion Cloaks were ambushed, and the sled tracks that lead away from it. We follow the tracks",
            "Lydia is setting up a base camp to try and heal anyone we rescue",
            "We follow the sled tracks to the coast, where we find lots of ice. The tracks lead out into the ice",
            "We start to see the lights of fires above our heads. There's a massive igloo built onto the ice",
            "We find a big skeleton embedded in the ice, just like in the treasure map! The treasure chest is to the south-east of here",
            "We fight some trolls and some grubs, no big deal",
            "There are some smaller igloos attached to the main one. They have a smoke hole, so Caw looks down inside",
            "He sees 2 trolls inside and a human man next to a soup pot. We have to save him quick before he gets eaten",
            "We climb up a rope, jump down inside and fight the trolls",
            "Scab was overconfident, took a 15 damage hit from a troll and went down",
            "Piper also went down, it was a spooky fight. Very cramped, couldn't maneuver at all",
            "The man we saved was Devon LeBlanc, he was the ranger of the group and the original founder",
            "Scab bullied him into giving up his cloak, Lydia told us the secret phrase to say",
            "We sealed the single door to the kitchen by melting the ice door and freezing it shut, took a short rest",
            "Devon said he saw a troll chief and at least 2 other trolls",
            "The session ended here, we ran out of time",
        ],

        "Dec 29, 2022":
        [
            "Caw looks down a hole at the top of the main igloo, he sees two humanoids tied to a pole on a wooden platform in the middle",
            "There are 2 trolls and some grubs in the main room",
            "We make a plan to try and lure just one of the trolls into the kitchen by banging on the stew pot with a ladle",
            "Scab failed his Performance check and had to spend a fate token to succeed",
            "The plan worked, we separated one of the trolls and took it out without too much trouble",
            "Caw killed it by shooting it in the butt with a crossbow bolt",
            "The other troll came over, we got him too. Piper melted the rest of the grubs with a moonbeam",
            "We spoke to the two captured members of the Vermilion Cloaks, one very white themed and one very black themed",
            "White: Cassandra Castillian (Wizard), Black: Blake Lively (Warlock)",
            "Scab uses a Lesser Healing Potion to heal Blake, he was hurt and unconscious",
            "Cassandra gives us her white broach with black feather (cast Light as an action, cast Daylight once per day)",
            "Blake gives us his black ring with white skull (cast Darkness 3 times per day)",
            "These are their wedding gifts to each other",
            "We open the door to another room, it's dark and there are *4* sleeping trolls",
            "A female goblin is at the far end of the room, her name is Jingle Jangle and she's a bard with a bell covered jester hat",
            "Caw and Piper stay at the entrance since they can't see in the dark, so it's the Scab Show to save the day",
            "The floor was icy, so Scab had to make Acrobatics checks to avoid sliding into sleeping trolls",
            "He used Bladesong to get Advantage on these checks, still had to spend all my Fate to get through",
            "There was also 3 treasure chests in the room, Scab looted one and left the others, too much risk",
            "Jingle Jangle was frozen to the wall, Scab used his Alchemist Jug to pour acid on the ice to melt it",
            "We escaped, and Jingle Jangle grudglingly gave us her hat. She will no longer Jingle or Jangle, in the short term at least",
            "Jingle Jangle's hat lets us cast Alarm 3 times per day",
            "We just open the door to the chief's room, no fancy plans",
            "There is a heavily armored half elf (Everit Wilson) and an old halfling (Ada Lovelace) wearing chain and a holy symbol of Mors (hourglass)",
            "The chief is monologuing to his captives in Giant, they can't understand him. He has a fancy cape and a bone crown",
            "We fight the chief, he has a power where he can encase himself in ice and gain temp health",
            "We managed to take out all the temp health in 1 turn and the ice shattered, stunning the chief, who we continued to bully",
            "The chief managed to use his temp health ability again, and this time since we didn't deplete the temp health he made it explode and damage us all",
            "The chief retreated to his throne, with 1 health remaining, and Piper burned him up with a Flaming Sphere",
            "Everit gave us a +1 Longsword of Heroism (cast Heroism 1 time per day), Ada gave us a Bane Helm (cast Bane 1 time per day)",
            "We took the chief's magic ice cloak as well, need to identify it",
            "Level up!",
        ],

        "Jan 8, 2023":
        [
            "We sell some magic items, identified the frost troll's cape",
            "Decide to do the treasure map quest next, since the other only option is the dead elf city + Kotus quest line",
            "We set out, encounter an elf being attacked by two frost trolls. We kill 1 and the other flees",
            "He gives us some salmon tempHP treats as a reward",
            "We reach the bones and start heading 2 hours south-east",
            "The forest transitions from healthy snowy forest into a dead forest filled with skeletons of trees, no leaves",
            "We go up a hill and find a decrepit manor, maybe hundreds of years old",
            "Very gothic, gargoyles and stuff on the facade",
            "Behind the house is the large tree that we saw on our map, the treasure must be there!",
            "We scout around the house to go to the treasure chest under the tree, but it turns out there was an illusion!",
            "We were actually walking towards the front door of the house, where we are attacked by 2 gargoyles",
            "One of the gargoyles did a crit on Scab, he took 17 damage in on turn, more than half his health",
            "Caw tries to fly away, passes an Int save (taking 5 damage in the processes) and sees the illusion field that encompasses the field in front of the house",
            "It seems like we could maybe still walk to the tree without going into the house, but it's risky and scary",
            "So we go inside the house",
            "We see a scroll sitting on the floor in the front foyer, we read it",
            "The scroll is from the captain of the pirate ship where we found the treasure map",
            "He's hidden his treasure in this house, we can only get it by banishing the ghost that haunts the house",
            "The scroll describes a banishing ritual to get rid of the ghost, we need 3 objects to do it + the book detailing the ritual",
            "There are 4 rooms we can go in, all their doors are open, we start with the library to try and find the book",
            "When we go in the ghost slams the door shut, we're trapped",
            "3 book shelves per stack, 3 stacks in the room, one ladder per stack",
            "Caw flies up, the books start to attack us",
            "We keep finding books about different random information and creature types. We're probably looking for something about ghosts",
            "Caw finds the book with the banishing ritual after we checked 4/5 bookcases in total",
            "The book doesn't describe the specific object we need, just that they are 'powerful'",
            "We go to the bedroom, it belonged to a little girl and is filled with creepy dolls",
            "The dolls come alive, the rug tries to smother us, we search the room and find a girl's diary in the nightstand. We take it and run",
            "We take a short rest, then go into the trophy room",
            "We solve the puzzle, turns out there a items on each of the 4 walls, one of which is 'wrong' in some way",
            "For example, an atlas of the world with 3 blue moons all the same size, when really the moons are different colors and sizes (Occam!)",
            "We poke each of the 4 wrong things, they are an illusion and have a switch under them",
            "We press all 4, and a lock of hair tied in a pink bow is revealed, we take us",
            "We go to the kitchen, attacked by animated kitchen implements, defeat them all, found a burned painting in the oven",
            "Picture was of a mother and daughter",
            "We go into the foyer, get teleported to the attic, start fighting the ghost",
            "We fight some things, do the ritual 3 times, and on the 3rd time we banish the ghost",
            "We go back to the foyer and there is a ornate treasure chest sitting there",
            "The session has gone long, we'll look at the contents of the chest next session! :(",
        ],

        "Jan 22, 2023":
        [
            "We debate about who is going to open the chest, it was Scab of course",
            "We find a bunch of cool stuff, including a magic folding boat!",
            "The boat has command works: BOAT SHIP FOLD - Written in Draconic",
            "The treasure chest is filled with a bunch copper and silver, too much to carry normally",
            "Caw flies up to scout the land, we find quite close by a river running south",
            "We lug the treasure chest to the river, put it in the magic boat, and go down stream. Adventure!",
            "As we're traveling downstream, we hear and feel a big volcanic eruption, from a place that shouldn't have a volcano",
            "Caw flies up to check it out, it's about an hour south of us",
            "Scab is spooked, doesn't want to investigate. But Caw and Piper want to go check it out, and he can't get back the treasure chest back without them",
            "We beach the boat, fold it up, leave the treasure chest after taking all the money we can carry (90 out of the 180gp). We try and hide the chest, at least",
            "As we get to the source of the explosion, we find the remains of a hill with magma leaking out, running down the slope",
            "We hear someone blowing a hunting horn in desperation, it seems like they are trapped or in danger",
            "We save an elf and his 4 hunting dogs who were trapped on a hill surrounded by lava and fire snakes",
            "We then go to investigate the hill where the eruption happened, Caw is flying around and gets hit by a firebolt, clearly there's something in the hill",
            "We hide in the trees, two dwarves in armor come out looking for us",
            "Before we attach them, they mention they need to get back before Yangen (?) knows they've left",
            "We kill the dwarves, follow their tracks back, Scab investigates to open the dwarven door in the rock face, we go inside",
            "We come into a large chamber with a lava making machine in it, we just start blasting",
            "We take out the normal dwarves and the grenadier, turn off the lava pump",
            "We pulled the grenadier into the lava multiple times, all his grenades exploded, it was great",
            "Follow the next tunnel to a much bigger room, with a much bigger pump",
            "There we see Yangen, who is an even fancier grenadier with a special hand crossbow",
            "As a legendary action, Yangen avoided being pulled into the lava, it was sad",
            "Yangen's lair action lets him explode walls and bridges over the lava, it's pretty spooky",
            "Yangen eventually explodes the roof, making everyone roll for damage. He's so crazy",
            "We shove Yangen into the lava a few times, which is always funny",
            "Yangen ends up dying in the lava. He's so crazy",
            "There is one fire mage left alive, named Garvin",
            "We learn about the Sky Bridge, a place where a bunch of Dwarves live",
            "He says that if we keep him alive he'll get Yangen's magic items from the lava pit",
            "Piper also offers him a job at the pie stall, making lava cake (get it?)",
            "We leave, get our treasure chest back, and take the river back to New Emsworth",
            "We pass by a large stone tower, nothing else around it, kinda weird. Wonder what's up with that",
        ],

        "Feb 26, 2023":
        [
            "We're back in the Plot and Hook. There are a bunch of new adventurers in the bar who are looking at us with admiration",
            "W are great seasoned adventurers",
            "We wait a couple of days for Garvin to get back. We have some downtime",
            "Scab tries to find more about the temple of the dead elves, where the bandits are holed up. Rolled low-ish, didn't learn much",
            "Piper tries to find out if the bandits have been active recently. Learned a ranger was recently attacked by magic near the elven ruins. So people are still there",
            "Garvin comes back, causes a bit of a stir at the front gate due to his bucket of literal lava (for his lava cakes)",
            "We convince him to leave the bucket outside, we made a mini volcano out of some dirt and put the bucket on top",
            "He has Yangen's magic items, some magic rocket boots and a magic +1 hand crossbow",
            "We teach Garvin about capitalism, he gets it",
            "Off we go to the elven ruins!",
            "We get there and cast Past without Trace, for ultimate sneaking",
            "These ruins are pretty small, 3 buildings and 1 spire/watchtower",
            "We infiltrate the ruins, and come across 2 necromancers on skeleton horses, with 3 regular zombies and 1 ogre zombie",
            "They are breaking down a door to a building, not sure what's up with that",
            "Being the murder hobos we are, we decide to attack. No sense in talking to clearly evil necromancers",
            "We surprise them and dispatch them without too much trouble. The necromancers can resurrect each other, which is a neat (and annoying!) ability",
            "We talk to the people behind the door the zombies were trying to break down, turns out they were bandits!",
            "Each of them is missing a hand, which has been replaced by a black ichor that they can use to cast spells. They also have black handprints on their face",
            "We take some damage but defeat them, and examine the building they were hiding in",
            "There were stairs going up to a tower, where there was 3 more people",
            "Scab shoved one off the tower twice, it was great. Scab took a bunch of damage during the combat though, which was less great",
            "We take a short rest up in the tower, it's pretty defenseable",
        ],

        "Mar 5, 2023":
        [
            "Short rest was successful",
            "Caw does some scouting, all the dead necromancers, zombies, and cultist bodies are gone! Damn necromancers",
            "We go downstairs, it's dark, cold, wet, dank, like being inside of a well",
            "We reach the bottom, it's a long hallway with a bunch of alcoves",
            "In each alcove we find the magically preserved corpse(?) of an elf",
            "These are pretty clearly the Frost Elves, who are supposed to be gone from the world",
            "They preserve their dead by encasing them in magical ice, so the bodies are most likely actually dead",
            "Scab poked one of the bodies with his fire-y dwarven warpick, it reacted violently and I took 5 cold damage",
            "There are two doors at the end of the hall, a fancy one and a less fancy one. We go into the less fancy one",
            "It's full of elven statues, with a couple of fountains in it. One of the fountains has a big crack/hole in it, large enough for a small creature to squeeze through",
            "There are two necromancers and some zombies/skeletons in the room, we fight them. Spent some resources but took them down fairly easily",
            "Scab squeezes through the crack, eventually runs into a large rat, it looks like this is its burrow",
            "The rat bites Scab a bunch as he escapes, I took 4 damage! Once I escaped the burrow I was able to kill it with a dagger, no one bites Scab 4 times and gets away with it",
            "We go to the fancy door, Piper hears an argument on the other side",
            "We decide to barge on through and attack the people there, because that's all we're good for",
            "There are a bunch of cultists, along with a fancy boss cultist with a frost sword like Gretchen's",
            "We took down the boss really early, eventually defeated everyone else",
            "Their ability to cash Hellish Rebuke once per battle really hurt, since it's kind of unavoidable",
            "We leave one alive, interrogate her, learn there are more cultists next door.",
            "The boss we killed wasn't M",
            "As we're talking to the cultist, her black ichor hand detaches and latches onto her face and kills her",
            "We make really crappy disguises and walk into the next room",
            "The disguise doesn't need to last long, just a couple of seconds for us to position ourselves",
            "We come into a room where a bunch of people are worshiping a wall on fungal growth with a bunch of reaching arms coming out of it",
            "The source of the fungus is a well in the room, it's growing up and out of the well",
            "The person in charge is a half elf woman, both her hands and her tongue are black ichor",
            "She monologues at us, says Gretchen was like a mother to her, and then we fight!",
            "A number of centipede like arms erupt out of M's back, each with a human hand at the end",
            "Scab attacked M, she did a 3d10 Hellish Rebuke and downed him in one hit",
            "Caw flew over and poured a healing potion down my throat, getting me up",
            "Caw then used his Circlet of Blasting, hit all of them with one a crit, for 8d6 damage, killing M",
            "We can't do much about the Kotus wall at the moment, so we leave and go back to town",
            "We find a journal in M's pocket, her name is Emma",
            "She's an orphan, came to New Emsworth for a fresh start, turned to crime when work dried up, lost both of her hands for stealing and her tongue for lying",
            "We came to the elven ruins, found Kotus, made a pact, and wanted revenge on everyone in New Emsworth",
            "The last entry in her journal mentions that they've been under assault from necromancers, they want the corpses",
            "We vaguely know where they are coming from and record it on the map",
        ],
    },

    ////////////////////////
    // These are the things you own, organized by where they are stored.
    // You can add new sections as desired.
    ////////////////////////
    Inventory:
    {
        "Coin Purse":
        [
            { Name: "CP", Amount:   9, Weight: 0.02, Cost: 0.01, Description: "" },
            { Name: "SP", Amount:  20, Weight: 0.02, Cost: 0.1 , Description: "" },
            { Name: "GP", Amount: 326, Weight: 0.02, Cost: 1   , Description: "" },
        ],

        "Attuned Items":
        [
            { Name: "*The* Vermilion Cloak"        , Amount:    1, Weight: 0    , Cost:   0    , Description: "" },
            { Name: "War Pick of Ember Dwarves"    , Amount:    1, Weight: 0    , Cost:   0    , Description: "" },
            { Name: "Yangen's Rocket Boots"        , Amount:    1, Weight: 0    , Cost:   0    , Description: "" },
        ],

        "On Person":
        [
            { Name: "*The* Vermilion Cloak"        , Amount:    1, Weight: 0    , Cost:   0    , Description: "Freely given by Devon after saving his life. Cloak of elven kind, good at stealth" },
            { Name: "Bane Helm"                    , Amount:    1, Weight: 0    , Cost:   0    , Description: "Freely given by Ada after saving her life. Cast Bane 1 time per day (Save DC)" },
            { Name: "Clothes, traveler's"          , Amount:    1, Weight: 4    , Cost:   2    , Description: "" },
            { Name: "Component pouch"              , Amount:    1, Weight: 2    , Cost:  25    , Description: "" },
            { Name: "Dagger"                       , Amount:    1, Weight: 1    , Cost:   2    , Description: "" },
            { Name: "Disguise kit"                 , Amount:    1, Weight: 3    , Cost:  25    , Description: "" },
          //{ Name: "Pearl of Power"               , Amount:    1, Weight: 0    , Cost:   0    , Description: "Use an Action to regain one expended spell slot (up to 3rd level). Comes back on long rest" },
          //{ Name: "Shortsword of the Frost Elves", Amount:    1, Weight: 0    , Cost:   0    , Description: "Taken from Gretchen's corpse. +1 sword. On hit, once per long rest, DC 13 Str to avoid being restrained" },
            { Name: "Thieves' tools"               , Amount:    1, Weight: 1    , Cost:  25    , Description: "" },
            { Name: "Yangen's Rocket Boots"        , Amount:    1, Weight: 1    , Cost:   0    , Description: "3 times per long" },
            { Name: "War Pick"                     , Amount:    1, Weight: 2    , Cost:  25    , Description: "" },
            { Name: "War Pick of Ember Dwarves"    , Amount:    1, Weight: 0    , Cost:  0     , Description: "Looted from Dura's body. +1 warpick, does fire damage, can cast burning hands from it once per day, dc 15" },
        ],

        "In Backpack":
        [
            { Name: "Alchemist Jug"                , Amount:  1, Weight:  1  , Cost:  0   , Description: "Taken from a treasure chest in a goblin encampment" },
            { Name: "Backpack"                     , Amount:  1, Weight:  5  , Cost:  2   , Description: "" },
            { Name: "Bedroll"                      , Amount:  1, Weight:  7  , Cost:  1   , Description: "" },
            { Name: "Healing Potion (Greater)"     , Amount:  1, Weight:  0  , Cost:  0   , Description: "4d4 + 4" },
            { Name: "Healing Potion (Lesser)"      , Amount:  0, Weight:  0  , Cost: 50   , Description: "2d4 + 2" },
            { Name: "Hempen rope"                  , Amount: 50, Weight:  0.2, Cost:  0.02, Description: "" },
            { Name: "Mess kit"                     , Amount:  1, Weight:  1  , Cost:  0.2 , Description: "" },
            { Name: "Rations"                      , Amount: 10, Weight:  2  , Cost:  0.5 , Description: "" },
            { Name: "Rusty Key"                    , Amount:  1, Weight:  0  , Cost:  0   , Description: "Taken from goblin corpse, opens entrance to Temple of Gailian" },
            { Name: "Rusty Key"                    , Amount:  1, Weight:  0  , Cost:  0   , Description: "Taken from the Temple of Gailian, dropped by Paladin Thornseeker" },
            { Name: "Spellbook"                    , Amount:  1, Weight:  3  , Cost: 50   , Description: "Very fancy looking, bloodstained, taken from Clan Fester shaman I killed" },
            { Name: "Tent"                         , Amount:  1, Weight: 20  , Cost:  2   , Description: "" },
            { Name: "Tinderbox"                    , Amount:  1, Weight:  1  , Cost:  0.5 , Description: "" },
            { Name: "Torches"                      , Amount: 10, Weight:  1  , Cost:  0.01, Description: "" },
            { Name: "Treasure Map"                 , Amount:  1, Weight:  0  , Cost:  0   , Description: "Taken from the Pirate captain's chest" },
            { Name: "WandOfMinorPolymorph (Cursed)", Amount:  1, Weight:  0  , Cost:  0   , Description: "Taken off of a goblin shaman. Real gross. 7 charges, spend a charge to cast Polymorph with max CR of 1. Regains 1d6 + 1 charges after a long rest" },
            { Name: "Waterskin"                    , Amount:  1, Weight:  5  , Cost:  0.2 , Description: "" },
            { Name: "Black ring with white skull"  , Amount:  1, Weight:  0  , Cost:  0   , Description: "Freely given by Blake Lively after saving his life. Cast Darkness 3 times per day" },
            { Name: "Fine clothes"                 , Amount:  2, Weight:  0  , Cost:  0   , Description: "Found in the pirate treasure chest from the haunted mansion" },
            { Name: "Cloak of the Manta Ray"       , Amount:  1, Weight:  0  , Cost:  0   , Description: "Found in the pirate treasure chest from the haunted mansion" },
            { Name: "Small ornate magic box"       , Amount:  1, Weight:  0  , Cost:  0   , Description: "Found in the pirate treasure chest from the haunted mansion" },
            { Name: "Feather token - anchor"       , Amount:  1, Weight:  0  , Cost:  0   , Description: "Found in the pirate treasure chest from the haunted mansion" },
            { Name: "Feather token - fan"          , Amount:  1, Weight:  0  , Cost:  0   , Description: "Found in the pirate treasure chest from the haunted mansion" },
            { Name: "Evil spell book"              , Amount:  2, Weight:  0  , Cost:  0   , Description: "Taken from necromancer killed in the elven ruins" },
            { Name: "Ring of Cold Resistance"      , Amount:  0, Weight:  0  , Cost:  0   , Description: "Taken from magic chest in Elven Ruins" },
        ],
    },
};

//
// This is data that is generally changed more that once each level.
//
const DataThatChangesLessOften =
{
    ////////////////////////
    // This is your AC.
    ////////////////////////
    AC_Formula: "13 + DexMod", // Mage armor.

    ////////////////////////
    // These are reminders about special circumstances where your skills may be more or less effective.
    ////////////////////////
    SkillExceptionReminders:
    {
        Acrobatics : "Advantage while Bladesinging (Bladesong, Bladesinger 2)",
    },

    ////////////////////////
    // These are reminders about special circumstances where your saving throws may be more or less effective.
    ////////////////////////
    SavingThrowExceptionReminders:
    {
        Constitution : "Bonus on Concentration checks while Bladesinging (Bladesong, Bladesinger 2)",
    },

    ////////////////////////
    // These are attacks that are done against your opponent's AC.
    ////////////////////////
    Attacks:
    [
        { Name: "Dagger"                       , HitBonus_Formula: "DexMod + ProfBonus"    , DamageDice: "1d4" , DamageBonus_Formula: "DexMod"    , Range: "Melee" , DamageType: "Piercing" },
        { Name: "Fire Bolt"                    , HitBonus_Formula: "IntMod + ProfBonus"    , DamageDice: "1d10", DamageBonus_Formula: "0"         , Range: "120ft" , DamageType: "Fire" },
      //{ Name: "Frost Sword"                  , HitBonus_Formula: "DexMod + ProfBonus + 1", DamageDice: "1d6" , DamageBonus_Formula: "DexMod + 1", Range: "Melee" , DamageType: "Cold" },
        { Name: "Shadow Blade"                 , HitBonus_Formula: "DexMod + ProfBonus"    , DamageDice: "2d8" , DamageBonus_Formula: "DexMod"    , Range: "Melee" , DamageType: "Psychic" },
        { Name: "War pick"                     , HitBonus_Formula: "DexMod + ProfBonus"    , DamageDice: "1d8" , DamageBonus_Formula: "DexMod"    , Range: "Melee" , DamageType: "Piercing" },
        { Name: "War Pick of the Ember Dwarves", HitBonus_Formula: "DexMod + ProfBonus + 1", DamageDice: "1d8" , DamageBonus_Formula: "DexMod + 1", Range: "Melee" , DamageType: "Fire" },
    ],

    ////////////////////////
    // These are attacks that force your opponent to make a saving throw.
    ////////////////////////
    SavingThrowAttacks:
    [
        { Name: "Telekinetic Shove"      , DC_Formula: "8 + ProfBonus + IntMod", Stat: "Str", DamageDice: "0"   , DamageBonus_Formula: "0", Range: "30ft", DamageType: "N/A"     , OnSuccess: "No Movement" },
        { Name: "Thunderwave"            , DC_Formula: "8 + ProfBonus + IntMod", Stat: "Con", DamageDice: "2d8" , DamageBonus_Formula: "0", Range: "15ft", DamageType: "Thunder" , OnSuccess: "Half Damage, No Push" },
        { Name: "Poison Spray"           , DC_Formula: "8 + ProfBonus + IntMod", Stat: "Con", DamageDice: "1d12", DamageBonus_Formula: "0", Range: "10ft", DamageType: "Poison"  , OnSuccess: "No Damage" },
        { Name: "Tasha's Mind Whip"      , DC_Formula: "8 + ProfBonus + IntMod", Stat: "Int", DamageDice: "3d6" , DamageBonus_Formula: "0", Range: "90ft", DamageType: "Psychic" , OnSuccess: "Half Damage" },
        { Name: "Dragon's Breath"        , DC_Formula: "8 + ProfBonus + IntMod", Stat: "Dex", DamageDice: "3d6" , DamageBonus_Formula: "0", Range: "15ft", DamageType: "Many"    , OnSuccess: "Half Damage" },
    ],

    ////////////////////////
    // These are the spells that you know or can prepare, organized by the spell's level.
    ////////////////////////
    Spells:
    [
        // Cantrip.
        [
            { Name: "Booming Blade", IsPrepared: true, CastingTime: "Action", Range: "Self" , Damage: "1d8" , Type: "Attack"  , Components: "S M", Duration: "1 round", IsConcentration: false, IsRitual: false }, // Wizard 1.
            { Name: "Fire Bolt"    , IsPrepared: true, CastingTime: "Action", Range: "120ft", Damage: "1d10", Type: "Attack"  , Components: "V S", Duration: "Instant", IsConcentration: false, IsRitual: false }, // Wizard 1.
            { Name: "Mage Hand"    , IsPrepared: true, CastingTime: "Action", Range: "60ft" , Damage: "N/A" , Type: "N/A"     , Components: "N/A", Duration: "1m"     , IsConcentration: false, IsRitual: false }, // Wizard 1 + Goblin (Telekinetic Feat).
            { Name: "Poison Spray" , IsPrepared: true, CastingTime: "Action", Range: "10ft" , Damage: "1d12", Type: "Con Save", Components: "V S", Duration: "Instant", IsConcentration: false, IsRitual: false },

            /*
            { Name: "Acid Splash"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Blade Ward"       , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Booming Blade"    , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Chill Touch"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Control Flames"   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Create Bonfire"   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Dancing Lights"   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Fire Bolt"        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Friends"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Frostbite"        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Green-Flame Blade", IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Gust"             , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Infestation"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Light"            , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Lightning Lure"   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mage Hand"        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mending"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Message"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mind Sliver"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Minor Illusion"   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mold Earth"       , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Poison Spray"     , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Prestidigitation" , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Ray of Frost"     , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Shape Water"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Shocking Grasp"   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Sword Burst"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Thunderclap"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Toll the Dead"    , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "True Strike"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            */
        ],

        // Level 1.
        [
            { Name: "Absorb Elements"              , IsPrepared: true , CastingTime: "Reaction", Range: "Self" , Damage: "1d6", Type: ""        , Components: "S"    , Duration: "1 round", IsConcentration: false, IsRitual: false }, // Wizard 1.
            { Name: "Comprehend Languages"         , IsPrepared: false, CastingTime: "Action"  , Range: "Self" , Damage: ""   , Type: ""        , Components: "V S M", Duration: "1h"     , IsConcentration: false, IsRitual: true  }, // Wizard 1.
            { Name: "Detect Magic"                 , IsPrepared: false, CastingTime: "Action"  , Range: "Self" , Damage: ""   , Type: ""        , Components: "V S"  , Duration: "10m"    , IsConcentration: true , IsRitual: true  }, // Wizard 1.
            { Name: "Mage Armor"                   , IsPrepared: true , CastingTime: "Action"  , Range: "Touch", Damage: ""   , Type: ""        , Components: "V S M", Duration: "8h"     , IsConcentration: false, IsRitual: false }, // Wizard 1.
            { Name: "Protection from Evil and Good", IsPrepared: false, CastingTime: "Action"  , Range: "Touch", Damage: ""   , Type: ""        , Components: "V S M", Duration: "10m"    , IsConcentration: true , IsRitual: false }, // Wizard 2.
            { Name: "Shield"                       , IsPrepared: true , CastingTime: "Reaction", Range: "Self" , Damage: ""   , Type: ""        , Components: "V S"  , Duration: "1 round", IsConcentration: false, IsRitual: false }, // Wizard 1.
            { Name: "Tasha's Hideous Laughter"     , IsPrepared: false, CastingTime: "Action"  , Range: "30ft" , Damage: ""   , Type: "Wis Save", Components: "V S M", Duration: "1m"     , IsConcentration: true , IsRitual: false }, // Wizard 2.
            { Name: "Thunderwave"                  , IsPrepared: true , CastingTime: "Action"  , Range: "15ft" , Damage: "2d8", Type: "Con Save", Components: "V S"  , Duration: "Instant", IsConcentration: false, IsRitual: false }, // Wizard 1.

            /*
            { Name: "Absorb Elements"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Alarm"                        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Burning Hands"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Catapult"                     , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Cause Fear"                   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Charm Person"                 , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Chromatic Orb"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Color Spray"                  , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Comprehend Languages"         , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Detect Magic"                 , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Disguise Self"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Earth Tremor"                 , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Expeditious Retreat"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "False Life"                   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Feather Fall"                 , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Find Familiar"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Fog Cloud"                    , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Grease"                       , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Ice Knife"                    , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Identify"                     , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Illusory Script"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Jump"                         , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Longstrider"                  , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mage Armor"                   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Magic Missile"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Protection from Evil and Good", IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Ray of Sickness"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Shield"                       , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Silent Image"                 , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Sleep"                        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Snare"                        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Tasha's Caustic Brew"         , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Tasha's Hideous Laughter"     , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Tenser's Floating Disk"       , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Thunderwave"                  , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Unseen Servant"               , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Witch Bolt"                   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            */
        ],

        // Level 2.
        [
            { Name: "Darkvision"       , IsPrepared: false, CastingTime: "Action"      , Range: "Touch", Damage: ""   , Type: ""       , Components: "V S M", Duration: "8h"     , IsConcentration: false, IsRitual: false }, // Wizard 3.
            { Name: "Shadow Blade"     , IsPrepared: true , CastingTime: "Bonus Action", Range: "Self" , Damage: "2d8", Type: "Psychic", Components: "V S"  , Duration: "1m"     , IsConcentration: true , IsRitual: false }, // Wizard 3.
            { Name: "Dragon's Breath"  , IsPrepared: true , CastingTime: "Bonus Action", Range: "Touch", Damage: "3d6", Type: "Many"   , Components: "V S M", Duration: "1m"     , IsConcentration: true , IsRitual: false }, // Wizard 4.
            { Name: "Tasha's Mind Whip", IsPrepared: true , CastingTime: "Action"      , Range: "90ft" , Damage: "3d6", Type: "Psychic", Components: "V"    , Duration: "1 round", IsConcentration: false, IsRitual: false }, // Wizard 4.

            /*
            { Name: "Aganazzar's Scorcher"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Alter Self"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Arcane Lock"               , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Blindness/Deafness"        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Blur"                      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Cloud of Daggers"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Continual Flame"           , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Crown of Madness"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Darkness"                  , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Darkvision"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Detect Thoughts"           , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Dragon's Breath"           , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Dust Devil"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Earthbind"                 , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Enlarge/Reduce"            , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Flaming Sphere"            , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Gentle Repose"             , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Gust of Wind"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Hold Person"               , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Invisibility"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Knock"                     , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Levitate"                  , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Locate Object"             , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Magic Mouth"               , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Magic Weapon"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Maximilian's Earthen Grasp", IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Melf's Acid Arrow"         , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mind Spike"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Mirror Image"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Misty Step"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Nystul's Magic Aura"       , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Phantasmal Force"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Pyrotechnics"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Ray of Enfeeblement"       , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Rope Trick"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Scorching Ray"             , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "See Invisibility"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Shadow Blade"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Shatter"                   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Skywrite"                  , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Snilloc's Snowball Swarm"  , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Spider Climb"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Suggestion"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Tasha's Mind Whip"         , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Warding Wind"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Web"                       , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            */
        ],

        // Level 3.
        [
            // Cool spells
            // Haste
            // Fireball
            // Blink
            // Spirit Shroud
            // Summon Lesser Demons
            // Summon Shadowspawn
            // Summon Undead

            /*
            { Name: "Animate Dead"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Bestow Curse"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Blink"                 , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Catnap"                , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Clairvoyance"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Counterspell"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Dispel Magic"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Enemies Abound"        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Erupting Earth"        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Fear"                  , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Feign Death"           , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Fireball"              , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Flame Arrows"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Fly"                   , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Gaseous Form"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Glyph of Warding"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Haste"                 , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Hypnotic Pattern"      , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Intellect Fortress"    , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Leomund's Tiny Hut"    , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Life Transference"     , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Lightning Bolt"        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Magic Circle"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Major Image"           , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Melf's Minute Meteors" , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Nondetection"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Phantom Steed"         , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Protection from Energy", IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Remove Curse"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Sending"               , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Sleet Storm"           , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Slow"                  , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Spirit Shroud"         , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Stinking Cloud"        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Fey"            , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Lesser Demons"  , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Shadowspawn"    , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Summon Undead"         , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Thunder Step"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Tidal Wave"            , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Tiny Servant"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Tongues"               , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Vampiric Touch"        , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Wall of Sand"          , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Wall of Water"         , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            { Name: "Water Breathing"       , IsPrepared: true , CastingTime: "", Range: "", Damage: "", Type: "", Components: "", Duration: "", IsConcentration: false, IsRitual: false },
            */
        ],

        // Level 4.
        [],

        // Level 5.
        [],

        // Level 6.
        [],

        // Level 7.
        [],

        // Level 8.
        [],

        // Level 9.
        [],
    ],
};

//
// This is data that is generally changed only when a new level is gained.
//
const DataThatChangesOnLevelUp =
{
    ////////////////////////
    // This records what class you took at each level up.
    ////////////////////////
    Levels:
    [
        "Wizard", // Level 1
        "Wizard", // Level 2
        "Wizard", // Level 3
        "Wizard", // Level 4
    ],

    ////////////////////////
    // This allows you to override the default max HP formula with your own.
    // Use this if you are rolling for health, otherwise leave it undefined.
    ////////////////////////
    OverrideMaxHP_Formula: undefined,

    ////////////////////////
    // These are the things that you can do when taking a short or long rest.
    ////////////////////////
    RestOptions:
    {
        "On Short Rest":
        [
        ],

        "On Long Rest":
        [
            "Change Prepared Spells",    // Wizard 1.
            "Change one wizard cantrip", // Wizard 3 (Cantrip Formulas).
        ],
    },

    ////////////////////////
    // These are things that you can do during your turn in combat.
    ////////////////////////
    CombatOptions:
    {
        "Action":
        [
            "Cast a spell", // Wizard 1.
        ],

        "Bonus Action":
        [
            "Disengage",         // Goblin (Nimble Escape).
            "Enter a Bladesong", // Bladesinger 2 (Bladesong).
            "Hide",              // Goblin (Nimble Escape).
            "Telekinetic Shove", // Goblin (Telekinetic Feat).
        ],

        "Reaction":
        [
        ],

        "On Turn":
        [
            "Fury of the Small", // Goblin.
        ],

        "Any Time":
        [
            "Dismiss a Bladesong", // Bladesinger 2 (Bladesong).
        ],

        "Movement":
        [
        ],
    },

    ////////////////////////
    // These are all the things that your race, class, and background allow you to do.
    ////////////////////////
    Features:
    {
        Goblin:
        [
            { Name: "Fury of the Small"         , Desc: [ "Use when damaging a creature with an attack or spell and the creature is at least 1 size larger than me"
                                                        , "Deal an extra {Level} damage to the creature"
                                                        , "Comes back on a short or long rest" ] },

            { Name: "Nimble Escape"             , Desc: [ "Disengage or Hide as a bonus action" ] },

            { Name: "Feat: Telekinetic"         , Desc: [ "Learn mage hand cantrip"
                                                        , "Can cast it without verbal or somatic components"
                                                        , "Can make the spectral hand invisible"
                                                        , "If I already knew the cantrip (I did) then it's range is increased by 30ft"
                                                        , "As a bonus action, can telekinetically shove a creature within 30ft"
                                                        , "DC {8 + ProfBonus + IntMod} Strength saving throw"
                                                        , "On failure, pushed 5ft towards or away from me" ] },
        ],

        Urchin:
        [
            { Name: "City Secrets"              , Desc: [ "When not in combat, the party can travel between two locations in a city twice as fast as your speed would normally allow" ] },
        ],

        Wizard:
        [
            // Level 1.
            { Name: "Spellcasting"              , Desc: [ "Uses Int"
                                                        , "Can cast rituals"
                                                        , "Has a spell book with my spell list in it"
                                                        , "Can prepare spells out of my spell book at the end of a long rest"
                                                        , "Can copy new spells into the book, takes 50gp and 2 hours per spell level copied"
                                                        , "Can make a backup spell book, takes 10gp and 1 hour per speed level copied" ] },

            { Name: "Arcane Recovery"           , Desc: [ "Use after finishing a short rest"
                                                        , "Can recover {Math.ceil(WizardLevel / 2)} units of spell slots" ] },
            // Level 2.
            { Name: "Training In War And Song"  , Desc: [ "Gain proficiency with light armor"
                                                        , "Gain proficiency with one type of one-handed melee weapon"
                                                        , "Gain proficiency in Performance skill" ] },

            { Name: "Bladesong"                 , Desc: [ "Can't do it when wearing medium armor, heavy armor, or a shield"
                                                        , "Triggered with a Bonus Action"
                                                        , "Lasts for 1 minute"
                                                        , "Ends early if incapacitated, wear any of the disallowed equipment, or use two hands to make an attack with a weapon"
                                                        , "Can be dismissed at any time (no action required)"
                                                        , "While active, gain the following benefits:"
                                                        , "1) AC bonus of {IntMod}"
                                                        , "2) Walking speed increases by 10ft"
                                                        , "3) Advantage on Acrobatics checks"
                                                        , "4) Concentration saving throws get a bonus of {IntMod}"
                                                        , "Can be used {ProfBonus} number of times"
                                                        , "Comes back on a long rest" ] },
            // Level 3.
            // Better spell casting (Level 2 spells).
            { Name: "Cantrip Formulas"          , Desc: [ "When finishing a long rest, can replace a cantrip I know with another cantrip from the wizard spell list" ] },

            /*
            // Level 4.
            // ASI or Feat.

            // Level 5.
            // Better spell casting (Level 3 spells).

            // Level 6.
            { Name: "Extra Attack"              , Desc: [ "Can attack 2 times when taking the Attack action"
                                                        , "Can cast a cantrip in place of one of those attacks" ] },
            // Level 7.
            // Better spell casting (Level 4 spells).

            // Level 8.
            // ASI or Feat.
            */
        ],
    },
};

//
// This is data that generally only set when the character is first created.
//
const DataThatIsSetOnCharacterCreation =
{
    ///////////////////
    // This is your name.
    ///////////////////
    Name: "Scab",

    ///////////////////
    // This is the small image that is displayed next to your name.
    // It looks best if the image is square.
    ///////////////////
    HeadshotPicture: "Scab Headshot.png",

    ///////////////////
    // This is your initiative.
    ///////////////////
    Initiative_Formula: "DexMod",

    ///////////////////
    // This is your attack bonus and save DC.
    //
    // It makes sense for both magical and physical things. For example: weapon attacks,
    // spell attacks, Battle Master Fighter maneuvers, Monk Stunning Strike, etc...
    //
    // Set the formula to "undefined" if it doesn't make sense for your character (e.g. a barbarian without any save DCs).
    //
    // If you have more than one formula for your Attack Bonus or Save DC (e.g. you can
    // use different stats to cast spells) that is not well supported at the moment.
    ///////////////////
    AttackBonus_Formula: undefined,
    SaveDC_Formula     : undefined,

    ///////////////////
    // This is how many spells you can prepare.
    //
    // Set it to "undefined" if you can't prepare any spells.
    ///////////////////
    NumSpellsPrepared_Formula: "Math.max(1, IntMod + WizardLevel)",

    ///////////////////
    // This is how much extra HP you recover when you level up.
    ///////////////////
    ExtraHealthGainOnLevel_Formula: "ConMod",

    ///////////////////
    // This is how much extra HP you recover when you roll a hit die.
    ///////////////////
    HitDiceRollModifier_Formula: "ConMod",

    ///////////////////
    // If you're using the sidekick rules from Tasha's, this is how you specify the
    // starting health + hit dice that your creature's Monster Manual statblock has.
    //
    // These only apply if you have sidekick class levels (i.e. Expert, Warrior, or Spellcaster).
    //
    // Set all to "undefined" if this character isn't a sidekick.
    ///////////////////
    TashaSidekickHitDieSize         : undefined,
    TashaSidekickStartingNumHitDice : undefined,
    TashaSidekickStartingHealth     : undefined,

    ///////////////////
    // These are the things that you are proficient with.
    ///////////////////
    Proficiencies:
    {
        Weapons:
        [
            "Daggers",          // Wizard 1.
            "Darts",            // Wizard 1.
            "Slings",           // Wizard 1.
            "Quarterstaffs",    // Wizard 1.
            "Light Crossbows",  // Wizard 1.
            "War Pick",         // Bladesinger 2 (Training In War And Song).
            "Short Sword",      // Jake Houserule - Bladesinger 2 (Training In War And Song).
            "Whip",             // Jake Houserule - Bladesinger 2 (Training In War And Song).
            "Rapier",           // Jake Houserule - Bladesinger 2 (Training In War And Song).
        ],
        Armor:
        [
            "Light Armor", // Bladesinger 2 (Training In War And Song).
        ],
        Tools:
        [
            "Disguise Kit",   // Urchin.
            "Thieves' Tools", // Urchin.
        ],
        Languages:
        [
            "Common", // Goblin.
            "Goblin", // Goblin.
        ]
    },

    ///////////////////
    // These are your physical attributes.
    ///////////////////
    PhysicalAttributes:
    {
        MoveSpeed              : 30,              // Goblin.
        FlyingSpeed            : 0,               // Goblin.
        SwimSpeed_Formula      : "MoveSpeed / 2", // Goblin.
        ClimbSpeed_Formula     : "MoveSpeed / 2", // Goblin.
        StandFromProne_Formula : "MoveSpeed / 2",
        Height                 : 3.5,             // Goblin.
        Weight                 : 60,              // Goblin.
        Size                   : "Small",         // Goblin.
        CarryingCapacitySize   : "Small",         // Goblin.
        Age                    : 10,
        DarkVisionDistance     : 60,              // Goblin.
    },

    ///////////////////
    // These are your stats.
    ///////////////////
    Stats:
    {
        Strength     : { Value_Formula: "11"         }, // Rolled stats.
        Dexterity    : { Value_Formula: "18 + 2"     }, // Rolled stats + ASI.
        Constitution : { Value_Formula: "15 + 1"     }, // Rolled stats + Goblin.
        Intelligence : { Value_Formula: "13 + 2 + 1" }, // Rolled stats + Goblin + Telekinetic Feat.
        Wisdom       : { Value_Formula: "12"         }, // Rolled stats.
        Charisma     : { Value_Formula: "10"         }, // Rolled stats.
    },

    ///////////////////
    // These are the saving throws that you are proficient in.
    ///////////////////
    ProficientSavingThrows:
    [
        "Intelligence", // Wizard 1.
        "Wisdom",       // Wizard 1.
    ],

    ///////////////////
    // These are the skills that you are proficient in. Set the multiplier to 2 if you have Expertise.
    ///////////////////
    ProficientSkillMultipliers:
    {
        Insight       : 1, // Wizard 1.
        Investigation : 1, // Wizard 1.
        Performance   : 1, // Bladesinger 2 (Training In War And Song).
        SleightOfHand : 1, // Fighter 1.
        Stealth       : 1, // Urchin.
    },

    ///////////////////
    // This is pretty much only for supporting the Bard's "Jack of all Trades" feature by setting it to 0.5.
    ///////////////////
    DefaultSkillProficiencyMultiplier: 0,

    ///////////////////
    // These are links to external websites that might have useful info.
    ///////////////////
    ExternalLinks:
    {
        "Cold Rush"    : "https://docs.google.com/document/d/1Yw6e5uC7ZYdNBQF3LrRV9LOaHbJmCRdwMlEToEzyFZM/edit?usp=sharing",
        "Jake's Feats" : "https://docs.google.com/document/d/15QYw_1cUztvFhacsFftqmUKBoJg9f3ddSDmysRh1cSE/edit#",
    },

    ///////////////////
    // Roleplay info about your character.
    ///////////////////
    Bio:
    {
        "Backstory":
        [
            "Was born into Clan Fester, a great Goblin clan",
            "Was (much) smarter than the average goblin, became the apprentice to the Clan's shaman",
            "The shaman saw his potential and was jealous, he kept Scab from seeing his spellbook out of fear that Scab would be able to replace him",
            "Scab didn't like this and tried to steal the spellbook",
            "He got caught, he and the shaman fought, the shaman was killed",
            "Scab ran away from the clan, otherwise he would have been executed for his crime",
            "He was only 6 years old",
            "Has been living on his own for 4 years at this point, begging, stealing, doing whatever he can to survive",
            "He is looking for a great treasure to bring back to the clan, to beg the Goblin king for forgiveness and permission to rejoin the clan",
            "Rumour #1 (True): That goblin killed someone to get that fancy book of his",
            "Rumour #2 (True): That goblin has some sticky fingers, sometimes things go missing even though you've been staring at him the entire time",
            "Rumour #3 (False): He may not look it, but he's secretly an exiled goblin prince. Try not to accidentally kill him if (when) you have to give him a thrashing",
        ],

        "Ideals":
        [
            "Might makes right. Scab defers to the most powerful person in the room, as most goblins do",
            "Scab is easily bribed, and takes great stock in physical possessions",
            "Scab tries to avoid killing intelligent beings; it always complicated things and there is usually an easier way to get what he needs",
            "He will follow the law unless breaking it is in his best interest and he's reasonably sure that he will not be caught",
        ],

        "Bonds":
        [
            "Scab's overall goal is to be able to return to Clan Fester without being executed",
            "He's trying to find a great treasure or gift that he can present to the Goblin king as a bribe to be allowed back",
            "His most treasured possession is his spellbook. It is the spellbook of the shaman's of Clan Fester, passed down over the years",
        ],

        "Flaws":
        [
            "Scab is a fairly typical goblin",
            "He is greedy, scheming, easily made jealous, and easily scared",
        ],

        "Physical Description":
        [
            "Grimy",
            "Furtive",
        ],

        Picture: "Scab.png",
    },
};

//
// Smush all the data together into a single object, since it's only spread
// across multiple objects to make editing the data easier for the user.
//
const Character = Object.assign
(
    DataThatChangesOften,
    DataThatChangesLessOften,
    DataThatChangesOnLevelUp,
    DataThatIsSetOnCharacterCreation
);
